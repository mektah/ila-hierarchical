from setuptools import setup
from distutils.core import Extension
from Cython.Build import cythonize

setup(ext_modules = cythonize(Extension(
           "CILA",                                # the extension name
           sources=["CILA.pyx", "src/Aggregation.cpp", "src/generative.cpp", "src/Settings.cpp", "src/Util.cpp", "src/Model.cpp", "src/Graph.cpp", "src/sampler.cpp", "src/ModelIO.cpp", "src/Plotting.cpp", "src/Input.cpp"], # the Cython source and
                                                  # additional C++ source files
           include_dirs=["cereal", "cxxopts"],
           language="c++",                        # generate and compile C++ code
           extra_compile_args=["-fopenmp", "-std=c++11", "-O3", "-g"],
           extra_link_args=["-fopenmp", "-std=c++11"],
           undef_macros = [ "NDEBUG" ]
      )))

