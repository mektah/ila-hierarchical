/*
 * Util.h
 *
 *  Created on: 02.04.2013
 *      Author: moritz
 */

#ifndef UTIL_H_
#define UTIL_H_

#include <assert.h>
#include "types.h"

namespace CILA {
namespace core {

class Util {
public:
	Util() = default;
	virtual ~Util() = default;
	static bool equals(vector<NodeID> perm1, vector<NodeID> perm2) __attribute__((const));
	static vector<NodeID> dummyPermutation(NodeID n);
	static vector<NodeID> randPermutation(NodeID n);
	static vector<NodeID> reverse(vector<NodeID> perm) __attribute__((const));
	static double anova(vector<int> ownSlice, vector<double> values);
	static double computeNMI(vector<int> a, vector<int> b);//TODO: figure out a series of wrappers to have a clean interface
	static vector<double> balancedPurityandNMI(const vector<int> &featureSlice, const vector<int> &externalLabels);
	static vector<double> averagePurityAndNMIMax(ClusterMatrix &ownlabels, ClusterMatrix &externalLabels);

	static vector<double> pValuesPurityAndNMI(vector<int> clusters, vector<int> external,	int samplesize);
	static double pValueAnova(vector<int> clusters, vector<double> external, int samplesize);
	static bool containsNegative(ClusterMatrix &externalLabels, FeatureID l);
	static double sumMatrix(vector<vector<int> > triangle);
	static vector<vector<double> > transpose(vector<vector<double> > input);
	static double logSumExp(vector<double> logProbs);
	static double normalizedSumOfsquaredDifference(vector<vector<double> > * a, vector<vector<double> > * b);
	static vector<vector<double> > averageMatrices(vector<vector<vector<double> > > matrices, vector<double> weights);
	template <class T> static vector<T> apply(vector<NodeID> perm1, vector<T> input) __attribute__((const));
	template <class T> static vector<T> matslice(vector<vector<T> > * matrix, ClusterID index, int offset = 0);
};

template<class T>
inline vector<T> Util::matslice(vector<vector<T> > * matrix, ClusterID index, int offset) {
	vector<T> result(matrix->size());
	for (unsigned i = 0; i < matrix->size(); i++) {
		assert(index < (*matrix)[i].size());
		T temp = (*matrix)[i][index]+offset;
		result[i] = temp;
	}
	return result;
}

template<class T>
inline vector<T> Util::apply(vector<NodeID> perm1, vector<T> input) {
	assert(perm1.size() == input.size());
	int n = perm1.size();
	vector<T> result(n);
	for (int i = 0; i < n; i++) {
		result[i] = input[perm1[i]];
	}
	return result;
}

} /* namespace core */
} /* namespace CILA */
#endif /* UTIL_H_ */
