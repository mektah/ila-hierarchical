#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <algorithm>

#include "Input.h"

using std::vector;

namespace CILA {
namespace io {

class Boxplotscript {

int main(int ac, char* av[]) {

vector<vector<double> > result = Input::parseDoubles(Input::loadRawArray("timedump", 50));

for (int i = 0; i < result.size(); i++) {
	sort(result[i].begin(), result[i].end());
	double l = result[i].size();
	double middle = result[i].size() / 2;
	double median;
	if (result[i].size() % 2 == 0) {
		median = (result[i][middle-1] + result[i][middle])/2;
	} else {
		median = result[i][middle];
	}

	double lowerquartil = result[i][l/4];
	double upperquartil = result[i][3*l/4];

//	cout << "l: " << l << ", middle: " << middle << ", lower: " << l/4 << endl;

	cout << (i+1)*5 << " " << median <<  " " << upperquartil << " " << lowerquartil << " " << result[i][l-1] << " " << result[i][0] << endl;
}
}

};

}
}
