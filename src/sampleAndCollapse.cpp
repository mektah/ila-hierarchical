/*
 * sampleAndCollapse.cpp
 *
 *  Created on: 08.03.2015
 *      Author: moritzl
 */

#include <iostream>
#include <vector>

#include "Aggregation.h"
#include "generative.h"
#include "sampler.h"
#include "Graph.h"
#include "types.h"
#include "TestChamber.h"
#include "mex.h"
#include "Model.h"

namespace CILA {
using namespace core;

ClusterMatrix sampleAndCollapse(Graph input, Settings settings) {

	NodeID n = input.size();
	double alpha = settings.init_alpha;
	double gamma = settings.init_gamma;
	Model r = Model::generateModel(n, alpha, gamma, settings.mu_weights, settings.sigma_weights, settings.mu_bias, settings.sigma_bias);
	
	settings.saveStates = false;//is it that good, overwriting settings?
	Sampler sampler(input, r, settings);

	Aggregation result(r);//the argument doesn't matter, since it goes into the averaging with a weight of 0.
	int j = 0;

	for (int i = 0; i < settings.iterations; i++) {
		sampler.sampleIter();
		if (i >= settings.burnin && i % settings.thin == 0) {
			Model model = sampler.getModel();
			result = Aggregation::aggregate(result, j, Aggregation(model), 1);
			j++;
		}
	}
	
	return result.collapse(settings).clusterAssignment;
}

Settings parseSettingsFromMex(const mxArray * inputstruct) {
	Settings prior = Settings::getDefaultSettings();

	int number;
	mxArray * tmp;

	number = mxGetFieldNumber(inputstruct, "iterations");
	if (number >= 0) {
		prior.iterations = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "mu_bias");
	if (number >= 0) {
		prior.mu_bias = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "sigma_bias");
	if (number >= 0) {
		prior.sigma_bias = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "mu_weights");
	if (number >= 0) {
		prior.mu_weights = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "sigma_weights");
	if (number >= 0) {
		prior.sigma_weights = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "gamma_scale");
	if (number >= 0) {
		prior.gamma_scale = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "gamma_shape");
	if (number >= 0) {
		prior.gamma_shape = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "alpha_scale");
	if (number >= 0) {
		prior.alpha_scale = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "alpha_shape");
	if (number >= 0) {
		prior.alpha_shape = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "variance_scale");
	if (number >= 0) {
		prior.variance_scale = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "variance_shape");
	if (number >= 0) {
		prior.variance_shape = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "init_alpha");
	if (number >= 0) {
		prior.init_alpha = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "init_gamma");
	if (number >= 0) {
		prior.init_gamma = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "init_bias");
	if (number >= 0) {
		prior.init_bias = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "init_variance");
	if (number >= 0) {
		prior.init_variance = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "m_aux");
	if (number >= 0) {
		prior.m_aux = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "cthreshold");
	if (number >= 0) {
		prior.cthreshold = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "fthreshold");
	if (number >= 0) {
		prior.fthreshold = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "sthreshold");
	if (number >= 0) {
		prior.sthreshold = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "burnin");
	if (number >= 0) {
		prior.burnin = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	number = mxGetFieldNumber(inputstruct, "thin");
	if (number >= 0) {
		prior.thin = *mxGetPr(mxGetFieldByNumber(inputstruct, 0, number));
	}

	return prior;
}
}

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{
  double *A;
  size_t m,n,p;      /* matrix dimensions */

  /* Check for proper number of arguments */

  if (nrhs < 1) {
    mexErrMsgIdAndTxt("MATLAB:sampleAndCollaps:nargin",
            "CILA requires a least one input argument.");
  } else if (nlhs != 1) {
    mexErrMsgIdAndTxt("MATLAB:sampleAndCollapse:nargout",
            "CILA requires one output argument.");
  } else if (nrhs > 2) {
	  mexErrMsgIdAndTxt("MATLAB:sampleAndCollapse:nargin",
	              "CILA requires at most two input arguments.");
  }

  A = mxGetPr(prhs[0]);

  n = mxGetM(prhs[0]);
  m = mxGetN(prhs[0]);

  cout << "n: "<< n<< ", m: " << m << endl;

  if (n != m) {
	  mexErrMsgIdAndTxt("MATLAB:sampleAndCollapse:square", "Input matrix must be square.");
  }

  if (n == 0) {
	  if (n != m) {
		  mexErrMsgIdAndTxt("MATLAB:sampleAndCollapse:matrixDim", "Input matrix must have dimension of at least 1.");
	  }
  }

  vector<vector<double> > Avector(n);

  //transfer input into nested vector
  for (size_t i = 0; i < n; i++) {
	  Avector[i].resize(n);
	  for (size_t j = 0; j < n; j++) {
		  Avector[i][j] = A[i*n+j];
	  }
  }

  CILA::core::Settings settings;
  if (nrhs > 1) {
	  if (!mxIsStruct(prhs[1])) mexErrMsgIdAndTxt("MATLAB:sampleAndCollapse:notstruct",
              "If the second argument is given, it must be a struct of type settings.");
	  settings = CILA::parseSettingsFromMex(prhs[1]);
  } else {
	  settings = CILA::core::Settings::getDefaultSettings();
  }

  CILA::core::ClusterMatrix result = CILA::sampleAndCollapse(CILA::core::Graph(Avector), settings);
  CILA::core::FeatureID fcount = result[0].size();

  plhs[0] = mxCreateDoubleMatrix(n, fcount, mxREAL);
  double* outputMatrix = (double *)mxGetPr(plhs[0]);

  //transfer back to array
  for (size_t i = 0; i < n; i++) {
  	  for (size_t j = 0; j < fcount; j++) {
  		  outputMatrix[i*fcount + j] = result[i][j];
  	  }
  }

  return;
}


