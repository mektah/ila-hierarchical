/*
 * Graph.h
 *
 *  Created on: 30.11.2012
 *      Author: moritz
 */

#ifndef GRAPH_H_
#define GRAPH_H_

#include <string>
#include <vector>
#include <iostream>
#include <cereal/access.hpp>

#include "types.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;
using std::ofstream;

namespace CILA {
namespace core {

class Graph {
public:
	Graph();
	Graph(NodeID nodeCount);
	Graph(vector<vector<edgetype> > edges);
	virtual ~Graph();
	NodeID size() __attribute__((pure));
	edgetype hasEdge(NodeID i, NodeID j) __attribute__((pure));//eventually replace with matrix approach to increase speed
	void addNode(vector<edgetype> connections);
	vector<edgetype> getNodeSlice(NodeID i);
	Graph getSubgraph(NodeID limit) __attribute__((pure));
	void permute(vector<NodeID> permutation);
	bool equals(Graph other) __attribute__((pure));
	void exportToGraphviz(string filename);
	vector<vector<edgetype> > edges; //R in PalKnoGha

private:
	NodeID nodeCount;
	friend class cereal::access;
	template<class Archive>
		    void serialize(Archive & ar, const unsigned int version)
		    {
		        ar & edges;
		        ar & nodeCount;
		    }

};
}
}

#endif /* GRAPH_H_ */
