/*
 * ProbabiltyDensityFunctor.h
 *
 *  Created on: 02.04.2013
 *      Author: moritz
 */

#ifndef PROBABILTYDENSITYFUNCTOR_H_
#define PROBABILTYDENSITYFUNCTOR_H_

#include <assert.h>
#include <vector>
#include <limits>
#include <cmath>

#include "Graph.h"
#include "types.h"
#include "Settings.h"
#include "generative.h"
#include "Model.h"

using std::numeric_limits;

namespace CILA {
namespace core {

class ProbabilityDensityFunctor {
	public:
		ProbabilityDensityFunctor() {}
		virtual ~ProbabilityDensityFunctor() {}
		virtual double pdf(double alpha) {
			return 1;
		}
		virtual double logpdf(double alpha) {
			return 0;
		}
};

class CRPPDF : public ProbabilityDensityFunctor{
	public:
		CRPPDF(vector<int> * featurePopulation, vector<vector<int> > * subClusterPopulation, Settings settings) {
			featureCount = featurePopulation->size();
			assert(featureCount == subClusterPopulation->size());
			totalClusterCount = 0;
			temp = 0;
			feaPop = featurePopulation;
			param = settings;

			for (FeatureID m = 0; m < featureCount; m++) {
				totalClusterCount += (*subClusterPopulation)[m].size();
				for (ClusterID c = 0; c < (*subClusterPopulation)[m].size(); c++) {
					int clusterPopulation = (*subClusterPopulation)[m][c];
					temp += lgamma(clusterPopulation);
					assert(temp < numeric_limits<double>::infinity());
				}
			}
		}

		virtual ~CRPPDF() {

		}

		double pdf(double gamma) {
			/*
			 * threadsafe
			 */
			return exp(logpdf(gamma));
		}

		double logpdf(double gamma) {
			/*
			 * threadsafe
			 */
			if (gamma == 0) return log(0);

			double sumFeaPopgamma = 0;
			for (FeatureID m = 0; m < featureCount; m++) {
				sumFeaPopgamma += lgamma(gamma + (*feaPop)[m]);
			}

			double result = -param.gamma_shape*log(param.gamma_scale) - lgamma(param.gamma_shape) + (param.gamma_shape-1)*log(gamma) - gamma / param.gamma_scale;

			result += featureCount*lgamma(gamma)+totalClusterCount*log(gamma)+temp - sumFeaPopgamma;
			//assert(result <= 0);
			assert(result > -numeric_limits<double>::infinity());
			return result;
		}
	private:
		double temp;
		double totalClusterCount;
		double featureCount;
		vector<int> * feaPop;
		Settings param;
	};

class BiasPDF : public ProbabilityDensityFunctor{
	public:
		BiasPDF(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double mu_s, double sigma_s, double variance, Settings settings) {
			graph = g;
			features = f;
			clusters = c;
			weights = w;
			mu = mu_s;
			sigma = sigma_s;
			this->variance = variance;
			vector<vector<double > > T = Model::computeT(f, c, w);
			nodeCount = g->size();
			diff.resize(nodeCount);
			for (NodeID i = 0; i < nodeCount; i++) diff[i].resize(nodeCount);
			for (NodeID i = 0; i < nodeCount; i++) {
				for (NodeID j = 0; j < nodeCount; j++) {
					diff[i][j] = g->hasEdge(i,j) - T[i][j];
					diff[j][i] = g->hasEdge(j,i) - T[j][i];
				}
			}
			param = settings;
		}

		virtual ~BiasPDF() {

		}

		double pdf(double bias) {
			return exp(logpdf(bias));
		}

		double logpdf(double bias) {
			/*
			 * threadsafe
			 */
			//prior:
			double prior = normalLogProbabilityDensity(mu, sigma, bias);

			double sum = 0;
			if (param.naivebias) return prior + Model::logLikelihood(graph, features, clusters, weights, bias, variance);

			for (NodeID i = 0; i < nodeCount; i++) {
				for (NodeID j = 0; j < nodeCount; j++) {
					double d = diff[i][j];
					sum += - ((bias-d)*(bias-d));
				}
			}
			sum = sum / (2*variance*variance);
			/*
			 * the following line can be omitted since it is the same for every bias
			 */
			//sum += nodeCount*nodeCount*(-log(variance) - 0.5*log(2*pi));

			return sum+prior;
		}
	private:
		NodeID nodeCount;
		vector<vector<double > > diff;
		FeatureMatrix * features;
		ClusterMatrix * clusters;
		WeightMatrices * weights;
		Graph * graph;
		double mu, sigma, variance;
		Settings param;
	};

class VariancePDF : public ProbabilityDensityFunctor{
	public:
		VariancePDF(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, Settings settings) {
			graph = g;
			param = settings;
			features = f;
			clusters = c;
			weights = w;
			this->s = s;
			this->sumOfSquares = 0;
			vector<vector<double > > T = Model::computeT(f, c, w);
			nodeCount = g->size();
			diff.resize(nodeCount);
			for (NodeID i = 0; i < nodeCount; i++) diff[i].resize(nodeCount);
			for (NodeID i = 0; i < nodeCount; i++) {
				for (NodeID j = 0; j < nodeCount; j++) {//TODO: replace this with j <= i
					diff[i][j] = g->hasEdge(i,j) - (T[i][j]+s);
					sumOfSquares += diff[i][j]*diff[i][j];
					//diff[j][i] = g->hasEdge(j,i) - (T[j][i]+s);
				}
			}
		}

		virtual ~VariancePDF() {

		}

		double pdf(double variance) {
			return exp(logpdf(variance));
		}

		double logpdf(double variance) {
			double g = -param.variance_shape*log(param.variance_scale) - lgamma(param.variance_shape) + (param.variance_shape-1)*log(variance) - variance / param.variance_scale;

			double sum = -sumOfSquares/(2*variance*variance) + nodeCount*nodeCount*(-log(variance)-0.5*log(2*M_PI));
			assert(sum > -numeric_limits<double>::infinity());
			return sum - g;
		}
	private:
		NodeID nodeCount;
		vector<vector<double> > diff;
		FeatureMatrix * features;
		ClusterMatrix * clusters;
		WeightMatrices * weights;
		Graph * graph;
		Settings param;
		double s;
		double sumOfSquares;
	};

class WeightPDF : public ProbabilityDensityFunctor{
	public:
		WeightPDF(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, FeatureID m, ClusterID c1, ClusterID c2, double s, double variance, Settings settings) {
			graph = g;
			features = f;
			clusters = c;
			weights = w;
			assert(m < weights->size());
			assert(c1 < (*weights)[m].size());
			assert(c2 < (*weights)[m].size());
			this->fid = m;
			this->c1 = c1;
			this->c2 = c2;
			this->s = s;
			this->mu_w = settings.mu_weights;
			this->sigma_w = settings.sigma_weights;
			this->variance = variance;
			this->settings = settings;
			oldll = Model::logLikelihood(graph, features, clusters, weights, s, variance);
			c1pop.clear();
			c2pop.clear();

			for (NodeID i = 0; i < g->size(); i++) {
				if ((*c)[i][m] == c1) {
					c1pop.push_back(i);
				}
				if ((*c)[i][m] == c2) {
					c2pop.push_back(i);
				}
			}
		}

		virtual ~WeightPDF() {

		}

		double pdf(double weight) {
			return exp(logpdf(weight));
		}

		double logpdf(double weight) {
			//prior:
			double result;
			if (!settings.naiveWeights) {
				result = normalLogProbabilityDensity(mu_w, sigma_w, weight);
				result += Model::logLikelihoodNewWeights(graph, features, clusters, weights, s, variance, fid, c1, c2, &c1pop, &c2pop, weight, oldll);
			return result;
			} else {
				result = - ((weight-mu_w)*(weight-mu_w))/(2*sigma_w*sigma_w);

				double oldweight = (*weights)[fid][c1][c2];
				assert(oldweight == (*weights)[fid][c2][c1]);
				//likelihood:
				(*weights)[fid][c1][c2] = weight;
				(*weights)[fid][c2][c1] = weight;
				result += Model::logLikelihood(graph, features, clusters, weights, s, variance);
				(*weights)[fid][c1][c2] = oldweight;
				(*weights)[fid][c2][c1] = oldweight;

				return result;
			}
		}
	private:
		FeatureMatrix * features;
		ClusterMatrix * clusters;
		WeightMatrices * weights;
		Graph * graph;
		FeatureID fid;
		ClusterID c1;
		ClusterID c2;
		vector<NodeID> c1pop;
		vector<NodeID> c2pop;
		double s;
		double mu_w;
		double sigma_w;
		double variance;
		double oldll;
		Settings settings;
	};

class PoissonPDF : public ProbabilityDensityFunctor {
public:
	PoissonPDF(double mean) {
		lambda = mean;
	}

	virtual ~PoissonPDF() {

	}

	double pdf(double k) {
		return (pow(lambda, k) / gamma(k+1))*exp(-lambda);
	}

	double logpdf(double k) {
		return k*log(lambda) - lgamma(k+1) - lambda;
	}

private:
	double lambda;
};

}//end namespace core
}//end namespace CILA

#endif /* PROBABILTYDENSITYFUNCTOR_H_ */
