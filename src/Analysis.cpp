/*
 * Analysis.cpp
 *
 *  Created on: 22.07.2013
 *      Author: moritz
 */

#include <queue>

#include "Analysis.h"
#include "Aggregation.h"
#include "Util.h"
#include "ModelIO.h"

namespace CILA {
using namespace core;
namespace io {

void Analysis::printPurity(ClusterMatrix& external,	Model& result) {
	vector < vector<double> > purityMatrix;

	vector<vector < vector<double> > > matrices = result.computePurityandNMIMatrices(external);
	purityMatrix = matrices[0];
	vector < vector<double> > reducedPMatrix(purityMatrix.size());
	vector<vector<double> > pMatrix = result.pValuePurityMatrix(external, 1000);
	for (FeatureID m = 0; m < purityMatrix.size(); m++) {
		reducedPMatrix[m].resize(purityMatrix[m].size());
		for (FeatureID l = 0; l < purityMatrix[m].size(); l++) {
			cout << purityMatrix[m][l] << " (" << pMatrix[m][l] << ") ";
			reducedPMatrix[m][l] = std::min(pMatrix[m][l],0.1);
		}
		cout << endl;
	}
	Plotting::plotMatrix(purityMatrix, "Purity");
	Plotting::plotMatrix(reducedPMatrix, "Capped p-Values of Purity");
}

void Analysis::printDistances(Model& orig, Model& result) {
	FeatureID ofeatureCount = orig.getFeatureCount();
	FeatureID rfeatureCount = result.getFeatureCount();
	vector<vector<double> > distanceMatrix(rfeatureCount);
	for (FeatureID m = 0; m < rfeatureCount; m++) {
		distanceMatrix[m].resize(ofeatureCount);
		for (FeatureID l = 0; l < ofeatureCount; l++) {
			distanceMatrix[m][l] = Model::computeDistance(&orig, l, &result, m);
		}
	}
	Plotting::plotMatrix(distanceMatrix, "Distances");
}

void Analysis::printAnova(vector<vector<double> > external,	Model& result) {
	vector < vector<double> > anovaMatrix;
	vector<vector<double> > pvalues = result.pValueAnovaMatrix(external, 1000);
	std::streamsize oldprecision = cout.precision(2);
	anovaMatrix = result.computeAnovaMatrix(external);
	for (FeatureID m = 0; m < anovaMatrix.size(); m++) {
		for (FeatureID l = 0; l < anovaMatrix[m].size(); l++) {
			cout << anovaMatrix[m][l] << " ";
			cout << "(" << pvalues[m][l] << ") ";
			pvalues[m][l] = std::min(pvalues[m][l],0.1);
		}
		cout << endl;
	}
	cout.precision(oldprecision);
	Plotting::plotMatrix(anovaMatrix, "Anova");
	Plotting::plotMatrix(pvalues, "Capped-p-values-of-Anova");
}

void Analysis::printNMI(ClusterMatrix& external,	Model& result) {
	vector<vector<vector<double> > > compound = result.computePurityandNMIMatrices(external);
	ClusterMatrix clusters = result.getClusterMatrix();
	vector < vector<double> > nmiMatrix = compound[1];
	vector<vector<double> > redMatrix = compound[2];
	vector<vector<double> > pvalues = result.pValueNMIMatrix(external, 1000);
	vector<vector<double> > cappedpvalues(nmiMatrix.size());
	std::streamsize oldprecision = cout.precision(2);
	for (FeatureID m = 0; m < nmiMatrix.size(); m++) {
		cappedpvalues[m].resize(nmiMatrix[m].size());
		for (FeatureID l = 0; l < nmiMatrix[m].size(); l++) {
			cout << nmiMatrix[m][l] << " ";
			cout << "(" << redMatrix[m][l] << ") ";
			cappedpvalues[m][l] = std::min(pvalues[m][l], 0.1);
		}
		cout << endl;
	}
	cout.precision(oldprecision);
	Plotting::plotMatrix(cappedpvalues, "Capped-P-Values-of-NMI");
	Plotting::plotMatrix(nmiMatrix, "NMI");
}

void Analysis::investigateCumulativeMean(vector<vector<Model> > &chains, Model &orig, Settings settings) {
	ClusterMatrix labels = orig.getClusterMatrix();
	Graph g = orig.sampleGraphInstance(0);
	vector<double> distanceToPast;
	vector<double> distanceToOrig;
	vector<double> singleDistanceToOrig;
	vector<double> alpha;
	vector<double> gamma;
	vector<double> bias;
	vector<double> variance;
	vector<double> purityFF;
	vector<double> nmiFF;
	vector<double> nmiAvg;
	vector<double> tracepurityFF;
	vector<double> tracenmiFF;
	vector<double> tracenmiAvg;
	vector<double> length;
	vector<double> traceLength;
	vector<double> totalLinkCount;
	vector<double> traceTotalLinkCount;
	vector<Model> cumMean;
	vector<Model> singlechain;
	Model origrep = Model::mapToRepresentative(orig);
	Aggregation cumAgg;
	int totalcount = 0;
	for (unsigned c = 0; c < chains.size(); c++) {
		for (unsigned i = 0; i < chains[c].size(); i++) {
			Model model = chains[c][i];
			singlechain.push_back(model);
			if (totalcount > 0) {
				cumAgg = Aggregation::aggregate(cumAgg, totalcount, Aggregation(model), 1);
				cumMean.push_back(cumAgg.collapse(settings.cthreshold, settings.fthreshold, settings.sthreshold));
				if (totalcount > 10) {
					distanceToPast.push_back(Model::computeDistance(&cumMean.back(), &cumMean[totalcount-10]));
				}
			} else {
				cumMean.push_back(model);
				cumAgg = Aggregation(model);
			}
			ClusterMatrix clusters = cumMean.back().getClusterMatrix();
			double purity, nmi;

			int ownOffset = 0;
			int extOffset = 0;

			if (clusters[0].size() != 0) ownOffset = Util::containsNegative(clusters, 0) ? 1 : 0;
			if (labels[0].size() != 0) extOffset = Util::containsNegative(labels, 0) ? 1 : 0;

			if (clusters[0].size() == 0) {
				if (labels[0].size() == 0) {
					purity = 1;
					nmi = 1;
				} else {
					purity = 0;
					nmi = 0;
				}
			} else {
				if (labels[0].size() == 0) {
					purity = 0;
					nmi = 0;
				} else {
					vector<double> cache = Util::balancedPurityandNMI(Util::matslice(&clusters, 0,ownOffset), Util::matslice(&labels, 0, extOffset));
					purity = cache[0];
					nmi = cache[1];
				}
			}

			purityFF.push_back(purity);
			nmiFF.push_back(nmi);
			distanceToOrig.push_back(Model::computeDistance(&cumMean.back(), &origrep));
			totalLinkCount.push_back(cumMean.back().getTotalLinkCount());

			vector<double> avgcache = Util::averagePurityAndNMIMax(clusters, labels);
			nmiAvg.push_back(avgcache[1]);

			ClusterMatrix traceclusters = model.getClusterMatrix();
			double tracepurity, tracenmi;
			if (traceclusters[0].size() == 0) {
				tracepurity = (double) (labels[0].size() == 0); //1 if same, 0 if different
				tracenmi = (double) (labels[0].size() == 0);
			} else {
				ownOffset = Util::containsNegative(traceclusters, 0);
				if (labels[0].size() == 0) {
					tracepurity = 0;
					tracenmi = 0;
				} else {
					vector<double> trcache = Util::balancedPurityandNMI(Util::matslice(&traceclusters, 0,ownOffset), Util::matslice(&labels, 0, extOffset));
					tracepurity = trcache[0];
					tracenmi = trcache[1];
				}
			}


			tracepurityFF.push_back(tracepurity);
			tracenmiFF.push_back(tracenmi);
			traceTotalLinkCount.push_back(model.getTotalLinkCount());
			singleDistanceToOrig.push_back(Model::computeDistance(&model, &origrep));

			vector<double> travgcache = Util::averagePurityAndNMIMax(traceclusters, labels);
			tracenmiAvg.push_back(travgcache[1]);

			alpha.push_back(cumMean.back().alpha);
			gamma.push_back(cumMean.back().gamma);
			bias.push_back(cumMean.back().s);
			variance.push_back(cumMean.back().sigma);
			totalcount++;
		}
	}


	/**
	 * get nmiSum vs clusterthreshold
	 */


	int precision = 20;
	double stepsize = 1 / (double) (precision);
	vector<vector<double> > distthreshold(precision);
	vector<double> nmiSum(precision, 0);

	for (int i = 0; i < precision; i++) {
		distthreshold[i].resize(precision);
		Model nmitest = cumAgg.collapse(i*stepsize, settings.fthreshold,settings.sthreshold);
		ClusterMatrix ownclusters = nmitest.getClusterMatrix();
		ClusterMatrix origrepclusters = origrep.getClusterMatrix();
		for (FeatureID m = 0; m < ownclusters[0].size(); m++) {
			double max = 0;
			for (FeatureID f = 0; f < origrepclusters[0].size(); f++) {
				int ownOffset = Util::containsNegative(ownclusters, m) ? 1 : 0;
				int extOffset = Util::containsNegative(origrepclusters, f) ? 1 : 0;
				double nmi = Util::balancedPurityandNMI(Util::matslice(&ownclusters,m,ownOffset), Util::matslice(&origrepclusters,f,extOffset))[1];
				if (!(nmi < max)) max = nmi;
			}
			if (!(max == max)) max = 0;
			nmiSum[i] += max;
		}
		for (int j = 0; j < precision; j++) {
			assert(i * stepsize >= 0);
			assert(i * stepsize <= 1);
			assert(j * stepsize >= 0);
			assert(j * stepsize <= 1);
			Model interim = cumAgg.collapse(i*stepsize, j*stepsize, 0.1);
			distthreshold[i][j] = Model::computeDistance(&interim, &origrep);
		}
	}

	/**
	 * plot everything
	 */
	ClusterMatrix clusters = orig.getClusterMatrix();
	Model last = cumMean.back();
	if (!settings.silent) {
		cout << "Aggregation of everything:" << endl;
		Model::printClusters(last.getClusterMatrix());
		cout << "Likelihood of Aggregation: ";
		cout << last.logLikelihood(&g) << endl;
		printPurity(clusters, last);
		printNMI(clusters, last);
	}
	//Plotting::plotMatrix(distthreshold, "Distance vs. Thresholds");
	Plotting::plotHistogramm(nmiSum, 0, 1, "NMIsum vs clusterthreshold");
	Plotting::plotHistogramm(distanceToPast, "Ten-back distance of aggregated mean");
	Plotting::plotHistogramm(distanceToOrig, "Distance to Original");
	Plotting::plotHistogramm(singleDistanceToOrig, "Distance of trace to original");
	Plotting::plotHistogramm(alpha, "Alpha of aggregated mean");
	Plotting::plotHistogramm(gamma, "Gamma of aggregated mean");
	Plotting::plotHistogramm(bias, "Bias of aggregated mean");
	Plotting::plotHistogramm(variance, "Variance of aggregated mean");
	Plotting::plotHistogramm(purityFF, "First-Features-purity of aggregated mean");
	Plotting::plotHistogramm(nmiFF, "First-Features-nmi of aggregated mean");
	Plotting::plotHistogramm(nmiAvg, "Average NMI of aggregated mean");
	Plotting::plotHistogramm(tracenmiAvg, "Average NMI of trace");
	Plotting::plotHistogramm(tracepurityFF, "First-Features-purity of trace");
	Plotting::plotHistogramm(tracenmiFF, "First-Features-nmi of trace");
	Plotting::plotHistogramm(totalLinkCount, "Total Link Count of aggregation");
	Plotting::plotHistogramm(traceTotalLinkCount, "Total Link Count of trace");
}

void Analysis::investigateCumulativeMean(vector<vector<Model> >& chains, ClusterMatrix labels, Settings settings) {
	vector<double> purityFF;
	vector<double> nmiFF;
	vector<double> nmiAvg;
	vector<double> distanceToPast;
	vector<double> alpha;
	vector<double> gamma;
	vector<double> bias;
	vector<double> variance;
	vector<double> tracepurityFF;
	vector<double> tracenmiFF;
	vector<double> totalLinkCount;
	vector<double> traceTotalLinkCount;
	vector<Model> cumMean;
	Aggregation cumAgg;
	int totalcount = 0;
	for (unsigned c = 0; c < chains.size(); c++) {
		for (unsigned i = 0; i < chains[c].size(); i++) {
			Model model = chains[c][i];

			if (totalcount > 0) {
				cumAgg = Aggregation::aggregate(cumAgg, totalcount, Aggregation(model), 1);
				cumMean.push_back(cumAgg.collapse(settings.cthreshold,settings.fthreshold,settings.sthreshold));
				ClusterMatrix clusters = cumMean.back().getClusterMatrix();

				int ownOffset = Util::containsNegative(clusters, 0) ? 1 : 0;
				assert(!Util::containsNegative(labels, 0));
				vector<double> cache = Util::balancedPurityandNMI(Util::matslice(&clusters, 0,ownOffset), Util::matslice(&labels, 0, 0));
				purityFF.push_back(cache[0]);
				nmiFF.push_back(cache[1]);
				totalLinkCount.push_back(cumMean.back().getTotalLinkCount());

				vector<double> avgcache = Util::averagePurityAndNMIMax(clusters, labels);
				nmiAvg.push_back(avgcache[1]);

				ClusterMatrix traceclusters = model.getClusterMatrix();
				ownOffset = Util::containsNegative(traceclusters,0) ? 1 : 0;
				vector<double> trcache = Util::balancedPurityandNMI(Util::matslice(&traceclusters, 0,ownOffset), Util::matslice(&labels, 0, 0));
				tracepurityFF.push_back(trcache[0]);
				tracenmiFF.push_back(trcache[1]);
				traceTotalLinkCount.push_back(model.getTotalLinkCount());

				if (totalcount > 10) {
					distanceToPast.push_back(Model::computeDistance(&cumMean.back(), &cumMean[totalcount-10]));
				}
			} else {
				cumAgg = Aggregation(model);
				cumMean.push_back(model);
			}
			totalcount++;
		}
	}

	Model last = cumMean.back();
	if (!settings.silent) {
		printPurity(labels, last);
		printNMI(labels, last);
	}

	Plotting::plotHistogramm(purityFF, "First-Features-purity of aggregated mean");
	Plotting::plotHistogramm(nmiFF, "First-Features-nmi of aggregated mean");
	Plotting::plotHistogramm(nmiAvg, "Average NMI of aggregated mean");
	Plotting::plotHistogramm(distanceToPast, "Ten-back distance of aggregated mean");
	Plotting::plotHistogramm(alpha, "Alpha of aggregated mean");
	Plotting::plotHistogramm(gamma, "Gamma of aggregated mean");
	Plotting::plotHistogramm(bias, "Bias of aggregated mean");
	Plotting::plotHistogramm(variance, "Variance of aggregated mean");
	Plotting::plotHistogramm(purityFF, "First-Features-purity of aggregated mean");
	Plotting::plotHistogramm(nmiFF, "First-Features-nmi of aggregated mean");
	Plotting::plotHistogramm(tracepurityFF, "First-Features-purity of trace");
	Plotting::plotHistogramm(tracenmiFF, "First-Features-nmi of trace");
	Plotting::plotHistogramm(totalLinkCount, "Total Link Count of aggregation");
	Plotting::plotHistogramm(traceTotalLinkCount, "Total Link Count of trace");
}

int Analysis::calculateInterval(vector<Sampler>*& runlist, unsigned & maxsize, unsigned & realmax) {
	int maxPlotPoints = 0;
	maxsize = 0;
	realmax = 0;
	for (unsigned c = 0; c < runlist->size(); c++) {
		unsigned realsize = (*runlist)[c].chain.size() * (*runlist)[c].getSettings().thin;
		if (realsize > realmax) realmax = realsize;
		if ((*runlist)[c].chain.size() > maxsize) {
			maxsize = (*runlist)[c].chain.size();
			maxPlotPoints = (*runlist)[c].getSettings().maxPlotPoints;
		}
	}

	int interval = 1;

	if (maxsize > maxPlotPoints) {
		interval = maxsize / maxPlotPoints;
	}
	return interval;
}

vector<double> Analysis::extractValues(vector<Sampler>*runlist, unsigned c, unsigned i) {
	vector<double> values(6);
	values[0] = runlist->at(c).chain[i].alpha;
	values[1] = runlist->at(c).chain[i].gamma;
	values[2] = runlist->at(c).chain[i].s;
	values[3] = runlist->at(c).chain[i].sigma;
	values[4] = runlist->at(c).chain[i].getTotalLinkCount();
	values[5] = Model::logLikelihood(&((*runlist)[c].R),
			&((*runlist)[c].chain[i].featurePresent),
			&((*runlist)[c].chain[i].clusterAssignment),
			&((*runlist)[c].chain[i].weights),
			(*runlist)[c].chain[i].s,
			(*runlist)[c].chain[i].sigma);
	return values;
}

void Analysis::analyze(vector<Sampler>* runlist, Settings settings) {
	unsigned chaincount = runlist->size();
	if (chaincount == 0)
		return;

	int bins = 500;

	//vector<vector<double> > distanceToPast(chaincount);
	vector < vector<double> > alpha(chaincount);
	vector < vector<double> > gamma(chaincount);
	vector < vector<double> > bias(chaincount);
	vector < vector<double> > variance(chaincount);
	vector < vector<double> > totalLinkCount(chaincount);
	vector < vector<double> > traceTotalLinkCount(chaincount);
	vector < vector<double> > likelihood(chaincount);
	vector < vector<double> > movingAvgAlpha(chaincount);
	vector < vector<double> > movingAvgGamma(chaincount);
	vector < vector<double> > movingAvgTLC(chaincount);

	vector<vector<double> > biasmean(chaincount);

	unsigned maxSensibleTLC = pow((double) (*runlist)[0].R.size(), 2)/2;

	vector<double> bestllforTLC;
	vector<Model*> bestmodelforTLC;

	//	queue<Features> cumMean;
	//	Aggregation cumAgg;
	int totalcount = 0;
	unsigned maxsize = 0;
	unsigned realmax = 0;
	int interval = calculateInterval(runlist, maxsize, realmax);

	vector<double> cumalpha(chaincount);
	vector<double> cumgamma(chaincount);
	vector<double> cumbias(chaincount);
	vector<double> cumnoise(chaincount);
	vector<double> cumtlc(chaincount);
	vector<double> cumll(chaincount);

	vector<double> mvgalpha(chaincount);
	vector<double> mvggamma(chaincount);
	vector<double> mvgtlc(chaincount);

	vector<double> means(6, 0);
	vector<double> minvalue(6,numeric_limits<double>::max());
	vector<double> maxvalue(6,-numeric_limits<double>::max());

	for (unsigned c =  0; c < runlist->size(); c++)  {
		assert((*runlist)[c].chain.size() > 0);
		mvgalpha[c] = (*runlist)[c].chain[0].alpha;
		mvggamma[c] = (*runlist)[c].chain[0].gamma;
		mvgtlc[c] = (*runlist)[c].chain[0].getTotalLinkCount();
	}

	for (unsigned i = 0; i < maxsize; i++) {
		for (unsigned c = 0; c < runlist->size(); c++) {
			if ((*runlist)[c].chain.size() <= i) continue;
			//Features model = (*runlist)[c].chain[i];
			//Settings settings = (*runlist)[c].getSettings();
			/**
			 * get values
			 */
			vector<double> values = extractValues(runlist, c, i);

			/**
			 * update cumulative means
			 */
			cumalpha[c] = (cumalpha[c] * i + values[0]) / (i+1);
			cumgamma[c] = (cumgamma[c] * i + values[1]) / (i+1);
			cumbias[c] = (cumbias[c] * i + values[2]) / (i+1);
			cumnoise[c] = (cumnoise[c] * i + values[3]) / (i+1);
			cumtlc[c] = (cumtlc[c] * i + values[4]) / (i+1);
			cumll[c] = (cumll[c] * i + values[5]) / (i+1);

			/**
			 * update min/max
			 */
			for (int p = 0; p < 6; p++) {
				if (values[p] < minvalue[p]) minvalue[p] = values[p];
				if (values[p] > maxvalue[p]) maxvalue[p] = values[p];
			}

			/**
			 * update best fit for TLC
			 */
			double tlc = values[4];
			if (tlc < 2*maxSensibleTLC) {
				if (bestllforTLC.size() <= tlc) {
					bestllforTLC.resize(tlc+1, -numeric_limits<double>::infinity());
					bestmodelforTLC.resize(tlc+1);
				}

				if (values[5] > bestllforTLC[tlc]) {
						bestllforTLC[tlc] = values[5];
						bestmodelforTLC[tlc] = &(*runlist)[c].chain[i];
				}
			}

			/**
			 * update plotting vectors
			 */

			if (i % interval == 0) {
				alpha[c].push_back(cumalpha[c]);
				gamma[c].push_back(cumgamma[c]);
				movingAvgAlpha[c].push_back((mvgalpha[c]));
				movingAvgGamma[c].push_back(mvggamma[c]);
				bias[c].push_back(values[2]);
				biasmean[c].push_back(cumbias[c]);
				variance[c].push_back(values[3]);
				traceTotalLinkCount[c].push_back(values[4]);
				totalLinkCount[c].push_back(cumtlc[c]);
				movingAvgTLC[c].push_back(mvgtlc[c]);
				likelihood[c].push_back(values[5]);
			}

			/**
			 * update moving average
			 */

			unsigned m = i % interval;
			mvgalpha[c] = (mvgalpha[c] * m + (*runlist)[c].chain[i].alpha) / (m+1);
			mvggamma[c] = (mvggamma[c] * m + (*runlist)[c].chain[i].gamma) / (m+1);
			mvgtlc[c] = (mvgtlc[c] * m + (*runlist)[c].chain[i].getTotalLinkCount()) / (m+1);

			/**
			 * output progress indicator
			 */

			totalcount++;
			if (totalcount % 1000 == 0) {
				cout << ".";
				cout.flush();
			}
		}
		for (unsigned c = 0; c < 6; c++) {
			means[c] = 0;
		}
		for (unsigned c = 0; c < chaincount; c++) {
			unsigned totalsize = runlist->at(c).chain.size();
			int chainsize = std::min(totalsize, i);
			means[0] += (cumalpha[c] * chainsize)  / totalcount;
			means[1] += (cumgamma[c] * chainsize) / totalcount;
			means[2] += (cumbias[c] *chainsize)  / totalcount;
			means[3] += (cumnoise[c] *chainsize)/ totalcount;
			means[4] += (cumtlc[c] *chainsize) / totalcount;
			means[5] += (cumll[c] *chainsize) / totalcount;
		}
	}

	/*
	 * calculation of best instance for each complexity
	 */
	Model empty;
	empty.emptyModel((*runlist)[0].R.size(), 0, 0, 0);
	double localbest = Model::logLikelihood(&(*runlist)[0].R, &empty.featurePresent, &empty.clusterAssignment, &empty.weights, empty.s, 1);
	Model * localbestmodel = &empty;
	for (unsigned i = 0; i < bestllforTLC.size(); i++) {
		double storedll = bestllforTLC[i];
		if (storedll > localbest) {
			localbest = bestllforTLC[i];
			localbestmodel = bestmodelforTLC[i];
		} else if (localbest > storedll) {
			bestllforTLC[i] = localbest;
			bestmodelforTLC[i] = localbestmodel;
		}
	}

	/**
	 * preparation of covariance and correlation matrix
	 */

	vector<double> controlmeans(6);
	vector<vector<double> > covariances(6);
	vector<double> variances(6);
	for (unsigned i = 0; i < 6; i++) {
		covariances[i].resize(6, 0);
	}

	/**
	 * preparation of histogramm
	 *
	 * first dimension: parameter
	 * second dimension:chain
	 * third dimension:  bin
	 */
	vector<vector<vector<double> > > histogramm(6);
	for (int p = 0; p < 6; p++) {
		histogramm[p].resize(chaincount);
		for (unsigned c = 0; c < chaincount; c++) {
			histogramm[p][c].resize(bins);
		}
	}

	vector<double> stepsize(6);
	for (int p = 0; p < 6; p++) {
		stepsize[p] = (maxvalue[p] - minvalue[p]) / bins;
		if (stepsize[p] == 0) stepsize[p] = numeric_limits<double>::min();
	}

	//second pass, creating histogramm and covariance matrix
	for (unsigned c = 0; c < chaincount; c++) {
		for (unsigned i = 0; i < runlist->at(c).chain.size(); i++) {
			vector<double> values = extractValues(runlist, c, i);

			for (int p = 0; p < 6; p++) {
				/**
				 * fill histogramm
				 */
				int binnumber = (values[p] - minvalue[p]) / stepsize[p];
				if (binnumber == bins) binnumber--;
				assert(binnumber < bins);
				assert(binnumber >= 0);
				histogramm[p][c][binnumber]++;

				/**
				 * calculate covariances
				 */
				variances[p] += (values[p] - means[p])*(values[p] - means[p]) / totalcount;
				controlmeans[p] += values[p] / totalcount;
				for (int k = 0; k < 6; k++) {
					covariances[p][k] += ((values[p] - means[p])*(values[k] - means[k])) / totalcount;
				}
			}
		}
	}

	for (int i = 0; i < 6; i++) variances[i] = sqrt(variances[i]);

	vector<vector<double> > correlations(6);
	for (int i = 0; i < 6; i++) {
		correlations[i].resize(6);
		//cout << "means[" << i << "] = " << means[i] << ", controlmeans[" << i << "] =  " << controlmeans[i] << endl;
		for (int j = 0; j < 6; j++) {
			if (variances[i] == 0 || variances[j] == 0) correlations[i][j] = 0;
			else correlations[i][j] = covariances[i][j] / (variances[i]*variances[j]);
			//cout << "covariances[" << i << "][" << j << "] / variances[" << i << "] * variances[" << j << "] = " << covariances[i][j] << "/ (" << variances[i] << "*" << variances[j] << ") = " << correlations[i][j] << endl;
		}
	}

	/**
	 * plotting of results
	 */



	cout << endl;
	// Means
	Plotting::plotMultiple(&alpha, 0, realmax, "Alpha-mean");
	Plotting::plotMultiple(&gamma, 0, realmax, "Gamma-mean");
	Plotting::plotMultiple(&biasmean, 0, realmax, "Bias-mean");
	Plotting::plotMultiple(&totalLinkCount, 0, realmax, "Total-Link-Count-of-average");

	//Moving Averages
	Plotting::plotMultiple(&movingAvgAlpha, 0, realmax, "Alpha-moving-avg");
	Plotting::plotMultiple(&movingAvgGamma, 0, realmax, "Gamma-moving-avg");
	Plotting::plotMultiple(&movingAvgTLC, 0, realmax, "Total-Link-Count-moving-avg");

	// Traces
	Plotting::plotMultiple(&bias, 0, realmax, "Bias-of-trace");
	Plotting::plotMultiple(&variance, 0, realmax, "Variance-of-trace");
	Plotting::plotMultiple(&traceTotalLinkCount, 0, realmax, "Total-Link-Count-of-trace");
	Plotting::plotMultiple(&likelihood, 0, realmax, "Likelihood-of-trace");

	//Correlations and Covariances
	Plotting::plotHistogramm(bestllforTLC, "Likelihood-vs-TLC");
	Plotting::plotMatrix(covariances, "Covariances");
	Plotting::plotMatrix(correlations, "Correlations");

	//Histogramms
	vector<string> names(6);
	names[0] = "Alpha";
	names[1] = "Gamma";
	names[2] = "Bias";
	names[3] = "Noise";
	names[4] = "TLC";
	names[5] = "Likelihood";
	for (int p = 0; p < 6; p++) Plotting::plotMultiple(&histogramm[p], minvalue[p], maxvalue[p], names[p] + string("-histogramm"), 0, false);
}

void Analysis::analyze(vector<Sampler> * runlist, Model orig, Settings settings) {
	analyze(runlist, settings);
	unsigned chaincount = runlist->size();
	if (chaincount == 0) return;
	vector<vector<double> > tracenmiAvg(chaincount);
	vector<vector<double> > nmiAvg(chaincount);
	vector<vector<double> > purityFF(chaincount);
	vector<vector<double> > nmiFF(chaincount);
	vector<vector<double> > tracepurityFF(chaincount);
	vector<vector<double> > tracenmiFF(chaincount);
	vector<vector<double> > distanceToOrig(chaincount);
	vector<Aggregation> cumAgg(chaincount);
	vector<vector<double> > tlcAggregation(chaincount);

	ClusterMatrix labels = orig.getClusterMatrix();
	int extOffset = 0;
	if (labels[0].size() != 0) extOffset = Util::containsNegative(labels, 0) ? 1 : 0;

	int totalcount = 0;
	unsigned maxsize = 0;
	unsigned realmax = 0;
	int interval = calculateInterval(runlist, maxsize, realmax);

	for (unsigned c = 0; c < runlist->size(); c++) {
		cumAgg[c] = Aggregation(runlist->at(c).chain[0]);
	}

	for (unsigned i = 0; i < maxsize; i++) {
		for (unsigned c = 0; c < runlist->size(); c++) {
			if ((*runlist)[c].chain.size() <= i) continue;
			cumAgg[c] = Aggregation::aggregate(cumAgg[c], i, Aggregation(runlist->at(c).chain[i]), 1);

			if (i % interval == 0) {
				Model model = (*runlist)[c].chain[i];
				ClusterMatrix traceclusters = model.getClusterMatrix();

				Model collapsed = cumAgg[c].collapse(settings);
				ClusterMatrix clusters = collapsed.getClusterMatrix();

				vector<double> aggavgcache = Util::averagePurityAndNMIMax(clusters, labels);
				nmiAvg[c].push_back(aggavgcache[1]);

				vector<double> avgcache = Util::averagePurityAndNMIMax(traceclusters, labels);
				tracenmiAvg[c].push_back(avgcache[1]);

				int ownOffset = 0;
				if (traceclusters[0].size() != 0) ownOffset = Util::containsNegative(traceclusters, 0) ? 1 : 0;
				double tracepurity, tracenmi;
				if (traceclusters[0].size() == 0) {
					tracepurity = (double) ((labels[0].size() == 0)); //1 if same, 0 if different
					tracenmi = (double) ((labels[0].size() == 0));
				} else {
					ownOffset = Util::containsNegative(traceclusters, 0);
					if (labels[0].size() == 0) {
						tracepurity = 0;
						tracenmi = 0;
					} else {
						vector<double> trcache = Util::balancedPurityandNMI(Util::matslice(&traceclusters, 0, ownOffset),
									Util::matslice(&labels, 0, extOffset));
						tracepurity = trcache[0];
						tracenmi = trcache[1];
					}
				}
				tracepurityFF[c].push_back(tracepurity);
				tracenmiFF[c].push_back(tracenmi);

				int aggOffset = 0;
				if (clusters[0].size() != 0) aggOffset = Util::containsNegative(clusters, 0) ? 1 : 0;
				double purity, nmi;
				if (clusters[0].size() == 0) {
					purity = (double) ((labels[0].size() == 0)); //1 if same, 0 if different
					nmi = (double) ((labels[0].size() == 0));
				} else {
					aggOffset = Util::containsNegative(clusters, 0);
					if (labels[0].size() == 0) {
						purity = 0;
						nmi = 0;
					} else {
						vector<double> cache = Util::balancedPurityandNMI(Util::matslice(&clusters, 0, aggOffset),
													Util::matslice(&labels, 0, extOffset));
						purity = cache[0];
						nmi = cache[1];
					}
				}
				purityFF[c].push_back(purity);
				nmiFF[c].push_back(nmi);

				double distance = Model::computeDistance(&model, &orig);
				distanceToOrig[c].push_back(distance);

				tlcAggregation[c].push_back(collapsed.getTotalLinkCount());
			}

			totalcount++;
			if (totalcount % 1000 == 0) {
				cout << ".";
				cout.flush();
			}
		}
	}

	Aggregation final = cumAgg[0];
	for (unsigned c = 0; c < cumAgg.size(); c++) {
		final = Aggregation::aggregate(final, c, cumAgg[c], 1);
	}
	Settings fSettings = runlist->at(0).getSettings();
	Model finalModel = final.collapse(settings);
	ModelIO::writeModel(settings, "Aggregated" + std::to_string(totalcount), finalModel, false);
	ModelIO::exportToCSV(settings, "Aggregated" + std::to_string(totalcount), finalModel, false);

	printPurity(labels, finalModel);
	printNMI(labels, finalModel);
	printDistances(orig, finalModel);

	for (FeatureID m = 0; m < finalModel.getFeatureCount(); m++) {
		Plotting::exportToGraphviz(finalModel, runlist->at(0).R, "Aggregated" + std::to_string(totalcount) + "-" + fSettings.getVersionString() + "-" + std::to_string(m), m, false);
	}

	Plotting::plotMultiple(&purityFF, 0, realmax, "First-Features-purity-of-aggregated-mean");
	Plotting::plotMultiple(&nmiFF, 0, realmax, "First-Features-nmi-of-aggregated-mean");
	Plotting::plotMultiple(&nmiAvg, 0, realmax, "Average-NMI-of-aggregated-mean");
	Plotting::plotMultiple(&tracenmiAvg, 0, realmax, "Average-NMI-of-trace");
	Plotting::plotMultiple(&tracepurityFF, 0, realmax, "First-Features-purity-of-trace");
	Plotting::plotMultiple(&tracenmiFF, 0, realmax, "First-Features-nmi-of-trace");
	Plotting::plotMultiple(&distanceToOrig, 0, realmax, "Distance-to-Original");
	Plotting::plotMultiple(&tlcAggregation, 0, realmax, "TLC-of-aggregated-mean");
}

} /* namespace io */
} /* namespace CILA */
