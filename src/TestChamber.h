/*
 * TestChamber.h
 *
 *  Created on: 31.05.2013
 *      Author: moritz
 */

#ifndef TESTCHAMBER_H_
#define TESTCHAMBER_H_

#include "sampler.h"
#include "Aggregation.h"
#include "Settings.h"
#include "Graph.h"
#include "Model.h"

namespace CILA {

class TestChamber {
public:
	TestChamber();
	virtual ~TestChamber();
	static Aggregation directAggregation(Model model, Graph graph, Settings settings, string name = "run");
	static Sampler testrun(Model model, Graph graph, Settings settings, string name = "run");
	static vector<Aggregation> fixedRun(Model def, Graph R, bool alpha, bool gamma, bool z, bool c, bool w, bool s, bool sigma, Settings settings);
	static void fixedRuns(Settings settings);
	static double averageQuality(Settings settings, int graphiterations = 20, double maxnoise = 5, bool samplealpha = false, bool samplegamma = false);
	static void parametermap(double alphamin, double alphamax, double gammamin, double gammamax, double precision, Settings settings);
	static void biasmap(double min, double max, double precision, Settings settings);
	static void singleParmap(double min, double max, double precision, int parameter, Settings settings);
};

} /* namespace CILA */
#endif /* TESTCHAMBER_H_ */
