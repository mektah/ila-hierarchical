/*
 * gibbs.h
 *
 *  Created on: 12.12.2012
 *      Author: moritz
 */

#ifndef GIBBS_H_
#define GIBBS_H_

#include <vector>
#include <boost/random.hpp>
#include <map>
#include <cereal/access.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/base_class.hpp>

#include "Graph.h"
#include "types.h"
#include "generative.h"
#include "Model.h"
#include "PDFunctor.h"
#include "Settings.h"

namespace CILA {
namespace core {

class Sampler {
	/*
	 * This class handles inverting the model. A Markov chain is created modeling the posterior distribution of Z, C and W given R.
	 * The state of the chain is defined by Z, C, W, Alpha, Gamma, Bias and Variance.
	 * To advance the chain by one step, call sampleStepZandC, sampleStepW, sampleHyperAlpha, sampleHyperGamma, sampleHyperBias and sampleHyperVariance.
	 *
	 * For sequential initialization, construct the sampler with a subgraph and call addNode and the sampling steps for every remaining node.
	 */
public:
	Sampler(Graph g, Model f, Settings settings = Settings::getDefaultSettings());
	Sampler(Graph g, ClusterMatrix c, WeightMatrices w, Settings settings = Settings::getDefaultSettings());
	Sampler(Graph g, Settings settings = Settings::getDefaultSettings());
	Sampler();
	void init(Graph g, Model f, Settings settings);
	void sampleStepZandC();
	FeatureMatrix getZ();
	ClusterMatrix getC();
	void sampleStepW();
	WeightMatrices getW();
	Model getModel();
	void sampleHyperAlpha();
	double getAlpha();
	void sampleHyperGamma(double precision = 0.01);
	double getGamma();
	void sampleHyperBias();
	double getBias();
	void sampleHyperVariance(double precision = 0.01);
	double getVariance();
	vector<int> sampleNode(NodeID i, int m_aux,
			bool newFeaturesAllowed = false);
	void clearNode(NodeID i);

	Settings getSettings();

	void sampleIter();

	//maybe move the following block to features
	void purgeUnusedFeatures();
	void purgeEmptyClusters();
	void countClusters();
	void clusterSanityCheck();

	void addNode(vector<edgetype> connections);
	void permute(vector<NodeID> permutation);
	int featureCount();
	int totalClusterCount();

	void saveChainState();
	vector<Model> getChain();
	Model getLastChainlink();

	vector<std::pair<ClusterMatrix, int> > generateClusterStatistics();

	static double loginverseTransformSampling(ProbabilityDensityFunctor* logpdf,
			double leftBoundary, double rightBoundary, double stepsize = 0.1);
	static double sliceSample(ProbabilityDensityFunctor* pdf, double initial,
			int iterations = 1, double w = 0.1);

	//move to Util

	FeatureMatrix featurePresent; //Z in PalKnoGha
	ClusterMatrix clusterAssignment; //C in PalKnoGha
	WeightMatrices weights; //W in PalKnoGha
	Graph R;
	vector<Model> chain;

private:
	friend class cereal::access;
	bool sAlpha, sGamma, sBias, sZ, sC, sW, sVariance, sStates;
	NodeID nodeCount;
	double alpha;
	double gamma;
	double bias;
	double variance;
	double mu_w, sigma_w, mu_bias, sigma_bias;
	vector<int> featurePopulation; //redundant, made for easier access;
	vector<vector<int> > subClusterPopulation;

	ClusterID countEmptyClusters(FeatureID m);
	vector<double> decideClusterWithAux(NodeID i, FeatureID m, int m_aux,
			bool oldClusters);
	void step_z_im(FeatureID m, NodeID i, int m_aux, bool retryOld);

	bitPool compressNode(NodeID i);
	vector<bitPool> compressZ();
	bool isSet(bitPool result, FeatureID m);
	void purgeEmptyClusters(FeatureID m);

	template<class Archive>
	    void serialize(Archive & ar)
	    {
	        ar(sAlpha);
	        ar(sGamma);
	        ar(sBias);
	        ar(sZ);
	        ar(sC);
	        ar(sW);
	        ar(sVariance);
	        ar(nodeCount);
	        ar(alpha);
	        ar(gamma);
	        ar(bias);
	        ar(variance);
	        ar(mu_w);
	        ar(sigma_w);
	        ar(mu_bias);
	        ar(sigma_bias);
	        ar(chain);
	        ar(param);
	        ar(featurePresent);
	        ar(clusterAssignment);
	        ar(weights);
	        ar(R);
	    }

	int tClusterCount;
	vector<vector<double> > T;
	double ll;
	Settings param;
	int steps;
	vector<bitPool> compressedZ;
};

}//end namespace core
}//end namespace CILA

#endif /* GIBBS_H_ */
