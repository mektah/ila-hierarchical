/*
 * stochastics.h
 *
 *  Created on: 01.12.2012
 *      Author: moritz
 */

#ifndef STOCHASTICS_H_
#define STOCHASTICS_H_

#include <vector>
#include "types.h"
#include "Settings.h"

using std::vector;

namespace CILA {
namespace core {

//sets seed of random number generator
void setSeed(unsigned seed);

//these methods know an awful lot about the internal feature representation.
//Maybe simplify them based on unix philosophy?
FeatureMatrix sampleFeatureMatrixwithIBP(size_t n, float alpha);
ClusterMatrix sampleClusterAssignmentsbyCRP(size_t n, double gamma, FeatureMatrix features);
WeightMatrices sampleWeightMatrices(ClusterMatrix clusterAssignments, double mu_w, double sigma_w);
ClusterID seatSingleCRP(ClusterID clusters, vector<int> seatedGuests, int totalGuests, double gamma, int previouscluster);

/**sample from probability distributions for reals*/
double sampleNormal(double mu_s, double sigma_s);
double gammaProbabilityDensity(double shape, double scale, double x) __attribute__((const));
double normalProbabilityDensity(double mu, double sigma, double x) __attribute__((const));
double normalLogProbabilityDensity(double mu, double sigma, double x) __attribute__((const));
double sampleGamma(double shape, double scale);
FeatureID samplePoisson(double a);
double sampleUniform(double a = 0, double b = 1);

/**select randomly from vector*/
int selectUniform(vector<double> a);
int selectUniformLog(vector<double> a);

/**
 * helper functions
 */
vector<double> logNormalize(vector<double> a) __attribute__((const));
double normalize(double a, double b) __attribute__((const));
vector<double> normalize(vector<double> a) __attribute__((const));

}
}

#endif /* STOCHASTICS_H_ */
