/*
 * Util.cpp
 *
 *  Created on: 02.04.2013
 *      Author: moritz
 */

#include <cmath>
#include <iostream>
#include <limits>
#include <algorithm>

#include "Util.h"
#include "generative.h"

 using std::cout;
 using std::endl;
 using std::cerr;
 using std::min;

namespace CILA {
namespace core {

vector<NodeID> Util::randPermutation(NodeID n) {
	/**
	 * Returns a vector of size n with the numbers 0 .. n-1 permuted randomly.
	 */
	vector<NodeID> result;
	vector<NodeID> reservoir(n);
	//fill reservoir
	for (NodeID i = 0; i < n; i++) {
		reservoir[i] = i;
	}
	for (NodeID i = 0; i < n; i++) {
		int index = sampleUniform(0, reservoir.size());
		result.push_back(reservoir[index]);
		reservoir.erase(reservoir.begin()+index);
	}
	return result;
}

vector<NodeID> Util::dummyPermutation(NodeID n) {
	/**
	 * Returns a vector of size n with the numbers 0 .. n-1 in correct order.
	 */
	vector<NodeID> result(n);

	for (NodeID i = 0; i < n; i++) {
		result[i] = i;
	}
	return result;
}

bool CILA::core::Util::equals(vector<NodeID> perm1, vector<NodeID> perm2) {
	assert(perm1.size() == perm2.size());
	for (NodeID i = 0; i < perm1.size(); i++) {
		if (perm1[i] != perm2[i]) return false;
	}
	return true;
}

vector<NodeID> Util::reverse(vector<NodeID> perm) {
	vector<NodeID> result(perm.size());
	for (NodeID i = 0; i < result.size(); i++) {
		result[perm[i]] = i;
	}
	return result;
}

double Util::anova(vector<int> ownSlice, vector<double> values) {
	/*
	 * only computed for the nodes sorted into a cluster. No dummy clusters here!
	 */
	int clusterCount = 0;
	NodeID nodeCount = ownSlice.size();
	assert(values.size() == nodeCount);

	//count clusters
	for (NodeID i = 0; i < nodeCount; i++) {
		if (ownSlice[i] >= clusterCount) clusterCount = ownSlice[i]+1;
	}

	//get means for each cluster and global mean
	double globalmean = 0;
	vector<double> means(clusterCount, 0);
	vector<int> population(clusterCount, 0);
	int participating = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		if (ownSlice[i] >= 0) {
			means[ownSlice[i]] += values[i];
			population[ownSlice[i]]++;
			globalmean += values[i];
			participating++;
		}
	}
	globalmean = globalmean / participating;

	double soSquares = 0;
	for (ClusterID i = 0; i < means.size(); i++) {
		means[i] = means[i] / population[i];
		//get variance of means
		soSquares += population[i] * pow(means[i] - globalmean, 2);
	}

	//get total variance
	double totalVar = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		if (ownSlice[i] >= 0) {
			totalVar += pow(values[i] - globalmean, 2);
		}
	}
	return soSquares / totalVar;
}

vector<double> Util::pValuesPurityAndNMI(vector<int> clusters, vector<int> external, int samplesize) {
	vector<double> result(2);
	assert(clusters.size() == external.size());
	NodeID nodeCount = clusters.size();
	vector<double> purityComparison;
	vector<double> nmiComparison;
	for (int i = 0; i < samplesize; i++) {
		vector<NodeID> randperm = Util::randPermutation(nodeCount);
		vector<int> randclusters = Util::apply(randperm, clusters);
		vector<double> cache = Util::balancedPurityandNMI(randclusters, external);
		double purity = cache[0];
		double nmi = cache[1];
		purityComparison.push_back(purity);
		nmiComparison.push_back(nmi);
	}
	vector<double> cache = Util::balancedPurityandNMI(clusters, external);
	double realpurity = cache[0];
	double realnmi = cache[1];
	double biggerthanPurity = 0;
	double biggerthanNMI = 0;
	for (int i = 0; i < samplesize; i++) {
		if (realpurity > purityComparison[i]) biggerthanPurity++;
		if (realnmi > nmiComparison[i]) biggerthanNMI++;
	}
	result[0] = 1 - (biggerthanPurity / samplesize);
	result[1] = 1 - (biggerthanNMI / samplesize);
	return result;
}

double Util::pValueAnova(vector<int> clusters, vector<double> external, int samplesize) {
	assert(clusters.size() == external.size());
	NodeID nodeCount = clusters.size();
	vector<double> comparison;
	for (int i = 0; i < samplesize; i++) {
		vector<NodeID> randperm = Util::randPermutation(nodeCount);
		vector<int> randclusters = Util::apply(randperm, clusters);
		double anova = Util::anova(randclusters, external);
		comparison.push_back(anova);
	}
	double realanova = Util::anova(clusters, external);
	double biggerthan = 0;
	for (int i = 0; i < samplesize; i++) {
		if (comparison[i] < realanova) biggerthan++;
	}
	return 1 - (biggerthan / samplesize);
}

double Util::computeNMI(vector<int> a, vector<int> b) {
	return balancedPurityandNMI(a,b)[1];
}

vector<double> Util::balancedPurityandNMI(const vector<int> &clusterSlice,
		const vector<int> &externalLabels) {
	NodeID nodeCount = clusterSlice.size();
	assert(nodeCount == externalLabels.size());

	assert(nodeCount > 0);

	ClusterID numOfFoundClusters = *std::max_element(clusterSlice.begin(), clusterSlice.end()) + 1;
	ClusterID numOfExternalClusters = *std::max_element(externalLabels.begin(), externalLabels.end()) + 1;
	assert(numOfFoundClusters <= nodeCount +1);
	assert(numOfExternalClusters <= nodeCount +1);

	vector<vector<int> > assignment(numOfFoundClusters);
	for (ClusterID j = 0; j < numOfFoundClusters; j++) {
		assignment[j].resize(numOfExternalClusters, 0);
	}

	assert(numOfFoundClusters > 0);
	assert(numOfExternalClusters > 0);

	vector<int> externalClusterPopulation(numOfExternalClusters, 0);
	vector<int> foundClusterPopulation(numOfFoundClusters, 0);

	for (NodeID i = 0; i < nodeCount; i++) {
		assignment[clusterSlice[i]][externalLabels[i]]++;
		foundClusterPopulation[clusterSlice[i]]++;
		externalClusterPopulation[externalLabels[i]]++;
	}

	/**
	 * compute purity
	 */

	double purity = 0;
	for (ClusterID i = 0; i < numOfFoundClusters; i++) {
		purity += *std::max_element(assignment[i].begin(), assignment[i].end());
	}

	double maxExternal = externalClusterPopulation[0];
	for (ClusterID j = 0; j < numOfExternalClusters; j++) {
		if (maxExternal < externalClusterPopulation[j]) maxExternal = externalClusterPopulation[j];
	}

	purity = purity / nodeCount;
	double ratio = maxExternal / nodeCount;
	purity =  (1-((double) 1/numOfExternalClusters))*(purity-ratio)/(1-ratio) + ((double) 1/numOfExternalClusters);

	/**
	 * compute NMI
	 */
	double homega = 0;//homega is H(omega), entropy of the found clusters
	for (ClusterID j = 0; j < numOfFoundClusters; j++) {
		assert(foundClusterPopulation[j] > 0);
		homega += ((double) -foundClusterPopulation[j] / nodeCount) * log( (double) foundClusterPopulation[j]/nodeCount);
	}

	//TODO: maybe put this in an external function (double entropy(vector<double> input))
	double hc = 0;//entropy of external clusters
	for (ClusterID j = 0; j < numOfExternalClusters; j++) {
		assert(externalClusterPopulation[j] >= 0);
		if (externalClusterPopulation[j] == 0) {
			cout << "externalClusterPopulation[" << j << "] == 0!" << endl;
			cout << "Skipping - for now!" << endl;
			continue;
		}
		assert(externalClusterPopulation[j] > 0);
		hc += ((double) -externalClusterPopulation[j] / nodeCount) * log( (double) externalClusterPopulation[j] / nodeCount);
	}

	double mutualInformation = 0;
	for (ClusterID j = 0; j < numOfFoundClusters; j++) {
		for (ClusterID k = 0; k < numOfExternalClusters; k++) {
			if (assignment[j][k] != 0) {
				double assjk = assignment[j][k];
				double fcP = foundClusterPopulation[j];
				double ecP = externalClusterPopulation[k];
				mutualInformation += assjk/nodeCount * log(nodeCount*assjk / (fcP*ecP));
			}
		}
	}

	double nmi;
	if (hc + homega == 0) {
		nmi = NAN;
	} else {
		nmi = 2*mutualInformation / (hc + homega);
	}
	double redundant = 2*min(hc, homega)/(hc + homega);

	vector<double> result(3);
	result[0] = purity;
	result[1] = nmi;
	result[2] = redundant;
	return result;
}

vector<double> Util::averagePurityAndNMIMax(ClusterMatrix &ownlabels, ClusterMatrix &externalLabels) {
	FeatureID ownfcount = ownlabels[0].size();
	FeatureID extfcount = externalLabels[0].size();

	double nmisum = 0;
	double puritysum = 0;

	for (FeatureID m = 0; m < extfcount; m++) {
		int extOffset = Util::containsNegative(externalLabels, m) ? 1 : 0;
		vector<int> extSlice = Util::matslice(&externalLabels, m, extOffset);
		double nmimax = 0;
		double puritymax = 0;
		for (FeatureID l = 0; l < ownfcount; l++) {
			int ownOffset = Util::containsNegative(ownlabels, l) ? 1 : 0;
			vector<int> ownSlice = Util::matslice(&ownlabels, l, ownOffset);
			vector<double> cache = Util::balancedPurityandNMI(ownSlice, extSlice);
			double purity = cache[0];
			double nmi = cache[1];
			if (purity > puritymax) puritymax = purity;
			if (nmi > nmimax) nmimax = nmi;
		}
		puritysum += puritymax;
		nmisum += nmimax;
	}
	vector<double> result(2);
	if (extfcount > 0) {
		result[0] = puritysum / extfcount;
		result[1] = nmisum / extfcount;
	} else {
		if (ownfcount == 0) {
			//no features were present, no were recognized. Splendid!
			result[0] = 1;
			result[1] = 1;
		} else {
			//oh boy!
			result[0] = 0;
			result[1] = 0;
		}
	}


	return result;
}

bool Util::containsNegative(ClusterMatrix &externalLabels, FeatureID l) {
	for (NodeID i = 0; i < externalLabels.size(); i++) {
		if (l >= externalLabels[i].size()) {
			cerr << l << ">=" << externalLabels[i].size() << "!" << endl;
		}
		assert(l < externalLabels[i].size());
		if (externalLabels[i][l] < 0) return true;
	}
	return false;
}

double Util::logSumExp(vector<double> logProbs) {
	/**
	 * To compute the sum of a vector with logarithms. Better numerical stability than taking exp first.
	 */
	assert(logProbs.size() > 0);
	double maximum = logProbs[0];
	for (unsigned i = 0; i < logProbs.size(); i++) {
		if (logProbs[i] > maximum) maximum = logProbs[i];
	}
	double sum = 0;
	for (unsigned i = 0; i < logProbs.size(); i++) {
		sum += exp(logProbs[i]-maximum);
	}
	return log(sum)+maximum;
}


double Util::sumMatrix(vector<vector<int> > triangle) {
	/**
	 * Sum the differences between two clustering solutions and print them.
	 */
	double sum = 0;
	for (NodeID i = 0; i < triangle.size(); i++) {
		for (NodeID j = 0; j < triangle[i].size(); j++) {
			sum += triangle[i][j];
		}
	}
	return sum;
}

vector<vector<double> > Util::transpose(vector<vector<double> > input) {
	unsigned xdim,ydim;
	if (input.size() == 0) {
		xdim = 0;
		ydim = 0;
	}
	else {
		xdim = input[0].size();
		ydim = input.size();
	}
	vector<vector<double> > result(xdim);
	for (unsigned i = 0; i < xdim; i++) {
		result[i].resize(ydim);
		for (unsigned j = 0; j < ydim; j++) {
			assert(input[j].size() > i);
			result[i][j] = input[j][i];
		}
	}

	return result;
}

double Util::normalizedSumOfsquaredDifference(vector<vector<double> > * a, vector<vector<double> > * b) {
	/**
	 *
	 */
	assert(a->size() == b->size());
	double sum = 0;
	for (NodeID i = 0; i < a->size(); i++) {
		assert((*a)[i].size() == (*a)[0].size());
		assert((*a)[i].size() == (*b)[i].size());
		for (NodeID j = 0; j < (*a)[i].size(); j++) {
			double diff = (*a)[i][j] - (*b)[i][j];
			sum += diff*diff;
		}
	}

	return sum / (a->size() > 0 ? a->size() * (*a)[0].size() : 0);
}

vector<vector<double> > Util::averageMatrices(vector<vector<vector<double> > > matrices, vector<double> weights) {
	vector<vector<double> > participating;
	vector<vector<double> > sum;

	unsigned total = matrices.size();
	assert(total == weights.size());

	for (unsigned i = 0; i < total; i++) {
		assert(weights[i] >= 0);
		vector<vector<double> > current = matrices[i];
		if (sum.size() < current.size()) {
			sum.resize(current.size());
			participating.resize(current.size());
		}
		for (ClusterID j = 0; j < current.size(); j++) {
			if (sum[j].size() < current[j].size()) {
				sum[j].resize(current[j].size(),0);
				participating[j].resize(current[j].size(), 0);
			}
			for (ClusterID k = 0; k < current[j].size(); k++) {
				sum[j][k] += current[j][k] * weights[i];
				participating[j][k] += weights[i];
			}
		}
	}

	for (ClusterID j = 0; j < sum.size(); j++) {
		for (ClusterID k = 0; k < sum[j].size(); k++) {
			sum[j][k] = sum[j][k] / participating[j][k];
		}
	}
	return sum;
}


}
}
 /* namespace std */
