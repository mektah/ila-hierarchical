/*
 * Serialization.cpp
 *
 *  Created on: 27.04.2015
 *      Author: moritzl
 */

#include <fstream>
#include <cereal/archives/binary.hpp>
#include <boost/algorithm/string.hpp>

#include "ModelIO.h"

using std::ifstream;
using std::ofstream;

namespace CILA {
namespace io {

using CILA::core::Settings;
using CILA::core::Model;
using CILA::core::Sampler;
using CILA::core::ClusterMatrix;
using CILA::core::FeatureID;
using CILA::core::NodeID;

	/**
	 * Reads the model at path.
	 * @Returns
	 * 	The model at path, if everything went right
	 * 	or an empty model object if an error occured.
	 */
Model ModelIO::readModel(string path) {
	Model model;
	std::ifstream ifs(path);
	if (!ifs) {
		throw std::runtime_error("Couldn't open " + path + '!');
	}

	/**
	 * We are purposely not catching possible exceptions, since we cannot solve them here.
	 * They are thrown up to the calling instance instead.
	 */
	cereal::BinaryInputArchive ia(ifs);
	if (boost::ends_with(path, ".run")) {
		throw std::runtime_error("The file at " + path + " has suffix .run, not loading it.");
	}
	if (boost::ends_with(path, ".model")) {
		ia >> model;
	}
	return model;
}

string ModelIO::writeModel(Settings &settings, string name, Model & model, bool preciseName) {
	int index = 0;
	string filename;

	#pragma omp critical
	{
		cout << "Writing model ... " << endl;
		if (!preciseName) {
			bool exists;
			do {
				exists = false;
				filename = settings.getVersionString() + std::string("-") + name + std::string("-") + std::to_string(index) + std::string(".model");
				ifstream ifs;
				ifs.open(filename.c_str());
				if (ifs) {
					ifs.close();
					exists = true;
				}
				index++;
			} while (exists);
		} else {
			filename = name;
		}

		ofstream ofs;
		ofs.open(filename.c_str());
		cereal::BinaryOutputArchive oa(ofs);
		// write class instance to archive
		oa << model;
		ofs.close();
		cout << "Model " << filename << " written." << endl;
	}
	return filename;
}

Aggregation ModelIO::readAggregation(string path) {
	Aggregation agg;
	std::ifstream ifs(path);
	if (!ifs) {
		throw std::runtime_error("Couldn't open " + path + '!');
	}

	/**
	 * We are purposely not catching possible exceptions, since we cannot solve them here.
	 * They are thrown up to the calling instance instead.
	 */
	cereal::BinaryInputArchive ia(ifs);
	if (boost::ends_with(path, ".run")) {
		throw std::runtime_error("The file at " + path + " has suffix .run, not loading it.");
	}
	if (boost::ends_with(path, ".model")) {
		throw std::runtime_error("The file at " + path + " has suffix .model, not loading it.");
	}
	if (boost::ends_with(path, ".agg")) {
		ia >> agg;
		cout << "Loaded Aggregation " << path << endl;
	}

	return agg;
}

string ModelIO::writeAggregation(Settings &settings, string name, Aggregation & agg, bool preciseName) {
	int index = 0;
	string filename;

	#pragma omp critical
	{
		cout << "Writing Aggregation ... " << endl;
		if (!preciseName) {
			bool exists;
			do {
				exists = false;
				filename = settings.getVersionString() + std::string("-") + name + std::string("-") + std::to_string(index) + std::string(".agg");
				ifstream ifs;
				ifs.open(filename.c_str());
				if (ifs) {
					ifs.close();
					exists = true;
				}
				index++;
			} while (exists);
		} else {
			filename = name;
		}

		ofstream ofs;
		ofs.open(filename.c_str());
		cereal::BinaryOutputArchive oa(ofs);
		// write class instance to archive
		oa << agg;
		ofs.close();
		cout << "Aggregation " << filename << " written." << endl;
	}
	return filename;
}

Sampler ModelIO::readSampler(string path) {
	Sampler result;
	std::ifstream ifs(path);
	if (!ifs) {
		throw std::runtime_error("Couldn't open " + path + '!');
	}

	/**
	 * We are purposely not catching possible exceptions, since we cannot solve them here.
	 * They are thrown up to the calling instance instead.
	 */
		cereal::BinaryInputArchive ia(ifs);
		if (boost::ends_with(path, ".model")) {
			throw std::runtime_error("The file at " + path + " has suffix .model, not loading it.");
		}
		if (boost::ends_with(path, ".run")) {
			ia >> result;
			cout << "Loaded Sampler " << path << endl;
		}

		return result;
}

string ModelIO::writeSampler(Settings &settings, string name, Sampler & sampler, bool preciseName) {
	int index = 0;
	string filename;
	#pragma omp critical
	{
		cout << "Writing chain " << name << "... " << endl;
		if (!preciseName) {
			bool exists;
			do {
				exists = false;
				filename = settings.getVersionString() + std::string("-") + name + std::string("-") + std::to_string(index) + std::string(".run");
				ifstream ifs;
				ifs.open(filename.c_str());
				if (ifs) {
					ifs.close();
					exists = true;
				}
				index++;
			} while (exists);
		} else {
			filename = name;
		}

		ofstream ofs;
		ofs.open(filename.c_str());
		cereal::BinaryOutputArchive oa(ofs);
		// write class instance to archive
		oa << sampler;
		ofs.close();
		cout << "Chain " << filename << " written." << endl;
	}
	return filename;
}

string ModelIO::writeSettings(Settings &settings, string name, bool preciseName) {
	int index = 0;
	string filename;
	#pragma omp critical
	{
		if (!preciseName) {
			bool exists;
			do {
				exists = false;
				filename = settings.getVersionString() + std::string("-") + name + std::string("-") + std::to_string(index) + std::string(".settings");
				ifstream ifs;
				ifs.open(filename.c_str());
				if (ifs) {
					ifs.close();
					exists = true;
				}
				index++;
			} while (exists);
		} else {
			filename = name;
		}

		ofstream ofs;
		ofs.open(filename.c_str());
		cereal::BinaryOutputArchive oa(ofs);
		// write class instance to archive
		oa << settings;
		ofs.close();
	}
	return filename;
}

Settings ModelIO::readSettings(string path) {
	Settings settings;
	std::ifstream ifs(path);
	if (!ifs) {
		throw std::runtime_error("Couldn't open " + path + '!');
	}

	/**
	 * We are purposely not catching possible exceptions, since we cannot solve them here.
	 * They are thrown up to the calling instance instead.
	 */
	cereal::BinaryInputArchive ia(ifs);
	if (!boost::ends_with(path, ".settings")) {
		throw std::runtime_error("The file at " + path + " does not have suffix .settings, not loading it.");
	}
	if (boost::ends_with(path, ".settings")) {
		ia >> settings;
		cout << "Loaded settings " << path << endl;
	}
	return settings;
}


string ModelIO::exportToCSV(Settings &settings, string name, Model & model, bool preciseName) {
	int index = 0;
		string filename;
		/*
		 * one could and probably should extract this to a function
		 */
		#pragma omp critical
		{
			cout << "Writing csv ... " << endl;
			if (!preciseName) {
				bool exists;
				do {
					exists = false;
					filename = settings.getVersionString() + std::string("-") + name + std::string("-") + std::to_string(index) + std::string(".csv");
					ifstream ifs;
					ifs.open(filename.c_str());
					if (ifs) {
						ifs.close();
						exists = true;
					}
					index++;
				} while (exists);
			} else {
				filename = name;
			}

			ofstream ofs;
			ofs.open(filename.c_str());
			ClusterMatrix toWrite = model.clusterAssignment;
			for (NodeID i = 0; i < toWrite.size(); i++) {
				ofs << i + 1;
				for (FeatureID j = 0; j < toWrite[i].size(); j++ ) {
					ofs << ", ";
					ofs << toWrite[i][j];
				}
				ofs << endl;
			}
			ofs.close();
			cout << "CSV " << filename << " written." << endl;
		}
		return filename;
}

}
} /* namespace CILA */
