/*
 * Aggregation.h
 *
 *  Created on: 26.05.2013
 *      Author: moritz
 */

#ifndef AGGREGATION_H_
#define AGGREGATION_H_

#include "Model.h"
#include "types.h"
#include "Settings.h"

typedef vector<vector<double> > extFeatureMatrix;
typedef vector<vector<vector<double> > > extClusterMatrix;
typedef vector<vector<vector<double> > > extWeights;

namespace CILA {
using namespace core;

class Aggregation {
public:
	Aggregation() = default;
	virtual ~Aggregation() = default;
	Aggregation(Model base);
	Aggregation(vector<Model> chainlinks);
	static Aggregation aggregate(Aggregation a, double weightA, Aggregation b, double weightB);
	static Aggregation aggregate(Model a, double weightA, Model b, double weightB);
	static Aggregation aggregate(vector<Aggregation> chainlinks, vector<double> weights);
	static Aggregation aggregate(vector<Model> chainlinks, vector<double> weights);
	static Aggregation aggregate(vector<Model> chainlinks, unsigned burnin = 0);

	Model collapse(double clusterthreshold, double featurethreshold, double sparsethrehold);
	Model collapseTo(FeatureID featureCount);
	Model collapse(Settings settings);

	vector<vector<vector<double> > > getCommonClusteringRatios() const {
		return extC;
	}

	static ClusterMatrix collapseToTwoOld(FeatureMatrix * Z, vector<vector<vector<double> > > connectedness);
	static WeightMatrices averageWeights(ClusterMatrix clusters, vector<vector<vector<double> > > redWeights);

	double alpha, gamma, s, sigma;

private:
	friend class cereal::access;
	NodeID nodeCount;
	FeatureID featureCount;

	vector<vector<double> > extZ;//extended Z, values can be in [0,1] instead of {0,1}
	vector<vector<vector<double> > > extC;//extended C, for each feature a n*n-matrix exists
	vector<vector<vector<double> > > extW;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar & nodeCount;
		ar & featureCount;
		ar & extZ;
		ar & extC;
		ar & extW;
		ar & alpha;
		ar & gamma;
		ar & s;
		ar & sigma;
	}

	void collapseClusters(FeatureMatrix& Z, FeatureID& f, ClusterMatrix& C,
			double clusterthreshold);
	void collapseToTwo(FeatureMatrix& Z, FeatureID& f, ClusterMatrix& C);
};


} /* namespace std */
#endif /* AGGREGATION_H_ */
