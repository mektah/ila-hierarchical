/*
 * Plotting.cpp
 *
 *  Created on: 18.03.2013
 *      Author: moritz
 */

#include <fstream>
#include <iostream>
#include <string>
#include <time.h>
#include <algorithm>

#include "Plotting.h"
#include "Util.h"

using std::max;
using std::min;
using std::numeric_limits;
using std::cerr;
using std::cout;
using std::endl;

namespace CILA {
using namespace core;
namespace io {

void Plotting::plotPDF(ProbabilityDensityFunctor* pdf, double leftBoundary,
		double rightBoundary, double stepsize) {
	/**
	 * Create a histogramm of the probability density function specified by pdf and plot it using gnuplot.
	 * Since pdf is evaluated for each step, this function might take a while.
	 */

	vector<double> density = evalPDF(pdf, leftBoundary, rightBoundary, stepsize);

	plotHistogramm(density, leftBoundary, rightBoundary);
}

vector<double> Plotting::evalPDF(ProbabilityDensityFunctor* pdf, double leftBoundary,
		double rightBoundary, double stepsize) {
	/**
	 * Create a histogramm of the probability density function specified by pdf
	 * Since pdf is evaluated for each step, this function might take a while.
	 */
	int steps = (rightBoundary - leftBoundary)/stepsize;

	vector<double> density(steps);
	double max = pdf->logpdf(leftBoundary);
	for (int i = 0; i < steps; i++) {
		double index = leftBoundary + i*stepsize;
		double value = pdf->logpdf(index);
		if (value > max) max = value;
		density[i] = value;
	}

	for (int i = 0; i < steps; i++) {
		density[i] = exp(density[i] - max);
	}
	return density;
}

void Plotting::plotLogPDF(ProbabilityDensityFunctor* pdf, double leftBoundary,
		double rightBoundary, double stepsize) {
	int steps = (rightBoundary - leftBoundary)/stepsize;

	vector<double> density(steps);
	for (int i = 0; i < steps; i++) {
		double index = leftBoundary + i*stepsize;
		double value = pdf->logpdf(index);
		density[i] = value;
	}
	plotHistogramm(density, leftBoundary, rightBoundary);
}

Plotting::~Plotting() {

}

void Plotting::plotHistogramm(vector<double> priorDensity, vector<double> posteriorDensity, double priorMean, double postMean, double leftBoundary, double rightBoundary, string name) {
	assert(priorDensity.size() == posteriorDensity.size());

	string datafilePrior = name + "-prior.dat";
	string datafilePosterior = name + "-posterior.dat";

	vector<double> minmax1 = Plotting::saveDatafile(priorDensity, leftBoundary, rightBoundary, datafilePrior);
	vector<double> minmax2 = Plotting::saveDatafile(posteriorDensity, leftBoundary, rightBoundary, datafilePosterior);

	double min = minmax1[0] < minmax2[0] ? minmax1[0] : minmax2[0];
	double max = minmax1[1] > minmax2[1] ? minmax1[1] : minmax2[1];

	//write plotting instructions to file
	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title 'Probability Density Function'" << endl;
	plothandle << "set terminal postscript" << endl;//TODO: maybe replace this with png
	plothandle << "set output '" << name << ".ps'" << endl;
	plothandle << "set xrange[" << leftBoundary << ":" << rightBoundary << "]" << endl;
	if (min != max) plothandle << "set yrange[" << min-0.2*abs(min) << ":" << 0.2*abs(max)+max << "]" << endl;
	plothandle << "plot '" << datafilePrior << "' using 1:2 with lines, \\" << endl;
	plothandle << "'" << datafilePosterior << "' using 1:2 with points" << endl;
	plothandle << "set arrow from " << priorMean << ",graph(0,0) to " << priorMean << ",graph(1,1) nohead" << endl;
	plothandle << "set arrow from " << postMean << ",graph(0,0) to " << postMean << ",graph(1,1) nohead" << endl;
	plothandle.close();//TODO: add try/catch and finally to close handle in case of failure

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;
}

void Plotting::plotHistogramm(vector<double> density, double leftBoundary, double rightBoundary, string name) {
	/**
	 *	Plots input array using gnuplot. The arguments leftBoundary and rightBoundary and name are needed for the labels.
	 */
	int steps = density.size();
	double stepsize = (rightBoundary - leftBoundary)/steps;
	string datafilename = name + ".dat";

	//write datapoints to file
	ofstream datahandle;
	datahandle.open(datafilename.c_str());
	double min = density[0];
	double max = density[0];
	for (int i = 0; i < steps; i++) {
		double index = leftBoundary + i*stepsize;
		double value = density[i];
		if (value > max) max = value;
		if (value < min) min = value;
		datahandle << index << " " << value << endl;
	}
	datahandle.close();

	//write plotting instructions to file
	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title '" << name << "'" << endl;
	plothandle << "set terminal postscript" << endl;//TODO: maybe replace this with png
	plothandle << "set output '" << name << ".ps'" << endl;
	plothandle << "set xrange[" << leftBoundary << ":" << rightBoundary << "]" << endl;
	if (min != max) plothandle << "set yrange[" << min-0.2*abs(min) << ":" << 0.2*abs(max)+max << "]" << endl;
	plothandle << "plot '" << datafilename << "' using 1:2 with points" << endl;
	plothandle.close();//TODO: add try/catch and finally to close handle in case of failure

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;
}

void Plotting::plotMultiple(vector<vector<double> > * chains, double leftBoundary, double rightBoundary, string name, double gt, bool plotgt) {
	unsigned maxsize = 0;

	if (chains->size() == 0) {
		cerr << "Can't plot emtpy chains!" << endl;
		return;
	}

	for (unsigned c = 0; c < chains->size(); c++) {
		double size = (*chains)[c].size();
		if (size > maxsize) maxsize = size;
	}

	double min = numeric_limits<double>::infinity();
	double max = -numeric_limits<double>::infinity();
	for (unsigned c = 0; c < chains->size(); c++) {
		string numberedfilename = name + std::to_string(c) + ".dat";
		double scaledRight = (rightBoundary * (*chains)[c].size()) / maxsize;
		vector<double> minmax = Plotting::saveDatafile((*chains)[c], leftBoundary, scaledRight, numberedfilename);
		if (minmax[0] < min) min = minmax[0];
		if (minmax[1] > max) max = minmax[1];
	}

	double spread = max - min;

	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title '" << name << "'" << endl;
	plothandle << "set terminal postscript" << endl;//TODO: maybe replace this with png
	plothandle << "set output '" << name << ".ps'" << endl;
	plothandle << "set xrange[" << leftBoundary << ":" << rightBoundary << "]" << endl;
	if (min != max) plothandle << "set yrange[" << min-0.2*spread << ":" << max+0.2*spread << "]" << endl;

	plothandle << "plot ";

	vector<string> colors = Plotting::getColors();
	while (colors.size() < chains->size()) {
		colors.push_back(Plotting::getOverflowColor());
	}

	for (unsigned c = 0; c < chains->size()-1; c++) {
		string numberedfilename = name + std::to_string(c) + ".dat";
		plothandle << " '" << numberedfilename << "' using 1:2 notitle with lines linecolor rgb " << '"' << colors[c] << '"' <<  ", \\" << endl;
	}
	if (plotgt) plothandle << gt << ", \\" << endl;
	plothandle << " '" << name << chains->size()-1 << ".dat" << "' using 1:2 notitle with lines linecolor rgb " << '"' << colors[chains->size()-1] << '"' << endl;

	plothandle.close();//TODO: add try/catch and finally to close handle in case of failure

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;
}

void Plotting::plotMatrix(vector<vector<double> > input, string name) {
	if (input.size() == 0) return; //nothing to plot here, move along

	string datafilename = name + ".dat";
	saveMatrix(datafilename, &input);

	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title '" << name << "'" << endl;
	plothandle << "set terminal pngcairo" << endl;
	plothandle << "set output '" << name << ".png'" << endl;
	plothandle << "plot '" << datafilename << "' matrix with image" << endl;
	plothandle.close();

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;
}

void Plotting::saveMatrix(string datafilename, vector<vector<double> > * input) {
	ofstream datahandle;
	datahandle.open(datafilename.c_str());
	ofstream newlinehandle;
	datafilename.append("N");
	newlinehandle.open(datafilename.c_str());
	unsigned rowcount = input->size();
	unsigned colcount = (*input)[0].size();
	unsigned padding = max(rowcount,colcount)+1;
	for (unsigned i = 0; i < rowcount; i++) {
		assert(colcount == (*input)[i].size());
		for (unsigned j = 0; j < colcount; j++) {
			datahandle << (*input)[i][j] << " ";
			newlinehandle << j << " " << i << " " << (*input)[i][j] << endl;
		}
		for (unsigned j = colcount; j < padding; j++) {
			newlinehandle << j << " " << i << " " << -1 << endl;
		}
		newlinehandle << endl;
		datahandle << endl;
	}
	for (unsigned i = rowcount; i < padding; i++) {
		for (unsigned j = 0; j < padding; j++) {
			newlinehandle << j << " " << i << " " << -1 << endl;
		}
		newlinehandle << endl;
	}
	datahandle.close();
	newlinehandle.close();

}

void Plotting::plotMatrix(vector<vector<double> > input, string name, double ymin, double ymax, double xmin, double xmax) {
	if (input.size() == 0) return; //nothing to plot here, move along

	string datafilename = name + ".dat";
	saveMatrix(datafilename, &input);

	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title '" << name << "'" << endl;
	plothandle << "set xrange [" << xmin << ":" << xmax << "]" << endl;
	plothandle << "set yrange [" << ymin << ":" << ymax << "]" << endl;
	plothandle << "set terminal pngcairo" << endl;
	plothandle << "set output '" << name << ".png'" << endl;
	plothandle << "plot '" << datafilename << "' matrix with image" << endl;
	plothandle.close();

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;
}

vector<double> Plotting::saveDatafile(vector<double> data, double leftBoundary, double rightBoundary, string name) {
	/**
	 * Saves datapoints to file without plotting them. Useful for later analysis.
	 */
	double stepsize = (rightBoundary - leftBoundary)/data.size();
	ofstream datahandle;
	datahandle.open(name.c_str());
	vector<double> result(2,data[0]);
	for (unsigned i = 0; i < data.size(); i++) {
		double index = leftBoundary + i*stepsize;
		double value = data[i];
		if (value < result[0]) result[0] = value;
		if (value > result[1]) result[1] = value;
		datahandle << index << " " << value << endl;
	}
	datahandle.close();
	return result;
}

void Plotting::plotBoxplot(vector<vector<double> > input, double leftBoundary, double rightBoundary, string name) {

	string matrixfilename = name + "-raw.dat";
	vector<vector<double> > t = Util::transpose(input);
	saveMatrix(matrixfilename, &t);

	double step = ((rightBoundary-leftBoundary)/input.size());

	/*
	 * set border 2 front linetype -1 linewidth 1.000
		set boxwidth 0.5 absolute
		set style fill   solid 0.25 border lt -1
		unset key
		set pointsize 0.5
		set style data boxplot
		set xtics border in scale 0,0 nomirror norotate  offset character 0, 0, 0 autojustify
		set xtics  norangelimit
		set xtics   ("A" 1.00000, "B" 2.00000)
		set ytics border in scale 1,0.5 nomirror norotate  offset character 0, 0, 0 autojustify
		set yrange [ 0.00000 : 100.000 ] noreverse nowriteback
		plot 'silver.dat' using (1):2, '' using (2):(5*$3)
	 */

	ofstream plothandle;
	plothandle.open("plottempfile.txt");
	plothandle << "set title '" << name << "'" << endl;
	plothandle << "set terminal postscript" << endl;//TODO: maybe replace this with png
	plothandle << "set output '" << name << ".ps'" << endl;
	plothandle << "set xrange[" << leftBoundary << ":" << rightBoundary+step << "]" << endl;
	plothandle << "set style data boxplot" << endl;
	plothandle << "unset key" << endl;
	plothandle << "plot '" << matrixfilename << "' using (" << step+leftBoundary << "):" << 1;
	for (unsigned i = 1; i < input.size(); i++) {
		double x = step*(i+1)+leftBoundary;
		plothandle << ", '' using (" << x << "):" << i+1;
	}

	plothandle.close();

	//call gnuplot
	int result = system("gnuplot plottempfile.txt");
	if (result > 0) cout << "Something went wrong with plotting " << name << "!" << endl;
	result = system("rm plottempfile.txt");
	if (result > 0) cout << "Could not remove plotfile " << name << "!" << endl;

	string datafilename = name + "-pgf.dat";

	ofstream pgfhandle;
	pgfhandle.open(datafilename.c_str());

	for (unsigned i = 0; i < input.size(); i++) {
		std::sort(input[i].begin(), input[i].end());
		double l = input[i].size();
		double middle = input[i].size() / 2;
		double median;
		if (input[i].size() % 2 == 0) {
			median = (input[i][middle-1] + input[i][middle])/2;
		} else {
			median = input[i][middle];
		}

		double lowerquartil = input[i][l/4];
		double upperquartil = input[i][3*l/4];

		unsigned upperwhiskerpos = l-1;
		while (input[i][upperwhiskerpos] - upperquartil > upperquartil - median && upperwhiskerpos > 0) upperwhiskerpos--;
		double lowerwhiskerpos = 0;
		while (lowerquartil - input[i][lowerwhiskerpos] > upperquartil - median && lowerwhiskerpos < l) lowerwhiskerpos++;
		double pos = (i)*step + leftBoundary;

		pgfhandle << pos << " " << median <<  " " << upperquartil << " " << lowerquartil << " " << input[i][upperwhiskerpos] << " " << input[i][lowerwhiskerpos] << endl;
	}

	pgfhandle.close();
}

vector<double> Plotting::cumulativeMean(vector<double> list) {
	/**
	 * Computes the cumulative mean of the values in list and return it in a vector of the same size.
	 * Does not plot anything. Maybe move this to another class 'Evaluation' or so later on.
	 */
	vector<double> result(list.size());
	double sum = 0;
	for (unsigned i = 0; i < list.size(); i++) {
		sum += list[i];
		result[i] = sum / (i+1); //i+1 because i starts from zero
	}
	return result;
}

vector<string> Plotting::getColors() {
	vector<string> colors;
	colors.push_back("blue");
	colors.push_back("green");
	colors.push_back("yellow");
	colors.push_back("orange");
	colors.push_back("skyblue");
	colors.push_back("red");
	colors.push_back("grey");
	colors.push_back("steelblue");
	colors.push_back("chartreuse");
	colors.push_back("dark-red");
	colors.push_back("olive");
	colors.push_back("gold");
	colors.push_back("navy");
	colors.push_back("sienna4");
	return colors;
}

string Plotting::getOverflowColor() {
	return "magenta";
}

void Plotting::plotHistogramm(vector<double> density, string name) {
	if (density.size() == 0) return;
	plotHistogramm(density, 0, density.size(), name);
}

void Plotting::exportToGraphviz(Model model, Graph R, string name, FeatureID feature, bool positive) {
	NodeID nodeCount = model.getNodeCount();
	assert(nodeCount > 0);
	assert(model.getNodeCount() == nodeCount);
	assert(model.getFeatureCount() > feature);
	double min = R.hasEdge(0,0);
	double max = R.hasEdge(0,0);
	double notReflexiveMin = R.hasEdge(0,nodeCount-1);//evaluates to 0,0 if only one node is present
	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {
			edgetype value = R.hasEdge(i,j);
			if (value < min) min = value;
			if (value > max) max = value;
			if (i != j && value < notReflexiveMin) notReflexiveMin = value;
		}
	}

	double scale = 10;

	vector<string> colors = Plotting::getColors();

	string overflowcolor = Plotting::getOverflowColor();

	if (colors.size() < model.weights[feature].size()) {
		cout << "Warning: Not enough colors! Painting surplus clusters " << overflowcolor << "." << endl;
	}

	string datafilename = name + ".dat";
	ofstream datahandle;
	datahandle.open(datafilename.c_str());
	datahandle << "graph " << name << "{ ";

	for (NodeID i = 0; i < nodeCount; i++) {
		if (model.featurePresent[i][feature]) {
			ClusterID clusterno = model.clusterAssignment[i][feature];
			string color;
			assert(clusterno < model.weights[feature].size());
			 /*
			* by the magic of unsigned underflow, this assert checks for clusterno being too large and it being too small.
			*/
			if (clusterno < colors.size()) color = colors[clusterno];
			else color = overflowcolor;
			datahandle << "{node [color=" << color << "] n" << i << "}" << endl;
		}
	}

	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {
			if (i == j) continue;
			double length;
			if (positive) {
				length = 0.5 + ((R.hasEdge(i,j) - notReflexiveMin) / (max - notReflexiveMin)) * scale;
			} else {
				 length = 0.5 + 0.5 + ((max - R.hasEdge(i,j)) / (max - notReflexiveMin)) * scale;
			}
			datahandle << "n" << i << " -- n" << j << " [len=" << length << ", style=invis];" << endl;
		}
	}
	datahandle << "}" << endl;
	datahandle.close();
}

}
}

