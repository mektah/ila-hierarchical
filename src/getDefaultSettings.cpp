/*
 * getDefaultSettings.cpp
 *
 *  Created on: 10.03.2015
 *      Author: moritzl
 */
#include <vector>
#include <string>
#include <map>
#include <iostream>

#include "Settings.h"
#include "mex.h"

using std::cout;
using std::endl;
using std::vector;
using std::string;
using namespace CILA::core;

void mexFunction(
		 int          nlhs,
		 mxArray      *plhs[],
		 int          nrhs,
		 const mxArray *prhs[]
		 )
{

	if (nrhs > 0) {
		mexErrMsgIdAndTxt( "MATLAB:getDefaultSettings:invalidNumInputs",
		                "No inputs required.");
	}

	if (nlhs != 1) {
		mexErrMsgIdAndTxt( "MATLAB:getDefaultSettings:invalidNumOutputs",
						"One output required.");
	}

	Settings result = Settings::getDefaultSettings();
	int nfields = 21;

	vector<string> fnames(nfields);
	vector<double> fvalues(nfields);

  	fnames[0] = "iterations";//100;
	fnames[1] = "mu_bias";//-1;
	fnames[2] = "sigma_bias";//4;
	fnames[3] = "mu_weights";//2;
	fnames[4] = "sigma_weights";//1;
	fnames[5] = "gamma_scale";//1;
	fnames[6] = "gamma_shape";//1;
	fnames[7] = "alpha_shape";//1;
	fnames[8] = "alpha_scale";//1;
	fnames[9] = "variance_shape";//1;
	fnames[10] = "variance_scale";//1;
	fnames[11] = "init_alpha";//1;
	fnames[12] = "init_gamma";//1;
	fnames[13] = "init_bias";//0;
	fnames[14] = "init_variance";//1;
	fnames[15] = "m_aux";//10;
	fnames[16] = "cthreshold";//0.5;
	fnames[17] = "fthreshold";//0.5;
	fnames[18] = "sthreshold";//0.1;
	fnames[19] = "burnin";//0;
	fnames[20] = "thin";//1;
	/**

	fnames[19] = "retryOld";//false;
	fnames[20] = "classicNewFeatures";//true;
	fnames[21] = "sequential";//false;
	fnames[22] = "empty";//true;
	fnames[23] = "randommodel";//false;
	fnames[24] = "stepsPerNode";//3;
	fnames[25] = "nodeCount";//50;
	fnames[26] = "randomgraph";//false;
	fnames[27] = "euclid";//false;
	fnames[28] = "realnoise";//0;
	fnames[29] = "silent";//false;
	fnames[30] = "permute";//false;
	fnames[31] = "aggregate";//false;
	fnames[32] = "plot";//false;
	fnames[33] = "maxPlotPoints";//1000;
	fnames[34] = "limit";//70;
	fnames[35] = "leftOrder";//true;
	fnames[36] = "distReorder";//false;
	fnames[37] = "adaptiveThresholds";//false;
	fnames[41] = "num_threads";//2;
	fnames[42] = "maxTLC";//1250;
	fnames[43] = "TLClimitActive";//true;
	fnames[44] = "majorversion";//0;
	fnames[45] = "minorversion";//8;
	fnames[46] = "subminorversion";//0;
	fnames[47] = "inputpath";//"inputdata.csv";
	fnames[48] = "gtpath";//"ground_truth_comparison.csv";
	fnames[49] = "naiveWeights";//false;
	fnames[50] = "conjugateWeights";//true;
	fnames[51] = "naivebias";//false;
	fnames[52] = "conjugatebias";//true;
	fnames[53] = "naiveclusters";//false;
	fnames[54] = "compressZ";//true;
	fnames[55] = "burnin";//0;
	fnames[56] = "thin";//1;
	*/


	fvalues[0] = result.iterations;//100;
	fvalues[1] = result.mu_bias;//-1;
	fvalues[2] = result.sigma_bias;//4;
	fvalues[3] = result.mu_weights;//2;
	fvalues[4] = result.sigma_weights;//1;
	fvalues[5] = result.gamma_scale;//1;
	fvalues[6] = result.gamma_shape;//1;
	fvalues[7] = result.alpha_shape;//1;
	fvalues[8] = result.alpha_scale;//1;
	fvalues[9] = result.variance_shape;//1;
	fvalues[10] = result.variance_scale;//1;
	fvalues[11] = result.init_alpha;//1;
	fvalues[12] = result.init_gamma;//1;
	fvalues[13] = result.init_bias;//0;
	fvalues[14] = result.init_variance;//1;
	fvalues[15] = result.m_aux;//10;
	fvalues[16] = result.cthreshold;//0.5;
	fvalues[17] = result.fthreshold;//0.5;
	fvalues[18] = result.sthreshold;//0.1;
	fvalues[19] = result.burnin;//0;
	fvalues[20] = result.thin;//1;
	/**
	fvalues[19] = result.retryOld;//false;
	fvalues[20] = result.classicNewFeatures;//true;
	fvalues[21] = result.sequential;//false;
	fvalues[22] = result.empty;//true;
	fvalues[23] = result.randommodel;//false;
	fvalues[24] = result.stepsPerNode;//3;
	fvalues[25] = result.nodeCount;//50;
	fvalues[26] = result.randomgraph;//false;
	fvalues[27] = result.euclid;//false;
	fvalues[28] = result.realnoise;//0;
	fvalues[29] = result.silent;//false;
	fvalues[30] = result.permute;//false;
	fvalues[31] = result.aggregate;//false;
	fvalues[32] = result.plot;//false;
	fvalues[33] = result.maxPlotPoints;//1000;
	fvalues[34] = result.limit;//70;
	fvalues[35] = result.leftOrder;//true;
	fvalues[36] = result.distReorder;//false;
	fvalues[37] = result.adaptiveThresholds;//false;
	fvalues[38] = result.cthreshold;//0.5;
	fvalues[39] = result.fthreshold;//0.5;
	fvalues[40] = result.sthreshold;//0.1;
	fvalues[41] = result.num_threads;//2;
	fvalues[42] = result.maxTLC;//1250;
	fvalues[43] = result.TLClimitActive;//true;
	fvalues[44] = result.majorversion;//0;
	fvalues[45] = result.minorversion;//8;
	fvalues[46] = result.subminorversion;//0;
	fvalues[47] = result.inputpath;//"inputdata.csv";
	fvalues[48] = result.gtpath;//"ground_truth_comparison.csv";
	fvalues[49] = result.naiveWeights;//false;
	fvalues[50] = result.conjugateWeights;//true;
	fvalues[51] = result.naivebias;//false;
	fvalues[52] = result.conjugatebias;//true;
	fvalues[53] = result.naiveclusters;//false;
	fvalues[54] = result.compressZ;//true;
	fvalues[55] = result.burnin;//0;
	fvalues[56] = result.thin;//1;
	*/

	//TODO: convert string vector into mxArray
	const char *fnamesInChars[nfields];
	//fnamesInChars = mxCalloc(nfields, sizeof(*fnamesInChars));

	for (int i = 0; i < nfields; i++) {
		fnamesInChars[i] = fnames[i].c_str();
	}

	plhs[0] = mxCreateStructMatrix(1, 1, nfields, fnamesInChars);

	vector<mxArray*> doubleContainers(nfields);//now assuming only scalar number as settings. Small loss.
	for (int i = 0; i < nfields; i++) {
        mxArray *field_value;
        field_value = mxCreateDoubleMatrix(1,1,mxREAL);
		*mxGetPr(field_value) = fvalues[i];
		mxSetFieldByNumber(plhs[0],0,i,field_value);
	}
}
