/*
 * Input.h
 *
 *  Created on: 28.03.2013
 *      Author: moritz
 */

#ifndef INPUT_H_
#define INPUT_H_

#include "Graph.h"
#include "types.h"
#include "Settings.h"
#include <string>
#include "Model.h"

namespace CILA {
using namespace core;
namespace io {

class Input {
public:
	Input() = default;
	virtual ~Input() = default;
	static vector<vector<string> >loadRawArray(string path);
	static vector<vector<double> > parseDoubles(vector<vector<string> > stringVektor);
	static Graph computeR(vector<vector<double> > featureVektor, bool euclid = false, bool normalize = false);//TODO: change to enum later
	static double dotProduct(vector<double> first, vector<double> second) __attribute__((pure));
	static double euclidDistance(vector<double> first, vector<double> second) __attribute__((pure));
	void loadGroundTruth(string path);
	FeatureID groundTruthFeatureCount() __attribute__((pure));
	ClusterID groundTruthClusterCount(FeatureID m) __attribute__((pure));
	int groundTruthCluster(NodeID i, FeatureID m) __attribute__((pure));
	ClusterMatrix getgTClusters();
	Model getgTModel();
	double lowerBound(FeatureID m, ClusterID c) __attribute__((pure));
	double upperBound(FeatureID m, ClusterID c) __attribute__((pure));
	void permute(vector<NodeID> perm);

private:
	ClusterMatrix gtClusters;
	vector<vector<double> > lowerBounds;
	vector<vector<double> > upperBounds;
};

} /* namespace io */
}/* namespace CILA */
#endif /* INPUT_H_ */
