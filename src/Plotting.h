/*
 * Plotting.h
 *
 *  Created on: 18.03.2013
 *      Author: moritz
 */

#ifndef PLOTTING_H_
#define PLOTTING_H_

#include <vector>
#include <string>

#include "PDFunctor.h"
#include "Graph.h"
#include "Model.h"

namespace CILA {
using namespace core;
namespace io {

class Plotting {
public:
	Plotting() = default;
	static void plotPDF(ProbabilityDensityFunctor * pdf,
		double leftBoundary, double rightBoundary, double stepsize);
	static vector<double> evalPDF(ProbabilityDensityFunctor * pdf,
		double leftBoundary, double rightBoundary, double stepsize);
	static void plotLogPDF(ProbabilityDensityFunctor * pdf,
			double leftBoundary, double rightBoundary, double stepsize);
	virtual ~Plotting();
	static vector<double> cumulativeMean(vector<double> list) __attribute__((const));
	static vector<string> getColors();
	static string getOverflowColor();
	static void plotHistogramm(vector<double> density, double leftBoundary,
			double rightBoundary, string name = "testplot");
	static void plotHistogramm(vector<double> priorDensity, vector<double> posteriorDensity, double priorMean, double postMean, double leftBoundary, double rightBoundary, string name);
	static void plotMultiple(vector<vector<double> > * chains, double leftBoundary, double rightBoundary, string name, double gt = 0, bool plotgt = false);
	static void plotHistogramm(vector<double> density, string name = "testplot");
	static void plotMatrix(vector<vector<double> > input, string name);
	static void plotMatrix(vector<vector<double> > input, string name, double ymin, double ymax, double xmin, double xmax);
	static void plotBoxplot(vector<vector<double> > input, double leftBoundary, double rightBoundary, string name);
	static void exportToGraphviz(Model model, Graph R, string name, FeatureID feature, bool positive);
private:
	static void saveMatrix(string datafilename, vector<vector<double> > * input);
	static vector<double> saveDatafile(vector<double> data, double leftBoundary, double rightBoundary, string name);

};
}
}

#endif /* PLOTTING_H_ */
