/*
 * Input.cpp
 *
 *  Created on: 28.03.2013
 *      Author: moritz
 */

#include <assert.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <cmath>
#include <sstream>
#include <set>
#include <limits>

#include "Input.h"
#include "types.h"

using std::ifstream;
using std::cout;
using std::cerr;
using std::endl;
using std::istringstream;
using std::stringstream;
using std::numeric_limits;
using std::set;

namespace CILA {
namespace io {

void Input::loadGroundTruth(string path) {
	ClusterID maxClusters = 5;
	vector<vector<string> > raw = Input::loadRawArray(path);

	FeatureID featureCount = raw[0].size();
	NodeID nodeCount = raw.size();

	//need to select columns we are interested in
	vector<vector<string> > labelcopy(nodeCount);

	bool colmask[] = {0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};

	for (NodeID i = 0; i < nodeCount; i++) {
		assert(labelcopy[i].empty());
		for (FeatureID m = 0; m < featureCount; m++) {
			if (colmask[m]) {
				labelcopy[i].push_back(raw[i][m]);
			}
		}
		assert(labelcopy[i].size() == labelcopy[0].size());
	}
	featureCount = labelcopy[0].size();

	gtClusters.resize(nodeCount);
	vector<vector<string> > texts(featureCount);
	vector<vector<double> > labels(nodeCount);

	for (NodeID i = 0; i < nodeCount; i++) {
			gtClusters[i].resize(featureCount,-1);
	}
	lowerBounds.resize(featureCount);
	upperBounds.resize(featureCount);

	for (NodeID i = 0; i < nodeCount; i++) {
		labels[i].resize(featureCount);
		for (FeatureID m = 0; m < featureCount; m++) {
			string token = labelcopy[i][m];
			token.erase(token.find_last_not_of(" \t\r")+1);

			if (token.compare("NaN") == 0 || token.compare("nan") == 0 || token.length() == 0) {
				//node not participating in feature
				labels[i][m] = 0.0/0.0;
			} else if (token.find_first_of("0123456789-") == 0 && token.find_first_not_of("0123456789.-") == string::npos){//TODO: error checking
				//a number! we have a number!
				stringstream ss(token);
				double d;
				if (!(ss >> d)) {
					throw std::runtime_error("Couldn't convert "+token+" to double.");
				}
				labels[i][m] = d;
			} else {
				//text which is not nan. What could it be?
				ClusterID index = 0;
				while (index < texts[m].size() && texts[m][index].compare(token) != 0) index++;
				if (index == texts[m].size()) {
					texts[m].push_back(token);
				}
				labels[i][m] = index;
			}
		}
	}

	for (FeatureID m = 0; m < featureCount; m++) {
		//now count values. runtime: n log n
		ClusterID distinct = 0;
		set<double> values;
		double min = numeric_limits<double>::max();//we can't use labels[0][m] here because the value might be nan.
		double max = numeric_limits<double>::min();
		for (NodeID i = 0; i < nodeCount; i++) {
			double value = labels[i][m];
			if (std::isnan(value) || !(value == value)) continue;
			if (value < min) min = value;
			if (value > max) max = value;
			if (values.find(value) == values.end()) {
				distinct++;
				values.insert(value);
				assert(distinct == values.size());
			}
		}
		if (min == numeric_limits<double>::max()) {
			//no real values were found
			assert(max == numeric_limits<double>::min());
			max = 0;
			min = 0;
		}
		if (distinct <= maxClusters) {
			//iterate through values
			lowerBounds[m].clear();
			upperBounds[m].clear();
			for (set<double>::iterator it=values.begin(); it!=values.end(); ++it) {
				lowerBounds[m].push_back(*it);
				upperBounds[m].push_back(*it);
			}
		} else {
			//lowerBounds[m].push_back(-numeric_limits<double>::infinity());
			//upperBounds[m].push_back(min);
			for (ClusterID i = 0; i < maxClusters; i++) {
				lowerBounds[m].push_back(min + i*(max-min)/maxClusters);
				upperBounds[m].push_back(min + (i+1)*(max-min)/maxClusters);
			}
			//lowerBounds[m].push_back(max);
			//upperBounds[m].push_back(numeric_limits<double>::infinity());
		}
		//now sort nodes into clusters!
		for (NodeID i = 0; i < nodeCount; i++) {
			double value = labels[i][m];
			if (std::isnan(value) || !(value == value)) {
				gtClusters[i][m] = -1;
				continue;
			}
			ClusterID c = 0;
			while(value > upperBounds[m][c]) c++;
			assert(c < maxClusters);
			gtClusters[i][m] = c;
		}
	}
}

vector<vector<string> > Input::loadRawArray(string path) {
	string line;
	vector<vector<string> > result;
	unsigned maxcols = 0;

	cout << "Loading file " << path << " ... ";
	cout.flush();

	try {
	ifstream myfile (path.c_str());
	  if (myfile && myfile.is_open())
	  {
	    while ( myfile.good() )
	    {
	      getline (myfile,line);
	      stringstream lineStream(line);
	      string cell;

	      vector<string> values;

	      while(getline(lineStream, cell, ',')) {
	    	  values.push_back(cell);
	      }
	      if (values.size() > maxcols) maxcols = values.size();

	     result.push_back(values);
	    }
	    myfile.close();
	  }

	  else {
		  throw std::runtime_error("Unable to open file"+path);
	  }
	  result.pop_back();
	}
	catch  (...) {
		throw std::runtime_error("Error opening file "+path);
	}

	cout << " done" << endl;

	//adding padding
	for (unsigned i = 0; i < result.size(); i++) {
		if (result[i].size() < maxcols) {
			if (result[i].size() == maxcols -1) {
				//add padding
				result[i].push_back("");
			} else {
				cout << "Warning: Row " << i << " has only " << result[i].size() << " columns." << endl;
			}
		}
	}

	return result;
}

double Input::dotProduct(vector<double> first, vector<double> second) {
	assert(first.size() == second.size());
	double result = 0;
	for (NodeID i = 0; i < first.size(); i++) {
		result += first[i]*second[i];
	}
	return result;
}

double Input::euclidDistance(vector<double> first, vector<double> second) {
	assert(first.size() == second.size());
	double result = 0;
	for (NodeID i = 0; i < first.size(); i++) {
		result += pow(first[i]-second[i],2);
	}
	return pow(result,0.5);
}

vector<vector<double> > Input::parseDoubles(vector<vector<string> > stringVektor) {
	vector<vector<double> > doublified(stringVektor.size());
	for (NodeID i = 0; i < doublified.size(); i++) {
		doublified[i].resize(stringVektor[i].size());
		for (FeatureID m = 0; m < doublified[i].size(); m++) {
			istringstream ss(stringVektor[i][m]);
			ss >> doublified[i][m];
		}
	}
	return doublified;
}

Graph Input::computeR(vector<vector<double> > featureVektor, bool euclid, bool normalizeInput) {
	//prepare data structures
	NodeID n = featureVektor.size();
	FeatureID colCount = featureVektor[0].size();
	vector<vector<edgetype> > edges(n);
	for (NodeID i = 0; i < n; i++) {
		FeatureID curcol = featureVektor[i].size();
		assert(curcol == colCount);
		edges[i].resize(n);
	}

	if (normalizeInput) {
		for (FeatureID j = 0; j < colCount; j++) {
			double sum = 0;
			for (NodeID i = 0; i < n; i++) {
				sum += featureVektor[i][j];
			}
			double average = sum / n;
			for (NodeID i = 0; i < n; i++) {
				featureVektor[i][j] /= average;
			}
		}
	}

	double weightSum = 0;
	//calculate edge weights
	for (NodeID i = 0; i < n; i++) {
		for (NodeID j = 0; j <= i; j++) {
			double value = euclid ? -euclidDistance(featureVektor[i], featureVektor[j]) : dotProduct(featureVektor[i], featureVektor[j]);
			edges[i][j] = value;
			weightSum += value;
			if (i != j) {
				edges[j][i] = edges[i][j];
				weightSum += value;
			}
		}
	}

	if (normalizeInput) {
		double averageWeight = weightSum / (n*n);
		if (euclid) {
			averageWeight *= -1;
		}
		for (NodeID i = 0; i < n; i++) {
			for (NodeID j = 0; j <= i; j++) {
				edges[i][j] /= averageWeight;
				edges[j][i] = edges[i][j];
			}
		}
	}

	return Graph(edges);
}

FeatureID Input::groundTruthFeatureCount() {
	assert(gtClusters.size() > 0);
	return gtClusters[0].size();
}

ClusterID Input::groundTruthClusterCount(FeatureID m) {
	int max = -1;
	for (NodeID i = 0; i < gtClusters.size(); i++) {
		assert(m < gtClusters[i].size());
		if (gtClusters[i][m] > max) max = gtClusters[i][m];
	}
	return max+1;
}

int Input::groundTruthCluster(NodeID i, FeatureID m) {
	assert(i < gtClusters.size());
	assert(m < gtClusters[i].size());
	double result = gtClusters[i][m];
	assert(result >= -1);
	return gtClusters[i][m];
}

ClusterMatrix Input::getgTClusters() {//maybe remove this method. Can't label the other ones pure while it exists
	return gtClusters;
}

Model Input::getgTModel() {
	FeatureMatrix Z(gtClusters.size());
	if (Z.size() == 0) {
		Model empty;
		empty.emptyModel(0, 1, 1, 0);
		return empty;
	}
	FeatureID featureCount = gtClusters[0].size();
	for (NodeID i = 0; i < Z.size(); i++) {
		Z[i].resize(featureCount);
		for (FeatureID m = 0; m < Z[i].size(); m++) {
			Z[i][m] = (gtClusters[i][m] >= 0) ? 1 : 0;
		}
	}
	WeightMatrices weights(featureCount);
	for (FeatureID m = 0; m < featureCount; m++) {
		weights[m].resize(groundTruthClusterCount(m));
		for (ClusterID l = 0; l < weights[m].size(); l++) {
			weights[m][l].resize(weights[m].size());
		}
	}
	Model model(Z, gtClusters, weights, 1,1,0,1);
	return model;
}

double Input::lowerBound(FeatureID m, ClusterID c) {
	assert(m < lowerBounds.size());
	assert(c < lowerBounds[m].size());
	return lowerBounds[m][c];
}

double Input::upperBound(FeatureID m, ClusterID c) {
	assert(m < upperBounds.size());
	assert(c < upperBounds[m].size());
	return upperBounds[m][c];
}

void Input::permute(vector<NodeID> perm) {
	assert(perm.size() == gtClusters.size());
	NodeID nodeCount = gtClusters.size();
	ClusterMatrix clustercopy(nodeCount);
	for (NodeID i = 0; i < perm.size(); i++) {
		clustercopy[i] = gtClusters[perm[i]];
	}
	gtClusters = clustercopy;
}

}/* namespace io */
}/* namespace CILA */

