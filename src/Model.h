/*
 * Features.h
 *
 *  Created on: 30.11.2012
 *      Author: moritz
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <vector>
#include <cereal/access.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/base_class.hpp>

using std::vector;

#include "types.h"
#include "Graph.h"
#include "Settings.h"

namespace CILA {
namespace core {

class Model {
public:
	Model(FeatureMatrix f = {}, ClusterMatrix c = {}, WeightMatrices w={}, double alpha=1, double gamma=1, double s=0, double sigma=1);

	static Model generateModel(size_t n, double mu_w, double sigma_w, double mu_s, double sigma_s);
	static Model generateModel(Settings settings = Settings::getDefaultSettings());
	static Model generateModel(size_t n, double alpha, double gamma, double mu_w, double sigma_w, double mu_s, double sigma_s);
	static Model emptyModel(NodeID n, double alpha, double gamma, double s);
	static Model emptyModel(Settings settings = Settings::getDefaultSettings());
	static Model exampleModel();
	static Model singleFullFeatureModel(NodeID n, double alpha, double gamma, double s, double sigma);
	static Model singleFullFeatureModel(Settings settings);

	bool isConstructed() __attribute__((pure));

	static void mergeFeatures(FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, FeatureID l, FeatureID k);
	static void dropFeature(ClusterMatrix * c, WeightMatrices * w, FeatureID l);
	static bool isDependent(ClusterMatrix * c, FeatureID l, FeatureID k);
	static int countClusters(ClusterMatrix * c, FeatureID m);
	static ClusterID clusterCombinations(ClusterMatrix * c, FeatureID l, FeatureID k);
	static Model mapToRepresentative(Model input);
	static vector<vector<double> > computeTrestricted(FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, FeatureID l);
	static double computeDistance(Model * a, FeatureID k, Model * b, FeatureID l);
	static double computeDistance(Model * a, Model * b);
	static double computeLength(Model * a, FeatureID l);

	void leftOrder();

	void purgeUnusedFeatures();

	double logPrior();

	double likelihood(Graph * g) __attribute__((pure));
	double logLikelihood(Graph * g) __attribute__((pure));
	static double likelihood(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma = 0.5) __attribute__((pure));
	static double logLikelihood(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma) __attribute__((pure));
	static double logLikelihood(Graph * g, FeatureMatrix * f, vector<bitPool> * compressedFeatures, ClusterMatrix * c, WeightMatrices * w, double s, double sigma) __attribute__((pure));
	static double loglikelihood(Graph * g, vector<vector<double> > * T, double bias, double variance) __attribute__((pure));
	static double logLikelihoodNewCluster(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma, NodeID i, FeatureID m, ClusterID newCluster, double oldll) __attribute__((pure));
	static double logLikelihoodNewCluster(Graph * g, vector<bitPool> * compressedFeatures, ClusterMatrix * c, WeightMatrices * w, double s, double sigma, NodeID i, FeatureID m, ClusterID newCluster, double oldll) __attribute__((pure));
	static double logLikelihoodNewWeights(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma, FeatureID m, ClusterID c1, ClusterID c2, vector<NodeID> * c1pop, vector<NodeID> * c2pop, double wnew, double oldll) __attribute__((pure));
	static vector<vector<double> > computeT(FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w);
	static vector<vector<double> > computeT(FeatureMatrix * f, vector<bitPool> * compressedZ, ClusterMatrix * c, WeightMatrices * w);
	Graph sampleGraphInstance(double variance);

	FeatureMatrix getFeatureMatrix();
	ClusterMatrix getClusterMatrix();
	WeightMatrices getWeights();
	NodeID getNodeCount();
	FeatureID getFeatureCount();
	int getTotalLinkCount();

	static bool comp(ClusterMatrix &cl, FeatureID k, FeatureID l);

	vector<vector<vector<double> > > computePurityandNMIMatrices(ClusterMatrix externalLabels);
	vector<vector<double> > pValuePurityMatrix(ClusterMatrix externalLabels, int iter = 1000);
	vector<vector<double> > pValueNMIMatrix(ClusterMatrix externalLabels, int iter = 1000);
	vector<vector<double> > computeAnovaMatrix(vector<vector<double> > external);
	vector<vector<double> > pValueAnovaMatrix(vector<vector<double> >, int iter = 1000);


	static int clustersShared(NodeID a, NodeID b, ClusterMatrix * c) __attribute__((pure));
	static int clustersNotShared(NodeID a, NodeID b, ClusterMatrix * c) __attribute__((pure));

	static vector<vector<int> > clusterDifferenceTriangle(ClusterMatrix * c1, ClusterMatrix * c2);
	static int clusterDifferenceRestricted(ClusterMatrix * c1, FeatureID k, ClusterMatrix * c2, FeatureID l);

	double getLogLikelihoodContribution(Graph * g, FeatureID m);
	static double getLogLikelihoodContribution(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma, FeatureID m);
	static double getLogLikelihoodContributionExceptBias(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma, FeatureID m);
	friend class Sampler;

	void permute(vector<NodeID> permutation);
	void scale(double factor);

	void printWeightMatrices();
	void printLikelihoodContribution(Graph graph);
	static void printClusters(ClusterMatrix clusters);

	double s, sigma, alpha, gamma;
	FeatureMatrix featurePresent; //Z in PalKnoGha
	ClusterMatrix clusterAssignment;//C in PalKnoGha
	WeightMatrices weights;//W in PalKnoGha

private:
	friend class cereal::access;
	static double sigmoid(double weight) __attribute__((const));
	double getLinkMean(NodeID a, NodeID b) __attribute__((pure));
	static double getLinkMean(NodeID a, NodeID b, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s) __attribute__((pure));
	static double getLinkMean(NodeID a, NodeID b, vector<bitPool> * compressedFeatures, ClusterMatrix * c, WeightMatrices * w, double s) __attribute__((pure));
	bool constructed;
	NodeID n;

	template<class Archive>
	void serialize(Archive & ar)
	{
		ar & s;
	    ar & sigma;
	    ar & alpha;
	    ar & gamma;
	    ar & constructed;
	    ar & n;
	    ar & featurePresent;
	    ar & clusterAssignment;
	    ar & weights;
	}
};

}
}

#endif /* MODEL_H_ */
