/*
 * Settings.h
 *
 *  Created on: 30.06.2013
 *      Author: moritz
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <cereal/access.hpp>

using std::string;
using std::endl;

namespace CILA {
namespace core {

class Settings {
public:
	Settings() = default;
	virtual ~Settings() = default;
	bool isValid();
	static Settings getDefaultSettings();
	void dumpSettings();
	string getVersionString();

	int iterations;
	int chains;
	int repeat;
	int pvaluePerm;
	double mu_bias;
	double sigma_bias;
	double init_bias;
	double mu_weights;
	double sigma_weights;
	double gamma_shape;
	double gamma_scale;
	double alpha_shape;
	double alpha_scale;
	double variance_shape;
	double variance_scale;
	double init_gamma;
	double init_alpha;
	double init_variance;
	bool crp_only;
	bool sample_alpha;
	bool sample_gamma;
	bool sample_Z;
	bool sample_C;
	bool sample_weights;
	bool sample_bias;
	bool sample_variance;
	int m_aux;
	bool retryOld;
	bool classicNewFeatures;
	bool saveStates;
	bool sequential;
	bool empty;
	bool randommodel;

	bool randomgraph;
	bool euclid;

	double realnoise;

	int stepsPerNode;

	bool silent;
	bool plot;
	int maxPlotPoints;
	bool permute;
	bool aggregate;
	unsigned limit;
	unsigned nodeCount;

	bool leftOrder;
	bool distReorder;
	bool adaptiveThresholds;
	double cthreshold;
	double fthreshold;
	double sthreshold;

	int num_threads;

	string inputpath;
	string gtpath;
	bool normalize_input;

	int maxTLC;
	bool TLClimitActive;
	int majorversion;
	int minorversion;
	int subminorversion;

	bool naiveWeights;
	bool conjugateWeights;
	bool naivebias;
	bool conjugatebias;
	bool naiveclusters;
	bool compressZ;

	int burnin;
	int thin;

private:
	friend class cereal::access;
	// When the class Archive corresponds to an output archive, the
	// & operator is defined similar to <<.  Likewise, when the class Archive
	// is a type of input archive the & operator is defined similar to >>.
	template<class Archive>
	void serialize(Archive & ar)
	{
	    ar & iterations;
	    ar & chains;
	    ar & repeat;
	    ar & pvaluePerm;
	    ar & mu_bias;
	    ar & sigma_bias;
	    ar & init_bias;
	    ar & mu_weights;
	    ar & sigma_weights;
	    ar & gamma_shape;
	    ar & gamma_scale;
	    ar & alpha_shape;
	    ar & alpha_scale;
	    ar & variance_shape;
	    ar & variance_scale;
	    ar & init_gamma;
	    ar & init_alpha;
	    ar & init_bias;
		ar & init_variance;
		ar & crp_only;
		ar & sample_alpha;
		ar & sample_gamma;
		ar & sample_Z;
		ar & sample_C;
		ar & sample_weights;
		ar & sample_bias;
		ar & sample_variance;
		ar & saveStates;
	    ar & m_aux;
	    ar & retryOld;
	    ar & classicNewFeatures;
	    ar & sequential;
	    ar & empty;
	    ar & randommodel;
	    ar & randomgraph;
	    ar & euclid;
	    ar & realnoise;
	    ar & stepsPerNode;
	    ar & silent;
	    ar & plot;
	    ar & permute;
	    ar & normalize_input;
	    ar & limit;
	    ar & nodeCount;
	    ar & leftOrder;
	    ar & distReorder;
	    ar & cthreshold;
	    ar & fthreshold;
	    ar & sthreshold;
	    ar & num_threads;
	    ar & majorversion;
	    ar & minorversion;
	    ar & subminorversion;
	    ar & maxPlotPoints;
	    ar & burnin;
	    ar & thin;
	}
};

} /* namespace core */
} /* namespace CILA */
#endif /* SETTINGS_H_ */
