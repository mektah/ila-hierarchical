/*
 * ModelIO.h
 *
 *  Created on: 27.04.2015
 *      Author: moritzl
 */

#ifndef MODELIO_H_
#define MODELIO_H_

#include <string>

#include "Aggregation.h"
#include "Model.h"
#include "sampler.h"
#include "Settings.h"
#include "types.h"

namespace CILA {
namespace io {

using CILA::core::Settings;
using CILA::core::Model;
using CILA::core::Sampler;
using CILA::Aggregation;


class ModelIO {
public:
	ModelIO() = default;
	virtual ~ModelIO() = default;
	static string writeSampler(Settings &settings, string name, Sampler& sampler, bool preciseName = false);
	static Sampler readSampler(string path);
	static string writeAggregation(Settings &settings, string name, Aggregation& agg, bool preciseName = false);
	static Aggregation readAggregation(string path);
	static string writeModel(Settings &settings, string name, Model& model, bool preciseName = false);
	static Model readModel(string path);
	static string writeSettings(Settings &settings, string name, bool preciseName = false);
	static Settings readSettings(string path);
	static string exportToCSV(Settings &settings, string name, Model& model, bool preciseName = false);
};

}
} /* namespace CILA */
#endif /* MODELIO_H_ */
