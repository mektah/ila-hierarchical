/*
 * Analysis.h
 *
 *  Created on: 22.07.2013
 *      Author: moritz
 */

#ifndef ANALYSIS_H_
#define ANALYSIS_H_

#include <boost/program_options.hpp>
#include <iostream>
#include <fstream>

#include "types.h"
#include "Plotting.h"
#include "sampler.h"

namespace CILA {
using namespace core;

namespace io {


class Analysis {
public:
	Analysis() = default;
	virtual ~Analysis() = default;
	static void analyze(vector<Sampler> * runlist, Settings settings);
	static void analyze(vector<Sampler> * runlist, Model orig, Settings settings);
	static void investigateCumulativeMean(vector<vector<Model> > &chains, Model &orig, Settings settings);
	static void investigateCumulativeMean(vector<vector<Model> > &chains, ClusterMatrix labels, Settings settings);
	static void printPurity(ClusterMatrix& external,	Model& result);
	static void printDistances(Model& orig, Model& result);
	static void printAnova(vector<vector<double> > external,	Model& result);
	static void printNMI(ClusterMatrix& external,	Model& result);

private:
	static int calculateInterval(vector<Sampler>*& runlist, unsigned & maxsize, unsigned & realmax);
	static vector<double> extractValues(vector<Sampler>*runlist, unsigned c, unsigned i);
};

} /* namespace io */
} /* namespace CILA */
#endif /* ANALYSIS_H_ */
