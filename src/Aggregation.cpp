/*
 * Aggregation.cpp
 *
 *  Created on: 26.05.2013
 *      Author: moritz
 */

#include <assert.h>
#include <iostream>

#include "Aggregation.h"
#include "generative.h"
#include "PDFunctor.h"
#include "sampler.h"

#include "Util.h"

using std::cout;
using std::endl;

namespace CILA {
using namespace core;

Aggregation::Aggregation(Model base) {
	nodeCount = base.getNodeCount();
	featureCount = base.getFeatureCount();

	//TODO: currently, mapping to representatives doubles the weights.
	//base = Features::mapToRepresentative(base);

	alpha = base.alpha;
	gamma = base.gamma;
	sigma = base.sigma;
	s = base.s;

	extZ.resize(featureCount);
	extC.resize(featureCount);
	extW.resize(featureCount);

	FeatureMatrix Z = base.getFeatureMatrix();
	ClusterMatrix C = base.getClusterMatrix();
	WeightMatrices W = base.getWeights();

	for (FeatureID m = 0; m < featureCount; m++) {
		extZ[m].resize(nodeCount);
		extC[m].resize(nodeCount);
		extW[m].resize(nodeCount);
		for (NodeID i = 0; i < nodeCount; i++) {
			extZ[m][i] = Z[i][m];
			extC[m][i].resize(nodeCount,0);
			extW[m][i].resize(nodeCount);
			for (NodeID j = 0; j < nodeCount; j++) {
				if (Z[i][m] && Z[j][m]) {
					if (C[i][m] == C[j][m]) extC[m][i][j] = 1;
					extW[m][i][j] = W[m][C[i][m]][C[j][m]];
				}
				else {
					extW[m][i][j] = 0;
				}
			}
		}
	}
}

Aggregation Aggregation::aggregate(vector<Aggregation> chainlinks, vector<double> weights) {
/**
 * Three possible strategies:
 * 1. identify features with distance, sort features to minimize distance, average over fitting
 * 2. Broader equivalence classes
 * 3. stubbornly average over all first features, all second features, etc.
 *
 * First, we try the last approach.
 */
	vector<extFeatureMatrix> Zseries;
	vector<extClusterMatrix> Cseries;
	vector<extWeights> Wseries;

	unsigned chaincount = chainlinks.size();
	assert(chaincount == weights.size());
	assert(chaincount >= 1);
	Zseries.resize(chaincount);
	Cseries.resize(chaincount);
	Wseries.resize(chaincount);
	vector<double> unit(chaincount,1);
	NodeID nodeCount = chainlinks[0].nodeCount;
	FeatureID maxFeatureCount = 0;

	double weightsumint = 0;
	for (unsigned i = 0; i < chaincount; i++) {
		weightsumint += weights[i];
	}

	Aggregation result;
	result.alpha = 0;
	result.gamma = 0;
	result.s = 0;
	result.sigma = 0;
	result.nodeCount = nodeCount;

	for (unsigned i = 0; i < chaincount; i++) {
		result.alpha += chainlinks[i].alpha*(weights[i] / weightsumint);
		result.gamma += chainlinks[i].gamma*(weights[i] / weightsumint);
		result.sigma += chainlinks[i].sigma*(weights[i] / weightsumint);
		result.s += chainlinks[i].s*(weights[i] / weightsumint);
		assert(chainlinks[i].nodeCount == nodeCount);
		Zseries[i] = chainlinks[i].extZ;
		Cseries[i] = chainlinks[i].extC;
		Wseries[i] = chainlinks[i].extW;
		FeatureID currFeatureCount = chainlinks[i].extZ.size();
		if (currFeatureCount > maxFeatureCount) maxFeatureCount = currFeatureCount;

	}
	result.featureCount = maxFeatureCount;

	/**
	 * 1. dimension: feature
	 * 2. dimension: chainlink
	 * 3. dimension: node
	 * 4. dimension: node
	 */

	result.extZ = Util::averageMatrices(Zseries, weights);
	result.extC.resize(maxFeatureCount);
	result.extW.resize(maxFeatureCount);

	for (FeatureID f = 0; f < maxFeatureCount; f++) {

		//result.extW[f] = Util::average

		vector<vector<double> > weightsum;
		weightsum.resize(nodeCount);

		vector<vector<double> > Csum(nodeCount);
		double participating = 0;
		for (NodeID i = 0; i < nodeCount; i++) {
			Csum[i].resize(nodeCount, 0);
			weightsum[i].resize(nodeCount, 0);
		}

		for (unsigned c = 0; c < chaincount; c++) {
			if (f >= chainlinks[c].featureCount) continue;
			participating += weights[c];
			for (NodeID i = 0; i < nodeCount; i++) {
				for (NodeID j = 0; j < nodeCount; j++) {
					Csum[i][j] += chainlinks[c].extC[f][i][j] * weights[c];
					weightsum[i][j] += chainlinks[c].extW[f][i][j] * weights[c];
				}
			}
		}

		for (NodeID i = 0; i < nodeCount; i++) {
			for (NodeID j = 0; j < nodeCount; j++) {
				Csum[i][j] = Csum[i][j] / participating;
				weightsum[i][j] = weightsum[i][j] / participating;
			}
		}

		result.extC[f] = Csum;
		result.extW[f] = weightsum;
	}

	return result;
}

Aggregation Aggregation::aggregate(vector<Model> chainlinks, vector<double> weights) {
	Aggregation cumAgg = Aggregation(chainlinks[0]);
	double cumSum = weights[0];

	for (unsigned i = 1; i < chainlinks.size(); i++) {
		cumAgg = aggregate(cumAgg, cumSum, Aggregation(chainlinks[i]), weights[i]);
		cumSum += weights[i];
	}
	return cumAgg;
}

Aggregation Aggregation::aggregate(vector<Model> chainlinks, unsigned burnin) {
	assert(burnin < chainlinks.size());
	Aggregation cumAgg = Aggregation(chainlinks[burnin]);
	double cumSum = 1;

	for (unsigned i = burnin+1; i < chainlinks.size(); i++) {
		cumAgg = aggregate(cumAgg, cumSum, Aggregation(chainlinks[i]), 1);
		cumSum += 1;
	}
	return cumAgg;
}

Aggregation Aggregation::aggregate(Model a, double weightA, Model b, double weightB) {
	assert(a.getNodeCount() == b.getNodeCount());
	assert(weightA >= 0);
	assert(weightB >= 0);
	vector<double> weights(2);
	vector<Model> chain(2);
	weights[0] = weightA;
	weights[1] = weightB;
	chain[0] = a;
	chain[1] = b;
	return aggregate(chain, weights);
}

Aggregation Aggregation::aggregate(Aggregation a, double weightA, Aggregation b, double weightB) {
	assert(weightA != 0  || weightB != 0);
	if (weightA == 0) return b;
	if (weightB == 0) return a;
	assert(a.nodeCount == b.nodeCount);
	assert(weightA >= 0);
	assert(weightB >= 0);
	vector<double> weights(2);
	vector<Aggregation> chain(2);
	weights[0] = weightA;
	weights[1] = weightB;
	chain[0] = a;
	chain[1] = b;
	return aggregate(chain, weights);
}

Model Aggregation::collapse(Settings settings) {
	/**
	 * constructing feature matrix:
	 * If each node has Poisson(\alpha) features, the estimated total feature population is n*\alpha
	 * Sort entries of aggregated Z. Pick n*\alpha highest ones.
	 *
	 *Other option: Create feature matrix with k different values for feature threshold, sample alpha of each, pick best fit
	 *
	 * Construction Cluster Matrix:
	 *
	 *
	 */

	if (!settings.adaptiveThresholds) return collapse(settings.cthreshold, settings.fthreshold, settings.sthreshold);

	double harm = 0;
	for (NodeID i = 1; i <= nodeCount; i++) harm += 1.0f/i;

	int repeat = 10;
	double stepsize = 0.05;
	vector<double> alphas;

	for (double fth = 0; fth <= 1; fth += stepsize) {
		FeatureID collapsedFeatures = 0;
		for (FeatureID f = 0; f < featureCount; f++) {
			bool featureTaken = false;
			for (NodeID i = 0; i < nodeCount; i++) {
				if (extZ[f][i] > fth) {
					featureTaken = true;
					break;
				}
			}
			if (featureTaken) collapsedFeatures++;
		}
		double alphasum = 0;
		for (int a = 0; a < repeat; a++) {
			alphasum += sampleGamma(collapsedFeatures+1, 1 / (1 + harm));
		}
		double collapsedalpha = alphasum / repeat;
		alphas.push_back(collapsedalpha);
	}

	double bestfit = 0;
	for (unsigned j = 0; j < alphas.size(); j++) {
		if (abs(alphas[j] - this->alpha) < abs(alphas[bestfit] - this->alpha)) {
			bestfit = j;
		}
	}

	double featurethreshold = bestfit*stepsize;

	FeatureMatrix tempZ(nodeCount);
	vector<int> feapop;
	for (FeatureID f = 0; f < featureCount; f++) {
		int population = 0;
		vector<bool> featureSlice(nodeCount, false);

		for (NodeID i = 0; i < nodeCount; i++) {
			if (extZ[f][i] > featurethreshold) {
				population++;
				featureSlice[i] = true;
			}
		}
		if (population > 0) {
			for (NodeID i = 0; i < nodeCount; i++) {
				tempZ[i].push_back(featureSlice[i]);
			}
			feapop.push_back(population);
		}
	}

	FeatureID newFeatureCount = feapop.size();
	assert(tempZ[0].size() == newFeatureCount);

	vector<double> gammas;
	for (double cth = 0; cth <= 1; cth += stepsize) {
		ClusterMatrix tempC(nodeCount);
		vector<vector<int> > clusterpop;
		for (NodeID i = 0; i < nodeCount; i++) tempC[i].resize(newFeatureCount);
		for (FeatureID f = 0; f < newFeatureCount; f++) {
			assert(f < tempZ[0].size());
			collapseClusters(tempZ, f, tempC, cth);
			ClusterID clusterCount = Model::countClusters(&tempC, f);
			vector<int> population(clusterCount, 0);
			for (NodeID i = 0; i < nodeCount; i++) {
				assert(f < tempZ[i].size());
				if (tempZ[i][f]) {
					population[tempC[i][f]]++;
				}
			}
			clusterpop.push_back(population);
		}
		CRPPDF pdf(&feapop, &clusterpop, settings);
		double gammasum = 0;
		for (int i = 0; i < repeat; i++) {
			gammasum += Sampler::loginverseTransformSampling(&pdf, 0, 10, 0.1);
		}
		double tgamma = gammasum / repeat;
		gammas.push_back(tgamma);
	}

	double bestgammafit = 0;
	for (unsigned j = 0; j < gammas.size(); j++) {
		if (abs(gammas[j] - this->gamma) < abs(gammas[bestgammafit] - this->gamma)) {
			bestgammafit = j;
		}
	}

	double clusterthreshold = bestgammafit*stepsize;
	//cout << "Choosing featurethreshold " << featurethreshold << " and clusterthreshold" << clusterthreshold << endl;
	return collapse(clusterthreshold, featurethreshold, 0);

}

void Aggregation::collapseToTwo(FeatureMatrix& Z, FeatureID& f,
		ClusterMatrix& C)  {
	double stepsize = 0.01;
	NodeID nodeCount = Z.size();
	for (double cth = 1; cth >= 0; cth -= stepsize) {
		for (NodeID i = 0; i < nodeCount; i++) {
			C[i][f] = -1;
		}
		collapseClusters(Z, f, C, cth);
		int clusterCount = 0;
		for (NodeID i = 0; i < Z.size(); i++) {
			if (C[i][f] >= clusterCount) clusterCount = C[i][f]+1;
		}
		if (clusterCount == 2) break; //mission accomplished!
		if (clusterCount < 2) {
			cout << "Could not bring clusterCount in feature " << f << " to 2." << endl;
			for (NodeID i = 0; i < nodeCount; i++) {
				C[i][f] = -1;
			}
			collapseClusters(Z, f, C, cth+stepsize);
			break;
		}
	}
}

void Aggregation::collapseClusters(FeatureMatrix& Z, FeatureID& f,
		ClusterMatrix& C, double clusterthreshold) {
	ClusterID currentCluster = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		if (Z[i][f]) {
			if (C[i][f] == -1) {
				C[i][f] = currentCluster;
				currentCluster++;
			}
			for (NodeID j = 0; j < nodeCount; j++) {
				if (extC[f][i][j] > clusterthreshold) {
					if (C[j][f] == -1)
						C[j][f] = C[i][f];

					if (C[j][f] != -1 && C[j][f] != C[i][f]) {
						//cout << "Inconsistency:" << "(" << i << "," << j << "):" << extC[f][i][j] << " > " << clusterthreshold;
						//cout << ", but " << C[j][f] << "!=" << C[i][f] << endl;
					}
				}
			}
		}
	}
}

ClusterMatrix Aggregation::collapseToTwoOld(FeatureMatrix * Z, vector<vector<vector<double> > > connectedness) {
	NodeID nodeCount = Z->size();
	FeatureID featureCount = connectedness.size();
	ClusterMatrix result(nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) result[i].resize(featureCount);

	for (FeatureID m = 0; m < featureCount; m++) {
		int pop = 0;
		for (NodeID i = 0; i < nodeCount; i++) {
			if ((*Z)[i][m]) {
				result[i][m] = pop;
				pop++;
			} else {
				result[i][m] = -1;
			}
		}
		if (pop == 0 || pop == 1) continue;
		for (int k = 0; k < pop - 2; k++) {//exactly two clusters should remain
			int maxcI = -1;
			int maxcJ = -1;
			double prevmax = 0;
			for (NodeID i = 0; i < nodeCount; i++) {
				if (!(*Z)[i][m]) continue;
				for (NodeID j = 0; j < nodeCount; j++) {
					if (!(*Z)[j][m]) continue;
					if (connectedness[m][i][j] >= prevmax
							&& result[i][m] != result[j][m]) {
						prevmax = connectedness[m][i][j];
						maxcI = result[i][m];
						maxcJ = result[j][m];
					}
				}
			}
			if (maxcI > maxcJ) {
				ClusterID temp = maxcI;
				maxcI = maxcJ;
				maxcJ = temp;
			}
			for (NodeID i = 0; i < nodeCount; i++) {
				if (result[i][m] == maxcJ)
					result[i][m] = maxcI;
			}
		}
		for (NodeID i = 0; i < nodeCount; i++) {
			bool secondencountered = false;
			int sec = 0;
			if (result[i][m] > 0) {
				if (!secondencountered) {
					sec = result[i][m];
					result[i][m] = 1;
					secondencountered = true;
				} else {
					assert(sec == result[i][m]);
					result[i][m] = 1;
				}
			}
		}
	}
	return result;
}

Model Aggregation::collapse(double clusterthreshold, double featurethreshold, double sparsethreshold) {
	/**
	 * featurethreshold: set Z[i][m to 1 if extZ[m][i] >= featurethreshold
	 * clusterthreshold: put two nodes in same cluster iff similarity > clusterthreshold
	 */
	FeatureMatrix Z(nodeCount);
	ClusterMatrix C(nodeCount);
	WeightMatrices W(featureCount);
	for (NodeID i = 0; i < nodeCount; i++) {
		Z[i].resize(featureCount);
		C[i].resize(featureCount, -1);
	}
	vector<bool> featureTaken(featureCount);
	for (FeatureID f = 0; f < featureCount; f++) {
		double sum = 0;
		for (NodeID i = 0; i < nodeCount; i++) {
			sum += extZ[f][i];
			Z[i][f] = (extZ[f][i] > featurethreshold) ? 1 : 0;
		}
		collapseClusters(Z, f, C, clusterthreshold);
		featureTaken[f] = (bool) ((sum >= sparsethreshold * nodeCount));//TODO: implement sparsethreshold
	}

	W = averageWeights(C, extW);
	Model result = Model(Z, C, W, alpha, gamma, s, sigma);
	result.purgeUnusedFeatures();
	//TODO: currently, mapping to representatives doubles the weights.
	//result = Features::mapToRepresentative(result);
	return result;
}

WeightMatrices Aggregation::averageWeights(ClusterMatrix clusters, vector<vector<vector<double> > > redWeights) {
	WeightMatrices result;
	NodeID nodeCount = clusters.size();

	assert(nodeCount > 0);
	result.resize(redWeights.size());
	for (FeatureID f = 0; f < result.size(); f++) {
		ClusterID clusterCount = Model::countClusters(&clusters, f);
		vector<vector<int> > occurence(clusterCount);
		result[f].resize(clusterCount);
		for (ClusterID c = 0; c < clusterCount; c++) {
			occurence[c].resize(clusterCount);
			result[f][c].resize(clusterCount);
		}
		assert(redWeights[f].size() == nodeCount);
		for (NodeID i = 0; i < nodeCount; i++) {
			if (clusters[i][f] == -1) continue;
			for (NodeID j = 0; j < nodeCount; j++) {
				if (clusters[j][f] == -1) continue;
				result[f][clusters[i][f]][clusters[j][f]] += redWeights[f][i][j];
				occurence[clusters[i][f]][clusters[j][f]]++;
			}
		}
		for (ClusterID c = 0; c < clusterCount; c++) {
			for (ClusterID d = 0; d < clusterCount; d++) {
				result[f][c][d] = result[f][c][d] / occurence[c][d];
			}
		}
	}
	return result;
}

/* namespace CILA */

}


