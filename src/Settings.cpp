/*
 * Settings.cpp
 *
 *  Created on: 30.06.2013
 *      Author: moritz
 */

#include <iostream>
#include "Settings.h"

using std::cout;

namespace CILA {
namespace core {

bool Settings::isValid() {
	if (sequential && randommodel) return false;
	if (leftOrder && distReorder) return false;
	if (iterations <= 0) return false;
	if (chains <= 0) return false;
	if (sigma_bias < 0 || sigma_weights < 0 || m_aux < 0 || init_variance < 0) return false;
	if (cthreshold < 0 || cthreshold > 1) return false;
	if (fthreshold < 0 || fthreshold > 1) return false;
	//if (sthreshold < 0 || sthreshold > 1) {
	//	cout << sthreshold << endl;
	//	return false;
	//}
	if (naivebias && conjugatebias) return false;
	if (naiveWeights && conjugateWeights) return false;
	if ((!sample_variance) && (init_variance == 0)) return false;
	if (sample_Z && !sample_C) return false;
	if (sample_C && !sample_weights) return false;
	if (crp_only && sample_Z) return false;
	return true;
}

Settings Settings::getDefaultSettings() {
	Settings result;
	result.iterations = 500;
	result.chains = 1;
	result.repeat = 50;
	result.pvaluePerm = 1000;

	result.mu_bias = 0;
	result.sigma_bias = 10;
	result.mu_weights = 0;
	result.sigma_weights = 10;
	result.gamma_scale = 1;
	result.gamma_shape = 1;
	result.alpha_shape = 1;
	result.alpha_scale = 1;
	result.variance_shape = 0.01;
	result.variance_scale = 0.01;
	result.init_alpha = 1;
	result.init_gamma = 1;
	result.init_bias = 0;
	result.init_variance = 1;
	result.crp_only = false;
	result.sample_alpha = true;
	result.sample_gamma = true;
	result.sample_Z = true;
	result.sample_C = true;
	result.sample_weights = true;
	result.sample_bias = true;
	result.sample_variance = false;

	result.m_aux = 10;
	result.retryOld = false;
	result.classicNewFeatures = true;

	result.saveStates = true;

	result.sequential = false;
	result.empty = true;
	result.randommodel = false;

	result.stepsPerNode = 3;

	result.nodeCount = 50;
	result.randomgraph = false;
	result.euclid = false;
	result.realnoise = 0;

	result.silent = false;
	result.permute = false;
	result.aggregate = false;
	result.plot = false;
	result.maxPlotPoints = 1000;
	result.limit = 70;

	result.leftOrder = true;
	result.distReorder = false;
	result.adaptiveThresholds = false;
	result.cthreshold = 0.5;
	result.fthreshold = 0.5;
	result.sthreshold = 0.1;

	result.num_threads = 2;
	result.maxTLC = 1250;
	result.TLClimitActive = false;
	result.majorversion = 0;
	result.minorversion = 14;
	result.subminorversion = 1;

	result.inputpath = "";//inputdata.csv
	result.gtpath = ""; //ground_truth_comparison.csv
	result.normalize_input = false;

	result.naiveWeights = false;
	result.conjugateWeights = true;
	result.naivebias = false;
	result.conjugatebias = true;
	result.naiveclusters = false;
	result.compressZ = true;

	result.burnin = 50;
	result.thin = 25;

	return result;
}

void Settings::dumpSettings() {
	cout << "Settings dump:" << endl;
	cout << "iterations: " << iterations << endl;
	cout << "chains: " << chains << endl;

	cout << "repeat: " << repeat << endl;
	cout << "mu_bias: " << mu_bias << endl;
	cout << "sigma_bias: " << sigma_bias << endl;
	cout << "mu_weights: " << mu_weights << endl;
	cout << "sigma_weights: " << sigma_weights << endl;
	cout << "gamma_scale: " << gamma_scale << endl;
	cout << "alpha_shape: " << alpha_shape << endl;
	cout << "init_alpha: " << init_alpha << endl;
	cout << "init_gamma: " << init_gamma << endl;
	cout << "init_bias: " << init_bias << endl;
	cout << "retryOld: " << retryOld << endl;
	cout << "classicNewFeatures: " << classicNewFeatures << endl;
	cout << "init_variance" << init_variance << endl;
	cout << "sample_alpha" << sample_alpha << endl;
	cout << "sample_gamma" << sample_gamma << endl;
	cout << "sample_Z" << sample_Z << endl;
	cout << "sample_C" << sample_C << endl;
	cout << "sample_weights" << sample_weights << endl;
	cout << "sample_bias" << sample_bias << endl;
	cout << "sample_variance" << sample_variance << endl;
	cout << "sequential: " << sequential << endl;
	cout << "empty: " << empty << endl;
	cout << "randommodel: " << randommodel << endl;
	cout << "stepsPerNode: " << stepsPerNode << endl;
	cout << "silent: " << silent << endl;
	cout << "permute: " << permute << endl;
	cout << "plot: " << plot << endl;
	cout << "limit: " << limit << endl;
	cout << "nodeCount: " << nodeCount << endl;
	cout << "leftOrder: " << leftOrder << endl;
	cout << "distReorder: " << distReorder << endl;
	cout << "cthreshold: " << cthreshold << endl;
	cout << "fthreshold: " << fthreshold << endl;
	cout << "sthreshold: " << sthreshold << endl;
	cout << "num_threads: " << num_threads << endl;
	cout << "version: " << getVersionString() << endl;
}

string Settings::getVersionString() {
	return std::to_string(majorversion)
			+ string(".")
			+ std::to_string(minorversion)
			+ string(".")
			+ std::to_string(subminorversion);
}

} /* namespace core */
} /* namespace CILA */
