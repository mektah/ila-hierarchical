/*
 * stochastics.cpp
 *
 *  Created on: 01.12.2012
 *      Author: moritz
 */

#include <cstdlib>
#include <iostream>
#include <assert.h>
#include <random>

#include "generative.h"
#include "types.h"

using namespace std;

namespace CILA {
namespace core {

std::mt19937 generator;

void setSeed(unsigned seed) {
	generator.seed(seed);
}

FeatureMatrix sampleFeatureMatrixwithIBP(size_t n, float alpha) { //maybe return array instead of vector, length shouldn't be changed after generation
	FeatureMatrix result(n);
	vector<double> popularity;
	unsigned int totalMealSamples = 0;
	unsigned int oldNumberofSamples = 0;
	unsigned int totalNumberofDishes = 0;

	for (NodeID i = 0; i < n; i++) {
		unsigned popularitySanityCheck = 0;
		result[i].resize(totalNumberofDishes, false);
		for (FeatureID k = 0; k < result[i].size(); k++) {
			popularitySanityCheck += popularity[k];
			//probability diner i samples dish k is popularity[k] / (totalMealSamples + alpha)
			// I took the following conditional from the ILA source, but I'm not sure why (or even if!) it's correct.
			//TODO: Investigate!
			if (popularity[k] / (i+1) > sampleUniform(0,1)) {
				//diner i samples dish k
				popularity[k]++;
				totalMealSamples++;
				result[i][k] = true;
			}
		}

		FeatureID newDishes = samplePoisson(alpha / (i+1));
		for (FeatureID k = 0; k < newDishes; k++) {
			//diner samples previously untried dish
			popularity.push_back(1);
			totalMealSamples++;
			totalNumberofDishes++;
			result[i].push_back(true);
		}
		assert(popularitySanityCheck == oldNumberofSamples);
		assert(totalNumberofDishes == popularity.size());
		assert(totalNumberofDishes == result[i].size());
		oldNumberofSamples = totalMealSamples;
	}
	for (NodeID i = 0; i < n; i++) {
		assert(result[i].size() <= totalNumberofDishes);
		result[i].resize(totalNumberofDishes, false);
	}
	return result;
}

ClusterID seatSingleCRP(ClusterID clusters, vector<int> seatedGuests, int totalGuests, double alpha, int previousCluster) {
	//assign cluster
	//probability of table l is proportional to seatedGuests[l] / totalGuests + alpha
    std::uniform_real_distribution<double> dist(0, 1);
	double assignment = dist(generator);
	double prefixsum = 0;

	for (ClusterID c = 0; c < clusters; c++) {
		prefixsum += (double) seatedGuests[c] / (totalGuests + alpha); //well, we could use i-1 instead of totalGuests;
		if (assignment <= prefixsum) { //\le vs \leq? discuss!
			//seat here!
			return c;
		}
	}

	//new table!
	assert(1-assignment < alpha / (totalGuests + alpha));
	return clusters;
}

ClusterMatrix sampleClusterAssignmentsbyCRP(size_t n, double lambda,
		vector<vector<bool> > featurePresent) {
	ClusterMatrix result(n);
	for (NodeID i = 0; i < n; i++) {
		result[i].resize(featurePresent[i].size(), -1);//how did this not raise an error when ClusterMatrix was unsigned?
		assert(result[i].size() == result[0].size());
	}
	assert(n == featurePresent.size());
	for (FeatureID k = 0; k < featurePresent[0].size(); k++) {
		vector<int> seatedGuests;
		ClusterID clusters = 0;
		int totalGuests = 0;
		for (NodeID i = 0; i < n; i++) {
			if (featurePresent[i][k]) {
				//assign cluster
				assert(seatedGuests.size() == clusters);
				ClusterID table = seatSingleCRP(clusters, seatedGuests, totalGuests, lambda, -1);
				result[i][k] = table;
				assert(table >= 0);
				assert(table <= clusters);
				if (table < clusters) {
					seatedGuests[table]++;
				}
				else {
					seatedGuests.push_back(1);
					clusters++;
				}
				totalGuests++;
			}
		}
	}

	for (NodeID i = 0; i < n; i++) {
		for (FeatureID k = 0; k < result[i].size(); k++) {
			assert (featurePresent[i][k] == (result[i][k] != -1));
		}
	}

	return result;
}

WeightMatrices sampleWeightMatrices(
		ClusterMatrix clusterAssignments, double mu_w, double sigma_w) {
	//get the number of clusters for each feature.

	vector<int> clusters(clusterAssignments[0].size());
	for (FeatureID k = 0; k < clusters.size(); k++) {
		int max = -1;
		for (NodeID i = 0; i < clusterAssignments.size(); i++) {
			if (clusterAssignments[i][k] > max)
				max = clusterAssignments[i][k];
		}

		//for every active feature, at least one non-empty subcluster must exist
		assert(max >= 0);
		clusters[k] = max + 1;
	}

	vector<vector<vector<double> > > result(clusters.size()); //one weight matrix for every feature
	for (FeatureID k = 0; k < clusters.size(); k++) {
		result[k].resize(clusters[k]);
		for (int l = 0; l < clusters[k]; l++) {
			/* TODO
			 * one day, when I'm sufficiently confident with the workings of clusters and don't need the -1 guards any more, change
			 * the type of clusters from int to ClusterID.
			 */
			result[k][l].resize(clusters[k]);
		}
		//two separate loops to make sure the result array is fully created before writing to it.
		for (int l = 0; l < clusters[k]; l++) {
			for (int j = 0; j <= l; j++) { //matrix should be symmetrical
				double weight = sampleNormal(mu_w, sigma_w);
				assert (weight == weight);
				result[k][l][j] = weight;
				result[k][j][l] = result[k][l][j];
			}
		}
	}
	//sanity check
	for (FeatureID k = 0; k < result.size(); k++) {
		for (ClusterID l = 0; l < result[k].size(); l++) {
			for (ClusterID m = 0; m < result[k][l].size(); m++) {
				//if nan, this assert will fail.
				assert(result[k][l][m] == result[k][l][m]);
				//if matrix is asymmetrical, this assert will fail;
				assert(result[k][l][m] == result[k][m][l]);
			}
		}
	}
	return result;
}

double sampleNormal(double mu_s, double sigma_s) {
	std::normal_distribution<double> distribution(mu_s,sigma_s);
	return distribution(generator);
}

double sampleGamma(double shape, double scale) {
	assert(shape > 0);
	assert(scale > 0);
	std::gamma_distribution<double> dist(shape);
	double result = scale*dist(generator) ;
	assert(result == result);
	return result;
}

FeatureID samplePoisson(double a) {
	if (a == 0) {
		cout << "Poisson with parameter 0? Odd." << endl;
		return 0;
	}
	assert(a > 0);
	//cout << a << endl;
	FeatureID result;
	std::poisson_distribution<unsigned int> distribution(a);
	result = distribution(generator);
	//if (result > 0) cout << "poisson!" << result << endl;
	//assert(result < 100);
	assert(result == result);
	assert(result >= 0);
	return result;
}

double sampleUniform(double a, double b) {
	assert(a < b);
	std::uniform_real_distribution<double> dist(a, b);
	double result = dist(generator);
	if (result == b) {
		result = std::nextafter(result, 0);
	}
	assert(result >= a);
	assert(result < b);
	return result;
}

int selectUniform(vector<double> list) {
	/*
	 * assume the sum is 1
	 */
	assert(list.size() > 0);
	double rand = sampleUniform();
	for (unsigned i = 0; i < list.size(); i++) {
		rand -= list[i];
		if (rand < 0) return i;
	}
	return list.size()-1;
}

double gammaProbabilityDensity(double shape, double scale, double x) {
	assert(shape > 1 || x > 0);
	assert(scale > 0);
	assert(shape > 0);
	double result = (1 / pow(scale, shape)) * (1 / tgamma(shape)) * pow(x, shape-1) * exp(-x/scale);
	assert(result < numeric_limits<double>::infinity());
	return result;
}

double normalProbabilityDensity(double mu, double sigma, double x) {
	return (1.0f / (sigma * sqrt(2*M_PI))) * exp((-(x-mu)*(x-mu))/2*sigma*sigma);
}

double normalLogProbabilityDensity(double mu, double sigma, double x) {
	return -log(sigma) -0.5*log(2*M_PI) - ((x-mu)*(x-mu))/(2*sigma*sigma);
}

vector<double> normalize(vector<double> a) {
	double sum = 0;
	for (unsigned i = 0; i < a.size(); i++) {
		assert(a[i] >= 0);
		sum += a[i];
	}
	assert(sum > 0);
	for (unsigned i = 0; i < a.size(); i++) a[i] = a[i] / sum;
	return a;
}

double normalize(double a, double b) {
	//now normalize these likelihoods
	assert(a >= 0);
	assert(b >= 0);
	assert(a+b > 0);
	return a / (a+b);
}

//TODO: maybe replace this with unsigned
int selectUniformLog(vector<double> list) {

	assert(list.size() > 0);
	double max = list[0];
	for (unsigned i = 0; i < list.size(); i++) {
		assert(list[i] == list[i]);
		if (list[i] > max) max = list[i];
	}

	//normalize using logs as good as possible for numeric stability
	for (unsigned i = 0; i < list.size(); i++) {
		list[i] = exp(list[i] - max);
		assert(list[i] >= 0);//this would only fail if list[i] was NaN
	}

	//normalize using real values
	list = normalize(list);

	//select
	return selectUniform(list);
}

vector<double> logNormalize(vector<double> list) {
	assert(list.size() > 0);
	double max = list[0];
	for (unsigned i = 0; i < list.size(); i++) {
		if (list[i] > max) max = list[i];
	}
		//normalize using logs as good as possible for numeric stability
	for (unsigned i = 0; i < list.size(); i++) {
		list[i] = exp(list[i] - max);
		assert(list[i] >= 0);
	}

	//normalize using real values
	list = normalize(list);

	for (unsigned i = 0; i < list.size(); i++) {
		list[i] = log(list[i]);
	}
	return list;
}

}
}
