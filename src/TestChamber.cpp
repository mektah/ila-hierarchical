/*
 * TestChamber.cpp
 *
 *  Created on: 31.05.2013
 *      Author: moritz
 */

#include "TestChamber.h"
#include "Plotting.h"
#include "Util.h"
#include "ModelIO.h"

namespace CILA {
using namespace io;

void TestChamber::fixedRuns(Settings settings) {
	NodeID nodeCount = settings.nodeCount;
	double alpha = sampleGamma(1,1);
	double gamma = sampleGamma(1,1);
	double mu_w = settings.mu_weights;
	double sigma_w = settings.sigma_weights;
	double mu_s = settings.mu_bias;
	double sigma_s = settings.sigma_bias;
	double sigma_edge = sampleGamma(1,1) / 2;
	double cthreshold = settings.cthreshold;
	double fthreshold = settings.fthreshold;
	double sparsethreshold = settings.sthreshold;

	double precision = 0.01;
	double limit = 3;

	int chains = settings.chains;
	int iterations = settings.iterations;
	assert(chains > 0);

	Model testModel = Model::generateModel(settings);
	Graph R = testModel.sampleGraphInstance(sigma_edge);

	Model fixed(testModel.getFeatureMatrix(), testModel.getClusterMatrix(), testModel.getWeights(), alpha, gamma, testModel.s, testModel.sigma);

	/**
	 * preparing settings object
	 */
	settings.sample_alpha = false;
	settings.sample_gamma = false;
	settings.sample_C = false;
	settings.sample_Z = false;
	settings.sample_weights = false;
	settings.sample_bias = false;
	settings.sample_variance = false;

	/**
	 * sample variance
	 */
	cout << "Sampling only variance." << endl;
	double sigmasum = 0;
	vector<double> variancepop(limit/precision, 0);
	vector<double> varpriorpop(limit/precision, 0);

	settings.sample_variance = true;
	#pragma omp parallel for reduction(+:sigmasum)
	for (int c = 0; c < chains; c++) {
		cout << c << " ";
		cout.flush();

		fixed.sigma = sampleUniform(0, 10);
		Sampler sampler(R, fixed, settings);

		for (int i = 0; i < iterations; i++) {
			sampler.sampleHyperVariance(0.01);
			sigmasum += sampler.getVariance();
			#pragma omp atomic
			variancepop[sampler.getVariance()/precision]++;
		}
	}
	cout << endl;
	fixed.sigma = testModel.sigma;
	double sigmamean = sigmasum / (chains*iterations);
	Plotting::plotHistogramm(variancepop, 0, limit, "Comparison-Variance");
	cout << "Original sigma:" << sigma_edge <<", Sampled sigma:" << sigmamean << endl;
	settings.sample_variance = false;

	/**
	 * set everything else fixed, sample bias
	 */
	cout << "Sampling only bias." << endl;
	settings.sample_bias = true;
	double biassum = 0;
	double upperlimit = 10;
	double lowerlimit = -10;
	vector<double> biaspop((upperlimit-lowerlimit)/precision, 0);
	#pragma omp parallel for reduction(+:biassum)
	for (int c = 0; c < chains; c++) {
		cout << c << " ";
		cout.flush();
		fixed.s = sampleNormal(mu_s, sigma_s);
		Sampler sampler(R, fixed, settings);

		for (int i = 0; i < iterations; i++) {
			sampler.sampleHyperBias();
			double bias = sampler.getBias();
			biassum += bias;
			if (bias < upperlimit && bias >= lowerlimit) {
			#pragma omp atomic
			biaspop[(bias-lowerlimit)/precision]++;
			}
		}
	}
	cout << endl;
	double biasmean = biassum / (chains*iterations);
	cout << "Original bias:" << testModel.s << ", sampled bias:" << biasmean << endl;
	Plotting::plotHistogramm(biaspop, lowerlimit,upperlimit,"Comparison-bias");
	settings.sample_bias = false;

	/**
	 * sample weights
	 */
	settings.sample_weights = true;
	cout << "Sampling only weights." << endl;
	vector<Aggregation> weightresult = TestChamber::fixedRun(testModel, R, false, false, false, false, true, false, false, settings);
	vector<double> distances(chains);
	for (unsigned i = 0; i < weightresult.size(); i++) {
		Model result = weightresult[i].collapse(cthreshold,fthreshold,sparsethreshold);
		distances[i] = Model::computeDistance(&result, &fixed);
	}
	Plotting::plotHistogramm(distances, "Distances of weight sampling");
	cout << "Distance of last average:" << distances[chains-1] << endl;

	/**
	 * sample bias + weights
	 */
	cout << "Sampling bias and weights" << endl;
	settings.sample_bias = true;
	vector<Aggregation> biasweights = TestChamber::fixedRun(testModel, R, false, false, false, false, true, true, false, settings);
	vector<double> biases(biasweights.size());
	distances.clear();
	distances.resize(chains);
	for (unsigned i = 0; i < biasweights.size(); i++) {
		Model result = biasweights[i].collapse(cthreshold,fthreshold,sparsethreshold);
		distances[i] = Model::computeDistance(&result, &fixed);
		biases[i] = result.s;
	}
	cout << "Distance of last average:" << distances[chains-1] << endl;
	Plotting::plotHistogramm(distances, "Distances of weight + bias sampling");
	Plotting::plotHistogramm(biases, "Bias of combined distance + weight sampling");
	cout << "Original bias:" << testModel.s << ", sampled bias:" << biases[chains-1] << endl;
	settings.sample_bias = false;
	settings.sample_weights = false;

	/**
	 * sample alpha
	 */
	cout << "Sampling only alpha." << endl;

	vector<double> alphapriorpop(limit/precision, 0);
	vector<double> alphapop(limit/precision, 0);
	vector<double> alphas;
	double cummean = 0;
	int alphasamples = 0;
	for (int c = 0; c < chains; c++) {
		Model copy = testModel;
		copy.alpha = sampleGamma(settings.alpha_shape,settings.alpha_scale);
		Sampler sampler(R, copy, settings);
		for (int i = 0; i < iterations; i++) {
			sampler.sampleHyperAlpha();

			cummean = (cummean * alphasamples + sampler.getAlpha()) / (alphasamples +1);
			alphasamples++;
		}
		alphas.push_back(cummean);
	}


	double harm = 0;
	for (unsigned i = 1; i <= nodeCount; i++) {
		harm += 1.0f/i;
	}
	double featureCount = testModel.weights.size();

	for (unsigned i = 1; i < alphapriorpop.size(); i++) {
		double x = precision*i;
		assert(x <= limit);
		alphapriorpop[i] = alphasamples * gammaProbabilityDensity(settings.alpha_shape, settings.alpha_scale, x)*precision;
		alphapop[i] = alphasamples * gammaProbabilityDensity(featureCount+settings.alpha_shape, settings.alpha_scale / ( settings.alpha_scale + harm ), x)*precision;
	}
	double directalpha = (featureCount+settings.alpha_shape) * (settings.alpha_scale / ( settings.alpha_scale + harm ));
	alphapriorpop[0] = alphapriorpop[1];//Ugly hack. TODO: fix this.
	alphapop[0] = alphapop[1];//Ugly hack. TODO: fix this.
	Plotting::plotHistogramm(alphapriorpop, alphapop, testModel.alpha, cummean, 0, limit, "Comparison-Alphas");
	Plotting::plotHistogramm(alphas, "Cumulative-Average-Alphas");
	cout << "Original alpha:" << testModel.alpha << ", sampled alpha:" << cummean << ", direct alpha: " << directalpha << endl;

	/**
	 * sample gamma
	 */
	cout << "Sampling only gamma." << endl;
	precision = 0.01;
	limit = 3;
	vector<double> gammapop(limit/precision, 0);
	vector<double> gammapriorpop(limit/precision, 0);
	vector<double> gammas;
	cummean = 0;
	int gammasamples = 0;
	for (int c = 0; c < chains; c++) {
		Model copy = testModel;
		copy.gamma = sampleGamma(1,1);
		Sampler sampler(R, copy, settings);
		for (int i = 0; i < iterations; i++) {
			sampler.sampleHyperGamma();
				if (sampler.getGamma() / precision < gammapop.size()) {
					gammapop[sampler.getGamma()/precision]++;
				}
				cummean = (cummean * gammasamples + sampler.getGamma()) / (gammasamples +1);
				gammasamples++;
			}

			gammas.push_back(cummean);
		}

	for (unsigned i = 1; i < gammapriorpop.size(); i++) {
		double x = precision*i;
		assert(x <= limit);
		gammapriorpop[i] = gammasamples * gammaProbabilityDensity(settings.gamma_shape, settings.gamma_scale, x)*precision;
	}
	gammapriorpop[0] = gammapriorpop[1];//Ugly hack. TODO: fix this.
	Plotting::plotHistogramm(gammapriorpop, gammapop, testModel.gamma, cummean, 0, limit, "Comparison-Gammas");
	Plotting::plotHistogramm(gammas, "Cumulative-Average-Gammas");
	cout << "Original gamma:" << testModel.gamma << ", sampled gamma:" << cummean << endl;

	/**
	 * sample C + weights
	 */
	cout << "Sampling clusters and weights" << endl;
	settings.sample_C = true;
	settings.sample_weights = true;

	vector<Aggregation> cplusweights = TestChamber::fixedRun(testModel, R, false, false, false, true, true, false, false, settings);
	distances.clear();
	distances.resize(chains);
	Model result;
	for (int i = 0; i < chains; i++) {
		result = cplusweights[i].collapse(cthreshold, fthreshold, sparsethreshold);
		distances[i] = Model::computeDistance(&result, &testModel);
	}
	Plotting::plotHistogramm(distances, "Distance of C + weights");
	cout << "Distance of last average:" << distances[chains-1] << endl;
	Settings paramcopy = settings;
	paramcopy.iterations = 100;
	paramcopy.chains = 1;
	vector<Aggregation> newalpha = TestChamber::fixedRun(result, R, true, true, false, false, false, false, false, paramcopy);
	cout << "Resampled alpha of average:" << newalpha[0].alpha << endl;
	cout << "Resampled gamma of average:" << newalpha[0].gamma << endl;

	/**
	 * sample C + weights + bias
	 */
	cout << "Sampling clusters, weights and bias." << endl;
	settings.sample_bias = true;
	vector<Aggregation> cweightsbias = TestChamber::fixedRun(testModel, R, false, false, false, true, true, true, false, settings);
	distances.clear();
	distances.resize(chains);
	biases.clear();
	biases.resize(chains);
	for (int i = 0; i < chains; i++) {
		result = cweightsbias[i].collapse(cthreshold, fthreshold, sparsethreshold);
		distances[i] = Model::computeDistance(&result, &testModel);
		biases[i] = result.s;
	}
	Plotting::plotHistogramm(distances, "Distance of C + weights + bias");
	cout << "Original bias:" << testModel.s << ", sampled bias:" << biases[chains-1] << endl;
	newalpha = TestChamber::fixedRun(result, R, true, true, false, false, false, false, false, paramcopy);
	cout << "Resampled alpha of average:" << newalpha[0].alpha << endl;
	cout << "Resampled gamma of average:" << newalpha[0].gamma << endl;

	/**
	 * sample Z + C + weights
	 */
	settings.sample_bias = false;
	settings.sample_Z = true;
	cout << "Sampling features, clusters and weights." << endl;
	vector<Aggregation> zcweights = TestChamber::fixedRun(testModel, R, false, false, true, true, true, false, false, settings);
	distances.clear();
	distances.resize(chains);
	for (int i = 0; i < chains; i++) {
		result = zcweights[i].collapse(cthreshold, fthreshold, sparsethreshold);
		distances[i] = Model::computeDistance(&result, &testModel);
	}
	Plotting::plotHistogramm(distances, "Distance of Z + C + weights");
	newalpha = TestChamber::fixedRun(result, R, true, true, false, false, false, false, false, paramcopy);
	cout << "Resampled alpha of average:" << newalpha[0].alpha << endl;
	cout << "Resampled gamma of average:" << newalpha[0].gamma << endl;

	/**
	 * sample C + sigma
	 */
	cout << "Sampling clusters, weights and sigma." << endl;
	vector<Aggregation> clusterssigma = TestChamber::fixedRun(testModel, R, false, false, false, true, true, false, true, settings);
	distances.clear();
	distances.resize(chains);
	for (int i = 0; i < chains; i++) {
		result = clusterssigma[i].collapse(cthreshold, fthreshold, sparsethreshold);
		distances[i] = Model::computeDistance(&result, &testModel);
	}
	cout << "Old sigma:" << sigma_edge << ", sampled sigma: " << result.sigma << endl;
	Plotting::plotHistogramm(distances, "Distance of C + weights + sigma");
	newalpha = TestChamber::fixedRun(result, R, true, true, false, false, false, false, false, paramcopy);
	cout << "Resampled alpha of average:" << newalpha[0].alpha << endl;
	cout << "Resampled gamma of average:" << newalpha[0].gamma << endl;

	/**
	 * sample Z + C + weights + bias
	 */

	/**
	 * full sampling
	 */
	cout << "Sampling everything! Beware of dragons!" << endl;
	vector<Aggregation> everythingburrito = TestChamber::fixedRun(testModel, R, true, true, true, true, true, true, true, settings);
	distances.clear();
	distances.resize(chains);
	for (int i = 0; i < chains; i++) {
		result = everythingburrito[i].collapse(cthreshold, fthreshold, sparsethreshold);
		distances[i] = Model::computeDistance(&result, &testModel);
	}
	cout << "Old sigma:" << sigma_edge << ", sampled sigma: " << result.sigma << endl;
	Plotting::plotHistogramm(distances, "Distance of everything");
	newalpha = TestChamber::fixedRun(result, R, true, true, false, false, false, false, false, paramcopy);
	cout << "Resampled alpha of average:" << newalpha[0].alpha << endl;
	cout << "Resampled gamma of average:" << newalpha[0].gamma << endl;
}

vector<Aggregation> TestChamber::fixedRun(Model def, Graph R, bool alpha, bool gamma, bool z, bool c, bool w, bool s, bool sigma, Settings settings) {
	Aggregation cumAgg;
	vector<Aggregation> result;
	NodeID nodeCount = def.getNodeCount();
	settings.sample_alpha = alpha;
	settings.sample_gamma = gamma;
	settings.sample_Z = z;
	settings.sample_C = c;
	settings.sample_weights = w;
	settings.sample_bias = s;
	settings.sample_variance = sigma;
	cout << "Chain:";
	#pragma omp parallel for shared(cumAgg, result)
	for (int ch = 0; ch < settings.chains; ch++) {
		Model copy = def;
		if (alpha) {
			copy.alpha = sampleGamma(1,1);
		}
		if (gamma) {
			copy.gamma = sampleGamma(settings.gamma_shape,settings.gamma_scale);
		}
		if (z || c || w) {
			FeatureMatrix newf;
			ClusterMatrix newc;

			if (z) {
				assert(c);
				assert(w);
				newf = sampleFeatureMatrixwithIBP(nodeCount, def.alpha);
			} else {
				newf = copy.getFeatureMatrix();
			}

			if (c) {
				assert(w);
				newc = sampleClusterAssignmentsbyCRP(nodeCount,def.gamma, newf);
			} else {
				newc = copy.getClusterMatrix();
			}

			WeightMatrices weights = sampleWeightMatrices(newc, settings.mu_weights, settings.sigma_weights);
			copy = Model(newf, newc, weights, copy.alpha, copy.gamma, copy.s, copy.sigma);
			copy.purgeUnusedFeatures();
		}
		if (s) {
			copy.s = sampleNormal(settings.mu_bias, settings.sigma_bias);
		}
		if (sigma) {
			copy.sigma = sampleUniform(0,10);
		}

		Sampler sampler(R, copy, settings);
		for (int i = 0; i < settings.iterations; i++) {
			sampler.sampleIter();
		}
		Aggregation chainagg = Aggregation::aggregate(sampler.getChain());
		#pragma omp critical
		{
			if (result.size() == 0) {
				cumAgg = chainagg;
			} else {
				cumAgg = Aggregation::aggregate(cumAgg, result.size(), chainagg, 1);
			}
			result.push_back(cumAgg);
			cout << ch << " ";
			cout.flush();
		}
	}
	cout << endl;
	return result;
}

/**
 * This method does not save all samples generated from the markov chain, but returns only the aggregation
 */
Aggregation TestChamber::directAggregation(Model model, Graph graph, Settings settings, string name) {
	Sampler sampler(graph, model, settings);

	Aggregation result(model);//the argument doesn't matter, since it goes into the averaging with a weight of 0.
	int i = 0;
	int j = 0;
	string filename = ModelIO::writeSampler(settings, name, sampler, false);

	while (i < settings.iterations) {
		sampler.sampleIter();
		i++;
		if (i >= settings.burnin && i % settings.thin == 0) {
			Model model = sampler.getModel();
			result = Aggregation::aggregate(result, j, Aggregation(model), 1);
			j++;
		}
	}
	return result;
}

Sampler TestChamber::testrun(Model model, Graph graph, Settings settings, string name) {
	/**
	 * Start the inversion algorithm.
	 * 	model: Initial state of the markov chain.
	 * 	graph: The graph R to be explained
	 * 	variance: Variance of the gaussian noise on R.
	 * 	weightprecision: Size of the sampling step to sample R. Since the runtime is dominated by weight sampling, time is proportional to 1/weightprecision
	 * 	m_aux: How many temporary new clusters are created during cluster assignment. If set to 0, no new clusters are created
	 * 	permute: Whether to permute the sampler and graph randomly after every step to inhibit order effects
	 * 	plot: Whether to plot statistics at the end
	 * 	sequential: Use sequential initialisation?
	 * 	stepsPerNode: When using sequential initialisation, how many sampling steps after each additional node?
	 */
	time_t start = time(NULL);
	bool silent = settings.silent;
	Sampler sampler;
	NodeID n = graph.size();
	if (settings.sequential) {
		/**
		 * Start sequential initialization.
		 */
		vector<NodeID> newperm = settings.permute ? Util::randPermutation(n) : Util::dummyPermutation(n);
		graph.permute(newperm);
		vector<vector<edgetype> > seqMatrix;
		seqMatrix.push_back(graph.getNodeSlice(0));
		sampler = Sampler(Graph(seqMatrix), model, settings);
		sampler.clusterSanityCheck();
		for (NodeID i = 1; i < n; i++) {
			if (n > 10 && (i % (n / 10) == 0) && !silent) {
				cout << i / (n / 10) <<"0%...";
				cout.flush();
			}
				sampler.addNode(graph.getNodeSlice(i));
				for (int j = 0; j < settings.stepsPerNode; j++) {
					sampler.sampleIter();
				}
			}
			if (!silent) cout << endl;
			vector<NodeID> rev = Util::reverse(newperm);
			sampler.permute(rev);
			graph.permute(rev);
	} else {
		 sampler = Sampler(graph, model, settings);
	}

	/*
	 * main sampling loop
	 */

	for (int i = 0; i < settings.iterations; i++) {
		sampler.sampleIter();
		//cout << "settings.thin:" << settings.thin << endl;
		//cout << i << " % " << settings.thin << ": " << i % settings.thin << endl;
		if (i % 25 == 0 && !silent) {
			cout << i << ": ";
			cout << Model::logLikelihood(&graph, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance());
			cout << ", " << time(NULL) - start << " seconds elapsed, with " << sampler.featureCount() << " features and " << sampler.totalClusterCount();
			cout << " clusters. ";
			if (sampler.chain.back().getTotalLinkCount() > 100) cout << "TLC: " << sampler.chain.back().getTotalLinkCount();
			cout << " Bias:" << sampler.getBias();
			cout << " Alpha:" << sampler.getAlpha();
			cout << " Gamma:" << sampler.getGamma();
			cout << " Variance:" << sampler.getVariance() << endl;
		}
	}
	double postLL = Model::logLikelihood(&graph, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance());
	if (!silent) cout << "Likelihood after sampling: " << postLL <<endl;

	/**
	FeatureID featureCount = sampler.featurePresent[0].size();
	for (FeatureID i = 0; i < featureCount; i++) {
		if (Features::countClusters(&sampler.clusterAssignment, i) == 0) continue;
		for (FeatureID j = 0; j < featureCount; j++) {
			if (Features::countClusters(&sampler.clusterAssignment, j) == 0) continue;
			if (Features::isDependent(&sampler.clusterAssignment, i, j) && i != j) {
				cout << "Feature " << j << " is redundant given " << i << "! Starting merge." << endl;
				Features::mergeFeatures(&sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, i, j);
				sampler.countClusters();
			}
		}
	}
	**/

	Model result = sampler.getChain().back();

	//printClusters(result.getClusterAssignments());
	assert(Model::computeDistance(&result, &result) == 0);
	Model oldr = result;
	if (!silent) cout << "Mapping ..." << endl;
	result = Model::mapToRepresentative(result);
	result.purgeUnusedFeatures();
	//assert(Features::computeDistance(&oldr, &result) == 0);//This assertion won't always hold. For testing purposes only.
	if (!silent) {
		Model::printClusters(result.getClusterMatrix());

		result.printLikelihoodContribution(graph);

		result.printWeightMatrices();

		if (result.getFeatureCount() > 1) {
			cout << " Distance of first and second feature:" << Model::computeDistance(&result, 0, &result, 1) << endl;
		}

		if (settings.aggregate) {
			Aggregation agg = Aggregation::aggregate(sampler.getChain());
			Model aggResult = agg.collapse(settings);

			cout << "Aggregated results:" << endl;
			Model::printClusters(aggResult.getClusterMatrix());
		}
	}

	/**
	for (int i = featureCount-1; i > 0; i--) {
		Features::mergeFeatures(&sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, i-1, i);
		printClusters(sampler.clusterAssignment);
		double postMergeLL = Features::logLikelihood(&graph, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance());
		assert(postMergeLL == postLL);
		printLikelihoodContribution(sampler, graph);
		sampler.countClusters();
	}
	if (settings.plot) {
		if (!settings.silent) cout << "Plotting..." << endl;
		vector<Sampler> runlist;
		runlist.push_back(sampler);
		Analysis::analyze(&runlist);
	}
	*/

	return sampler;
}

double TestChamber::averageQuality(Settings settings, int graphiterations, double maxnoise, bool samplealpha, bool samplegamma) {
	int modeliterations = settings.repeat;

	double sum = 0;
	vector<int> entries(graphiterations);
	vector<double> nmis(graphiterations);
	vector<vector<double> > wholeNMIs(graphiterations);
	#pragma omp parallel for shared(nmis, entries, sum) schedule(dynamic, 1)
	for (int i = 0; i < modeliterations; i++) {
		Model randomly = Model::generateModel(settings);
		double alpha, gamma;
		if (samplealpha) randomly.alpha = sampleGamma(settings.alpha_shape, settings.alpha_scale);
		if (samplegamma) randomly.gamma = sampleGamma(settings.gamma_shape, settings.gamma_scale);

		ClusterMatrix origclusters = randomly.getClusterMatrix();
		FeatureID origfcount = origclusters[0].size();
		double modelsum = 0;
		vector<double> localnmidump;
		for (int j = 0; j < graphiterations; j++) {
			double variance = maxnoise * ((double) j / (graphiterations));
			Graph randomized = randomly.sampleGraphInstance(variance);
			assert(!settings.randommodel || !settings.empty);
			settings.init_variance = std::max(variance,0.5);

			Model samplemodel;
			if (settings.randommodel || settings.sequential) {
				samplemodel = Model::generateModel(settings);
				samplemodel.alpha = alpha;
				samplemodel.gamma = gamma;
			} else if (settings.empty) {
				samplemodel = Model::emptyModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.init_bias);
			}

			Sampler result = TestChamber::testrun(samplemodel, randomized, settings);
			FeatureID ownfcount = result.featureCount();

			double avgavgnmi = 0;

			for (int k = settings.iterations/2; k < settings.iterations; k++) {
				double nmi;
				if (ownfcount == 0 && origfcount == 0) {
					nmi = 1;
				} else if (ownfcount > 0 && origfcount > 0) {
					nmi = Util::averagePurityAndNMIMax(result.chain[k].clusterAssignment, origclusters)[1];
				} else {
					nmi = 0;
				}
				localnmidump.push_back(nmi);
				avgavgnmi += nmi;
			}

			avgavgnmi =  avgavgnmi / (settings.iterations - settings.iterations/2);

			#pragma omp critical
			{
				wholeNMIs[j].insert(wholeNMIs[j].end(), localnmidump.begin(), localnmidump.end());
				nmis[j] += avgavgnmi;
				entries[j]++;
			}
			modelsum += avgavgnmi;
			cout << "graph " << j << " of " << graphiterations << ": NMI " << avgavgnmi << ", var " << variance << endl;;
		}
		#pragma omp critical
		{
			vector<double> realnmis(graphiterations);
			for (int k = 0; k < graphiterations; k++) {
				assert(entries[k] > 0);
				realnmis[k] = nmis[k] / entries[k];
			}
			Plotting::plotHistogramm(realnmis, 0, maxnoise, "NMIAverage vs. Noise");
		}
		modelsum = modelsum / graphiterations;
		cout << "model " << i << " of " << modeliterations << ": NMI " << modelsum << endl;
		#pragma omp atomic
		sum += modelsum;
	}
	//Plotting::plotHistogramm(nmis, 0, maxnoise, "NMIAverage vs. Noise");
	Plotting::plotBoxplot(wholeNMIs, 0, maxnoise, "NMIAverage-vs-Noise-boxplot");
	return sum / modeliterations;
}

void TestChamber::singleParmap(double min, double max, double precision, int parameter, Settings settings) {
	int steps = (max - min) / precision;
	assert(steps > 0);
	vector<double> direct(steps);
	vector<double> fromR(steps);
	vector<double> samples(steps);
	vector<double> directOut(steps);
	vector<double> fromROut(steps);
	vector<vector<double> > directboxplot(steps);
	vector<vector<double> > fromRboxplot(steps);
	vector<string> names(6);
	names[0] = "Alpha";
	names[1] = "Gamma";
	names[2] = "Bias";
	names[3] = "Noise";

	settings.sample_alpha = true;
	settings.sample_gamma = true;
	settings.sample_bias = (parameter == 2);
	settings.sample_Z = false;
	settings.sample_C = false;
	settings.sample_weights = false;
	settings.sample_variance = (parameter == 3);

	#pragma omp parallel for schedule(dynamic, 1)
	for (int index = 0; index < steps; index++) {
		double value = index*precision + min;
		#pragma omp parallel for schedule(dynamic, 1)
		for (int j = 0; j < settings.repeat; j++) {
			double alpha = sampleGamma(1,1);
			double gamma = sampleGamma(1,1);
			double noise = 0;
			if (parameter == 0) alpha = value;
			if (parameter == 1) gamma = value;
			Model model = Model::generateModel(settings);
			model.alpha = alpha;
			model.gamma = gamma;
			if (parameter == 2) model.s = value;
			if (parameter == 3) noise = value;

			Graph R = model.sampleGraphInstance(noise);
			double mean = 0;
			Sampler sampler(R, model, settings);
			vector<bool> doSample(4,false);
			doSample[parameter] = true;

			vector<double> result(4);

			for (int i = 0; i < settings.iterations; i++) {
				sampler.sampleIter();
				result[0] = sampler.getAlpha();
				result[1] = sampler.getGamma();
				result[2] = sampler.getBias();
				result[3] = sampler.getVariance();
				mean += result[parameter];
			}
			mean = mean / settings.iterations;

			Model newmodel = Model::generateModel(settings.nodeCount, settings.mu_weights, settings.sigma_weights, settings.mu_bias, settings.sigma_bias);
			Settings settingscopy = settings;
			settingscopy.sample_bias = true;
			sampler = Sampler(R, newmodel, settingscopy);

			double meanR = 0;
			vector<double> valueR(4);

			for (int i = 0; i < settings.iterations; i++) {
				sampler.sampleIter();
				if (i >= ((double) settings.iterations) / 2) {
					valueR[0] = sampler.getAlpha();
					valueR[1] = sampler.getGamma();
					valueR[2] = sampler.getBias();
					valueR[3] = sampler.getVariance();
					meanR += valueR[parameter];
				}
			}
			meanR = meanR / (settings.iterations/2);

			#pragma omp critical
			{
				samples[index]++;
				directboxplot[index].push_back(mean);
				fromRboxplot[index].push_back(meanR);
				direct[index] += mean;
				fromR[index] += meanR;
				directOut[index] = direct[index] / samples[index];
				fromROut[index] = fromR[index] / samples[index];
				Plotting::plotHistogramm(directOut, min, max, names[parameter] + string("-direct"));
				Plotting::plotHistogramm(fromROut, min, max, names[parameter] + string("-R"));
			}
		}
		cout << "Mean of recovered " << names[parameter] << " for " << value << ": " << fromROut[index] << endl;
	}
	Plotting::plotBoxplot(directboxplot, min, max, names[parameter] + string("-direct-boxplot"));
	Plotting::plotBoxplot(fromRboxplot, min, max, names[parameter] + string("-R-boxplot"));
}

void TestChamber::biasmap(double min, double max, double precision, Settings settings) {
	int biassteps = (max - min) / precision;
	assert(biassteps > 0);
	vector<double> biasdirect(biassteps);
	vector<double> biasR(biassteps);
	vector<double> bsamples(biassteps);
	vector<double> biasdirectOut(biassteps);
	vector<double> biasROut(biassteps);
	vector<vector<double> > biasdirectboxplot(biassteps);
	vector<vector<double> > biasRboxplot(biassteps);

	#pragma omp parallel for schedule(dynamic, 1)
	for (int bindex = 0; bindex < biassteps; bindex++) {
		double bias = bindex*precision + min;
		#pragma omp parallel for schedule(dynamic, 1)
		for (int j = 0; j < settings.repeat; j++) {
			Model model = Model::generateModel(settings);
			model.alpha = sampleGamma(settings.alpha_shape,settings.alpha_scale);
			model.gamma = sampleGamma(settings.alpha_shape,settings.alpha_scale);
			model.s = bias;
			Graph R = model.sampleGraphInstance(0);

			double mean = 0;

			Sampler sampler(R, model, settings);
			for (int i = 0; i < settings.iterations; i++) {
				sampler.sampleHyperBias();
				mean += sampler.getBias();
			}
			mean = mean / settings.iterations;

			Model newmodel = Model::generateModel(settings.nodeCount, settings.mu_weights, settings.sigma_weights, settings.mu_bias, settings.sigma_bias);
			sampler = TestChamber::testrun(newmodel, R, settings);

			double meanR = 0;

			for (int i = 0; i < settings.iterations; i++) {
				meanR += sampler.chain[i].s;
			}
			meanR = meanR / settings.iterations;

			#pragma omp critical
			{
				bsamples[bindex]++;
				biasdirectboxplot[bindex].push_back(mean);
				biasRboxplot[bindex].push_back(meanR);
				biasdirect[bindex] += mean;
				biasR[bindex] += meanR;
				biasdirectOut[bindex] = biasdirect[bindex] / bsamples[bindex];
				biasROut[bindex] = biasR[bindex] / bsamples[bindex];
				Plotting::plotHistogramm(biasdirectOut, min, max, "Bias-direct");
				Plotting::plotHistogramm(biasROut, min, max, "Bias-R");
			}
		}
		cout << "Mean of recovered bias for " << bias << ": " << biasROut[bindex] << endl;
	}
	Plotting::plotBoxplot(biasdirectboxplot, min, max, "Bias-direct-boxplot");
	Plotting::plotBoxplot(biasRboxplot, min, max, "Bias-R-boxplot");
}

void TestChamber::parametermap(double alphamin, double alphamax,
		double gammamin, double gammamax, double precision, Settings settings) {
	int alphasteps = (alphamax - alphamin) / precision;
	assert(alphasteps > 0);
	int gammasteps = (gammamax - gammamin) / precision;
	assert(gammasteps > 0);
	vector<vector<double> > NMIAvg(alphasteps);
	vector<double> alphadirect(alphasteps);
	vector<double> alphaR(alphasteps);
	vector<double> asamples(alphasteps);
	vector<double> gammadirect(gammasteps);
	vector<double> gammaR(gammasteps);
	vector<double> gsamples(gammasteps);
	vector<double> alphaRout(alphasteps);
	vector<double> alphadirectout(alphasteps);
	vector<double> gammaRout(gammasteps);
	vector<double> gammadirectout(gammasteps);
	vector<vector<double> > alphadirectboxplot(alphasteps);
	vector<vector<double> > alphaRboxplot(alphasteps);
	vector<vector<double> > gammadirectboxplot(gammasteps);
	vector<vector<double> > gammaRboxplot(gammasteps);

	//bool parallelizeInner = alphasteps < settings.num_threads;

	for (int i = 0; i < alphasteps; i++) {
		NMIAvg[i].resize(gammasteps, 0);
	}

	#pragma omp parallel for schedule(dynamic, 1)
	for (int aindex = 0; aindex < alphasteps; aindex++) {
		double alpha = aindex*precision+alphamin;

		#pragma omp parallel for schedule(dynamic, 1)
		for (int gindex = 0; gindex < gammasteps; gindex++) {
			double gamma = gindex*precision+gammamin;
			vector<double> localDirectAlphas;
			vector<double> localDirectGammas;
			vector<double> localRAlphas;
			vector<double> localRGammas;
			for (int i = 0; i < settings.repeat; i++) {
				//generate model
				Model model = Model::generateModel(settings);
				model.alpha = alpha;
				model.gamma = gamma;
				Graph R = model.sampleGraphInstance(0);
				ClusterMatrix origclusters = model.getClusterMatrix();

				int samples = 0;
				double cummeana = 0;
				double cummeang = 0;
				Sampler sampler(R, model, settings);
				//sample alpha and gamma direct
				for (int i = 0; i < settings.iterations; i++) {
					sampler.sampleHyperAlpha();
					sampler.sampleHyperGamma(0.1);
					cummeana = (cummeana * samples + sampler.getAlpha()) / (samples +1);
					cummeang = (cummeang * samples + sampler.getGamma()) / (samples +1);
					samples++;
				}

				localDirectAlphas.push_back(cummeana);
				localDirectGammas.push_back(cummeang);

				sampler = Sampler(R, settings);

				//sample alpha and gamma from R
				Model newmodel = Model::generateModel(settings.nodeCount, settings.mu_weights, settings.sigma_weights, settings.mu_bias, settings.sigma_bias);
				sampler = TestChamber::testrun(newmodel, R, settings);

				Aggregation agg = Aggregation::aggregate(sampler.getChain());

				localRAlphas.push_back(agg.alpha);
				localRGammas.push_back(agg.gamma);

				Model result = agg.collapse(settings);
				ClusterMatrix clusters = result.getClusterMatrix();

				double nmi = Util::averagePurityAndNMIMax(clusters, origclusters)[1];
				#pragma omp critical
				{
					asamples[aindex]++;
					alphadirect[aindex] += cummeana;
					alphaR[aindex] += agg.alpha;
					alphadirectout[aindex] = alphadirect[aindex] / asamples[aindex];
					alphaRout[aindex] = alphaR[aindex] / asamples[aindex];

					gsamples[gindex]++;
					gammadirect[gindex] += cummeang;
					gammaR[gindex] += agg.gamma;
					gammadirectout[gindex] = gammadirect[gindex] / gsamples[gindex];
					gammaRout[gindex] = gammaR[gindex] / gsamples[gindex];

					NMIAvg[aindex][gindex] += nmi/settings.repeat;
				}
			}
			#pragma omp critical
			{
			alphadirectboxplot[aindex].insert(alphadirectboxplot[aindex].end(), localDirectAlphas.begin(), localDirectAlphas.end());
			alphaRboxplot[aindex].insert(alphaRboxplot[aindex].end(), localRAlphas.begin(), localRAlphas.end());
			gammadirectboxplot[gindex].insert(gammadirectboxplot[gindex].end(), localDirectGammas.begin(), localDirectGammas.end());
			gammaRboxplot[gindex].insert(gammaRboxplot[gindex].end(), localRGammas.begin(), localRGammas.end());

			Plotting::plotHistogramm(asamples, alphamin, alphamax, "Asamples");
			Plotting::plotHistogramm(gsamples, gammamin, gammamax, "Gsamples");
			Plotting::plotHistogramm(alphadirectout, alphamin, alphamax, "Alphadirect");
			Plotting::plotHistogramm(alphaRout, alphamin, alphamax, "Alpha after Sampling");
			Plotting::plotHistogramm(gammadirectout, gammamin, gammamax, "Gammadirect");
			Plotting::plotHistogramm(gammaRout, gammamin, gammamax, "Gamma after Sampling");
			Plotting::plotMatrix(NMIAvg, "NMI Average on alpha, gamma");
			}
		}
	}
	Plotting::plotBoxplot(alphadirectboxplot, alphamin, alphamax, "Alpha-direct-box");
	Plotting::plotBoxplot(alphaRboxplot, alphamin, alphamax, "Alpha-R-box");
	Plotting::plotBoxplot(gammadirectboxplot, gammamin, gammamax, "Gamma-direct-box");
	Plotting::plotBoxplot(gammaRboxplot, gammamin, gammamax, "Gamma-R-box");
}
} /* namespace CILA */
