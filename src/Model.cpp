/*
 * Model.cpp
 *
 *  Created on: 30.11.2012
 *      Author: moritz
 */

#include "Model.h"

#include <cmath>
#include <cstdlib>
#include <assert.h>
#include <iostream>
#include <limits>
#include <algorithm>

#include "generative.h"
#include "Util.h"

using std::cout;
using std::cerr;
using std::endl;
using std::numeric_limits;

namespace CILA {
namespace core {

Model::Model(FeatureMatrix f, ClusterMatrix c, WeightMatrices w,
		double alpha, double gamma, double s, double sigma) {
	assert(f.size() == c.size());
	n = f.size();

	for (NodeID i = 0; i < f.size(); i++) {
		assert(f[i].size() == f[0].size());
		assert(c[i].size() == f[i].size());
		for (FeatureID j = 0; j < f[i].size(); j++) {
			assert(f[i][j] == (c[i][j] != -1));
		}
	}

	if (n > 0) {
		assert(w.size() == f[0].size());
	}


	featurePresent = f;
	clusterAssignment = c;
	weights = w;

	assert(alpha > 0);
	assert(gamma >= 0);
	this->alpha = alpha;
	this->gamma = gamma;

	this->sigma = sigma;
	this->s = s;
	constructed = true;
}

double Model::sigmoid(double weight) {//might also be called logistic function
	double result = pow(1+exp(-weight),-1);
	assert(result >= 0);
	assert(result <= 1);
	return result;
}

double Model::getLinkMean(NodeID a, NodeID b) {
	return getLinkMean(a, b, &featurePresent, &clusterAssignment, &weights, s);
}

void Model::permute(vector<NodeID> perm) {
	//permute F and C. W does not need to be permuted
	assert(perm.size() == featurePresent.size());
	NodeID nodeCount = featurePresent.size();
	FeatureMatrix featurecopy(nodeCount);
	ClusterMatrix clustercopy(nodeCount);
	for (NodeID i = 0; i < perm.size(); i++) {
		featurecopy[i] = featurePresent[perm[i]];
		clustercopy[i] = clusterAssignment[perm[i]];
	}
	featurePresent = featurecopy;
	clusterAssignment = clustercopy;
}

void Model::scale(double factor) {
	for (vector<vector<double> > matrix : weights) {
		for (vector<double> row : matrix) {
			for (double value : row) {
				value *= factor;
			}
		}
	}
	s *= factor;
	sigma *= factor;
}

vector<vector<double> > Model::computeT(FeatureMatrix* f, ClusterMatrix* c,
		WeightMatrices* w) {
	NodeID nodeCount = f->size();
	assert(nodeCount == c->size());
	assert(nodeCount > 0);
	assert((*f)[0].size() == (*c)[0].size());
	assert((*c)[0].size() == w->size());
	vector<vector<double> > result(nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) result[i].resize(nodeCount);

	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
			double mean = getLinkMean(i, j, f, c, w, 0);
			result[i][j] = mean;
			result[j][i] = mean;
		}
	}
	return result;
}

vector<vector<double> > Model::computeT(FeatureMatrix* f, vector<bitPool> * compressedZ, ClusterMatrix* c,
		WeightMatrices* w) {
	NodeID nodeCount = f->size();
	assert(nodeCount == c->size());
	assert(nodeCount > 0);
	assert((*f)[0].size() == (*c)[0].size());
	assert((*c)[0].size() == w->size());
	vector<vector<double> > result(nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) result[i].resize(nodeCount);

	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
			double mean;
			if (w->size() < 8*sizeof(bitPool)) mean = getLinkMean(i,j,compressedZ, c, w, 0);
			else mean = getLinkMean(i, j, f, c, w, 0);
			result[i][j] = mean;
			result[j][i] = mean;
		}
	}
	return result;
}

vector<vector<double> > Model::computeTrestricted(FeatureMatrix* f, ClusterMatrix* c,
		WeightMatrices* w, FeatureID l) {
	NodeID nodeCount = f->size();
	assert(nodeCount == c->size());
	vector<vector<double> > result(nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) {
		result[i].resize(nodeCount);
	}

	for (NodeID i = 0; i < nodeCount; i++) {
		if (!(*f)[i][l]) continue;
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
			if (!(*f)[j][l]) continue;
			double weight = (*w)[l][(*c)[i][l]][(*c)[j][l]];
			result[i][j] = weight;
			result[j][i] = weight;
		}
	}
	return result;
}

double Model::computeDistance(Model * a, FeatureID k, Model * b, FeatureID l) {
	/**
	 * distance is \sum ((T_a - T_b).^2)/n^2 + \sum (similarity_a - similarity_b)/n^2
	 */
	NodeID nodeCount = a->n;
	assert(b->n == nodeCount);
	Matrix T_a = computeTrestricted(&(a->featurePresent), &(a->clusterAssignment), &(a->weights), k);
	Matrix T_b = computeTrestricted(&(b->featurePresent), &(b->clusterAssignment), &(b->weights), l);

	double clusterDistance = Model::clusterDifferenceRestricted(&(a->clusterAssignment), k, &(b->clusterAssignment), l) / (nodeCount*nodeCount);
	return Util::normalizedSumOfsquaredDifference(&T_a, &T_b) + clusterDistance;
}

double Model::computeLength(Model * a, FeatureID l) {
	/**
	 * computes distance to empty feature
	 */
	Matrix T_a = computeTrestricted(&(a->featurePresent), &(a->clusterAssignment), &(a->weights), l);
	NodeID nodeCount = T_a.size();
	Matrix T_dummy(nodeCount);
	ClusterMatrix dummyClusters(nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) {
		T_dummy[i].resize(nodeCount, 0);
		dummyClusters[i].resize(1,-1);
	}

	double clusterLength = Model::clusterDifferenceRestricted(&(a->clusterAssignment), l, &dummyClusters, 0) / (nodeCount*nodeCount);
	return Util::normalizedSumOfsquaredDifference(&T_a, &T_dummy) + clusterLength;
}

double Model::computeDistance(Model * a, Model * b) {
	/**
	 * First, compute Distance matrix. *Expletive*ly expensive.
	 * Afterwards, assign each feature of a to closest feature of b \cup empty
	 */

	//w.l.o.g., a->featureCount >= b->featureCount
	if (a->getFeatureCount() < b->getFeatureCount()) {
		Model * temp = a;
		a = b;
		b = temp;
	}

	FeatureID fCa = a->getFeatureCount();
	FeatureID fCb = b->getFeatureCount();

	Matrix distances(fCa+1);
	for (FeatureID i = 0; i < distances.size(); i++) {
		distances[i].resize(fCb+1);
		for (FeatureID j = 0; j < fCb; j++) {
			if (i < fCa) distances[i][j] = computeDistance(a,i,b,j);
			else distances[i][j] = computeLength(b,j);
		}
		if (i < fCa) distances[i][fCb] = computeLength(a,i);
		else distances[i][fCb] = 0;
	}

	double result = 0;

	for (FeatureID i = 0; i < fCa; i++) {
		if (i < fCb) {
			result += distances[i][i];
		}
		else {
			double locmin = distances[i][0];
			for (FeatureID j = 0; j < distances[i].size(); j++) {
				if (distances[i][j] < locmin) locmin = distances[i][j];
			}
			result += locmin;
		}
	}

	return result;
}

bool Model::comp(ClusterMatrix &cl, FeatureID k, FeatureID l) {
	/**
	 * returns 1 if k should be left of l, 0 if they are equivalent, -1 if l should be left ok k
	 */
	NodeID n = cl.size();
	assert(n > 0);
	FeatureID f = cl[0].size();
	assert(k < f);
	assert(l < f);
	assert(k >= 0);
	assert(l >= 0);
	if (k == l) return 0;
	NodeID firstl = n+1;
	NodeID firstk = n+1;
	int locc = 0;
	int kocc = 0;
	for (NodeID i = 0; i < n; i++) {
		if (cl[i][k] >= 0) {
			if (firstk > i) firstk = i;
			kocc++;
		}
		if (cl[i][l]) {
			if (firstl > i) firstl = i;
			locc++;
		}
	}
	/*
	 * feature k should be left of feature l iff
	 * 	the first occurrence of l is at a lower node index than the first occurrence of k
	 * 	OR both occur at the same node for the first time, but k has more participants than l
	 */
	return (firstk < firstl || ((firstl == firstk) && kocc > locc));
}

vector<vector<vector<double> > > Model::computePurityandNMIMatrices(ClusterMatrix externalLabels) {
	assert(externalLabels.size() == this->n);
	FeatureID featureCount = weights.size();
	FeatureID extFeatureCount = externalLabels[0].size();

	vector<vector<double> > purity(featureCount);
	vector<vector<double> > nmi(featureCount);
	vector<vector<double> > redundancy(featureCount);
	vector<vector<double> > pValuePurity(featureCount);
	for (FeatureID i = 0; i < featureCount; i++) {
		purity[i].resize(extFeatureCount);
		nmi[i].resize(extFeatureCount);
		redundancy[i].resize(extFeatureCount);
		pValuePurity[i].resize(extFeatureCount);
	}

	for (FeatureID m = 0; m < featureCount; m++) {
		int ownOffset = Util::containsNegative(clusterAssignment, m) ? 1 : 0;
		vector<int> ownSlice = Util::matslice(&clusterAssignment, m, ownOffset);

		for (FeatureID l = 0; l < extFeatureCount; l++) {
			vector<int> extSlice(n);

			bool increaseExternal = Util::containsNegative(externalLabels, l);
			int extOffset = increaseExternal ? 1 : 0;
			extSlice = Util::matslice(&externalLabels, l, extOffset);

			for (NodeID i = 0; i < n; i++) {
				assert(extSlice[i] >= 0);
				assert(extSlice[i] <= 1000); //well, that would be just silly
			}

			vector<double> interimResults = Util::balancedPurityandNMI(ownSlice, extSlice);
			purity[m][l] = interimResults[0];
			nmi[m][l] = interimResults[1];
			redundancy[m][l] = interimResults[2];
		}
	}
	vector<vector<vector<double> > > result(3);
	result[0] = purity;
	result[1] = nmi;
	result[2] = redundancy;
	return result;
}

vector<vector<double> > Model::pValuePurityMatrix(ClusterMatrix externalLabels, int iter) {
	FeatureID featureCount = weights.size();
	assert(externalLabels.size() > 0);
	FeatureID extFeatureCount = externalLabels[0].size();
	vector<vector<double> > result(featureCount);

	for (FeatureID m = 0; m < featureCount; m++) {
		result[m].resize(extFeatureCount);
		int ownOffset = Util::containsNegative(clusterAssignment, m) ? 1 : 0;
		vector<int> ownSlice = Util::matslice(&clusterAssignment, m, ownOffset);

		for (FeatureID l = 0; l < extFeatureCount; l++) {
			vector<int> extSlice(n);

			bool increaseExternal = Util::containsNegative(externalLabels, l);
			int extOffset = increaseExternal ? 1 : 0;
			extSlice = Util::matslice(&externalLabels, l, extOffset);

			result[m][l] = Util::pValuesPurityAndNMI(ownSlice, extSlice, iter)[0];
		}
	}
	return result;
}

vector<vector<double> > Model::pValueNMIMatrix(ClusterMatrix externalLabels, int iter) {
	FeatureID featureCount = weights.size();
	assert(externalLabels.size() > 0);
	FeatureID extFeatureCount = externalLabels[0].size();
	vector<vector<double> > result(featureCount);

	for (FeatureID m = 0; m < featureCount; m++) {
		result[m].resize(extFeatureCount);
		int ownOffset = Util::containsNegative(clusterAssignment, m) ? 1 : 0;
		vector<int> ownSlice = Util::matslice(&clusterAssignment, m, ownOffset);

		for (FeatureID l = 0; l < extFeatureCount; l++) {
			vector<int> extSlice(n);

			bool increaseExternal = Util::containsNegative(externalLabels, l);
			int extOffset = increaseExternal ? 1 : 0;
			extSlice = Util::matslice(&externalLabels, l, extOffset);

			result[m][l] = Util::pValuesPurityAndNMI(ownSlice, extSlice, iter)[1];
		}
	}
	return result;
}

vector<vector<double> > Model::computeAnovaMatrix(vector<vector<double> > external) {
	assert(external.size() == n);
	assert(n > 0);
	FeatureID featureCount = weights.size();
	FeatureID extFeatureCount = external[0].size();

	vector<vector<double> > result(featureCount);
	for (FeatureID m = 0; m < featureCount; m++) {
		result[m].resize(extFeatureCount);
		vector<int> ownSlice(n);
		for (NodeID i = 0; i < n; i++) {
			ownSlice[i] = clusterAssignment[i][m];
		}
		for (FeatureID l = 0; l < extFeatureCount; l++) {
			vector<double> extSlice(n);
			for (NodeID i = 0; i < n; i++) {
				extSlice[i] = external[i][l];
			}
			result[m][l] = Util::anova(ownSlice, extSlice);
		}
	}
	return result;
}

vector<vector<double> > Model::pValueAnovaMatrix(
		vector<vector<double> > external, int iter) {
	FeatureID featureCount = weights.size();
	assert(external.size() > 0);
	FeatureID extFeatureCount = external[0].size();
	vector<vector<double> > result(featureCount);

	for (FeatureID m = 0; m < featureCount; m++) {
		result[m].resize(extFeatureCount);
		int ownOffset = Util::containsNegative(clusterAssignment, m) ? 1 : 0;
		vector<int> ownSlice = Util::matslice(&clusterAssignment, m, ownOffset);

		for (FeatureID l = 0; l < extFeatureCount; l++) {
			vector<double> extSlice(n);

			extSlice = Util::matslice(&external, l, 0);

			result[m][l] = Util::pValueAnova(ownSlice, extSlice, iter);
		}
	}
	return result;
}

double Model::logPrior() {
	double result = 0;

	const NodeID n = featurePresent.size();
	const FeatureID f = weights.size();
	vector<vector<int>> clusterPopulation(f);
	vector<int> featurePopulation(f);

	for (FeatureID m = 0; m < f; m++) {
			featurePopulation[m] = 0;

			for (ClusterID c = 0; c < clusterPopulation[m].size(); c++) {
				clusterPopulation[m][c] = 0;
			}
		}

		for (NodeID i = 0; i < n; i++) {
			for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
				if (featurePresent[i][m]) {
					featurePopulation[m]++;
					assert(clusterAssignment[i][m] >= 0);
					unsigned oldsize = clusterPopulation[m].size();
					if (oldsize <= (unsigned) clusterAssignment[i][m]) {
						clusterPopulation[m].resize(clusterAssignment[i][m] + 1,	0);
						assert(clusterPopulation[m].size() > oldsize);
					}
					clusterPopulation[m][clusterAssignment[i][m]]++;
				}
			}
		}

		//assert every feature has at least one cluster
		for (FeatureID m = 0; m < clusterPopulation.size(); m++) {
			assert(clusterPopulation[m].size() > 0);
		}

	for (FeatureID i = 0; i < f; i++) {
		int kPlus = clusterPopulation[i].size();
		int logPopulationProd = 0;
		for (int j = 0; j < kPlus; j++) {
			logPopulationProd += log(clusterPopulation[i][j]);
		}
		double logPriorCRP = alpha*kPlus + logPopulationProd + lgamma(alpha) - lgamma(n+alpha);
		result += logPriorCRP;
	}

	return result;
}

double Model::logLikelihoodNewCluster(Graph* g, vector<bitPool> * compressedFeatures,
		ClusterMatrix* c, WeightMatrices* w, double s, double sigma, NodeID i,
		FeatureID m, ClusterID newCluster, double oldll) {
		assert(oldll == oldll);
		assert(sigma > 0);
		assert(newCluster < (*w)[m].size());
		assert(newCluster >= 0);
		NodeID nodeCount = g->size();
		double oldRowll = 0;
		int oldCluster = (*c)[i][m];
		for (NodeID j = 0; j < nodeCount; j++) {
			/*I could skip nodes not participating in feature m,
			 *  the connections to them shouldn't change.
			 *  However, speedup is about 1%. Not worth it.
			 */
			//if (!(*f)[j][m]) continue;
			double diff = getLinkMean(i, j, compressedFeatures, c, w, s) - g->hasEdge(i,j);
			double value = -(diff)*diff;
			oldRowll += value;
			if (i != j) oldRowll += value;
			if (oldRowll == -numeric_limits<double>::infinity()) {
				cerr << "At [" << i <<"]["<< j << "], oldRowll is " << oldRowll << ", after adding " << value << ", which is derived from " << diff << endl;
			}
		}
		oldRowll = oldRowll / (2*sigma*sigma);
		double basell = oldll - oldRowll;
		assert(basell == basell);
		(*c)[i][m] = newCluster;
		double newRowll = 0;
		for (NodeID j = 0; j < nodeCount; j++) {
			//if (!(*f)[j][m]) continue;
			double diff = getLinkMean(i, j, compressedFeatures, c, w, s) - g->hasEdge(i,j);
			double value = -diff*diff;
			newRowll += value;
			if (i != j) newRowll += value;
			if (newRowll == -numeric_limits<double>::infinity()) {
				cerr << "At [" << i <<"]["<< j << "], newRowll is " << newRowll << ", after adding " << value << ", which is derived from " << diff << endl;
			}
		}
		newRowll = newRowll / (2*sigma*sigma);
		assert(newRowll == newRowll);
		(*c)[i][m] = oldCluster;
		double result = basell + newRowll;
		if (!(result == result)) {
			cout << "NaN at node " << i << ", feature " << m << ". " << endl;
			cout << "oldll: " << oldll << ", oldRowll: " << oldRowll << ", basell: " << basell << ", newRowll: " << newRowll << ", s: " << s << ", sigma: " << sigma << endl;
			cout << "oldCluster: " << oldCluster << ", newCluster: " << newCluster << ", reflexive weight of newCluster: " << (*w)[m][newCluster][newCluster] << endl;
		}
		assert(result == result);
		return result;
}

double Model::logLikelihoodNewCluster(Graph* g, FeatureMatrix* f,
		ClusterMatrix* c, WeightMatrices* w, double s, double sigma, NodeID i,
		FeatureID m, ClusterID newCluster, double oldll) {
		assert(oldll == oldll);
		assert(sigma > 0);
		assert(newCluster < (*w)[m].size());
		assert(newCluster >= 0);
		NodeID nodeCount = g->size();
		double oldRowll = 0;
		int oldCluster = (*c)[i][m];
		for (NodeID j = 0; j < nodeCount; j++) {
			/*I could skip nodes not participating in feature m,
			 *  the connections to them shouldn't change.
			 *  However, speedup is about 1%. Not worth it.
			 */
			//if (!(*f)[j][m]) continue;
			double diff = getLinkMean(i, j, f, c, w, s) - g->hasEdge(i,j);
			double value = -(diff)*diff;
			oldRowll += value;
			if (i != j) oldRowll += value;
			if (oldRowll == -numeric_limits<double>::infinity()) {
				cerr << "At [" << i <<"]["<< j << "], oldRowll is " << oldRowll << ", after adding " << value << ", which is derived from " << diff << endl;
			}
		}
		oldRowll = oldRowll / (2*sigma*sigma);
		double basell = oldll - oldRowll;
		assert(basell == basell);
		(*c)[i][m] = newCluster;
		double newRowll = 0;
		for (NodeID j = 0; j < nodeCount; j++) {
			//if (!(*f)[j][m]) continue;
			double diff = getLinkMean(i, j, f, c, w, s) - g->hasEdge(i,j);
			double value = -diff*diff;
			newRowll += value;
			if (i != j) newRowll += value;
			if (newRowll == -numeric_limits<double>::infinity()) {
				cerr << "At [" << i <<"]["<< j << "], newRowll is " << newRowll << ", after adding " << value << ", which is derived from " << diff << endl;
			}
		}
		newRowll = newRowll / (2*sigma*sigma);
		assert(newRowll == newRowll);
		(*c)[i][m] = oldCluster;
		double result = basell + newRowll;
		if (!(result == result)) {
			cout << "NaN at node " << i << ", feature " << m << ". " << endl;
			cout << "oldll: " << oldll << ", oldRowll: " << oldRowll << ", basell: " << basell << ", newRowll: " << newRowll << ", s: " << s << ", sigma: " << sigma << endl;
			cout << "oldCluster: " << oldCluster << ", newCluster: " << newCluster << ", reflexive weight of newCluster: " << (*w)[m][newCluster][newCluster] << endl;
		}
		assert(result == result);
		return result;
}

double Model::logLikelihoodNewWeights(Graph* g, FeatureMatrix* f,
		ClusterMatrix* c, WeightMatrices* w, double s, double sigma,
		FeatureID m, ClusterID c1, ClusterID c2, vector<NodeID> * c1pop, vector<NodeID> * c2pop, double wnew, double oldll) {
	/*
	 * NOT threadsafe, since the data lying behind w is critical
	 */

	assert(m < w->size());
	assert(c1 >= 0 && c1 < (*w)[m].size());
	assert(c2 >= 0 && c2 < (*w)[m].size());
	assert(sigma > 0);

	double previousWeight = (*w)[m][c1][c2];
	assert(previousWeight == (*w)[m][c2][c1]);

	double newpartll = 0;
	(*w)[m][c1][c2] = wnew;
	(*w)[m][c2][c1] = wnew;

	for (unsigned i = 0; i < c1pop->size(); i++) {
		for (unsigned j = 0; j < c2pop->size(); j++) {
			double diff = getLinkMean(c1pop->at(i), c2pop->at(j), f, c, w, s) - g->hasEdge(c1pop->at(i),c2pop->at(j));
			double loglinkDensity =  - (diff*diff);
			newpartll += loglinkDensity;
			if (c1 != c2) newpartll += loglinkDensity;//we assume a symmetric matrix and iterate only over one half. The missing duplicate is added here
		}
	}
	newpartll = newpartll / (2*sigma*sigma);
	(*w)[m][c1][c2] = previousWeight;
	(*w)[m][c2][c1] = previousWeight;
	assert(newpartll == newpartll);
	return newpartll;
}

int Model::countClusters(ClusterMatrix * c, FeatureID m) {
	int result = 0;
	NodeID nodeCount = c->size();
	assert(nodeCount > 0); //otherwise, no features would exist.
	assert(m < (*c)[0].size());

	for (NodeID i = 0; i < nodeCount; i++) {
		if ((*c)[i][m] >= result) result = (*c)[i][m]+1;
	}

	return result;
}

void Model::mergeFeatures(FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, FeatureID l, FeatureID k) {
	ClusterID lClusterCount = countClusters(c,l);
	ClusterID kClusterCount = countClusters(c,k);
	NodeID nodeCount = c->size();
	assert(nodeCount > 0);
	FeatureID featureCount = (*c)[0].size();
	assert(l < featureCount);
	assert(k < featureCount);
	vector<int> slice(nodeCount,-1);
	vector<vector<int> > projections(lClusterCount+1);
	vector<int> reverseL;
	vector<int> reverseK;

	for (ClusterID i = 0; i < projections.size(); i++) {
		projections[i].resize(kClusterCount+1, -1);
	}

	ClusterID ncc = 0;
	//assign new clusters
	for (NodeID i = 0; i < nodeCount; i++) {
		if ((*c)[i][l] > -1 || (*c)[i][k] > -1) {
			if (projections[(*c)[i][l]+1][(*c)[i][k]+1] == -1) {
				//first time we hit this cluster combo
				assert(reverseL.size() == ncc);
				assert(reverseK.size() == ncc);
				int target = ncc;
				slice[i] = target;
				projections[(*c)[i][l]+1][(*c)[i][k]+1] = target;
				reverseL.push_back((*c)[i][l]);
				reverseK.push_back((*c)[i][k]);
				ncc++;
			} else {
				slice[i] = projections[(*c)[i][l]+1][(*c)[i][k]+1];
			}
		}
	}
	assert(ncc <= (kClusterCount+1)*(lClusterCount+1)-1);
	assert(ncc >= kClusterCount || ncc >= lClusterCount);

	//assign weights
	vector<vector<double> > newWeights(ncc);
	for (ClusterID i = 0; i < ncc; i++) {
		newWeights[i].resize(ncc);
	}

	for (NodeID i = 0; i < nodeCount; i++) {
		if (slice[i] == -1) {
			assert((*c)[i][l] == -1 && (*c)[i][k] == -1);
			continue;
		}
		for (NodeID j = 0; j < nodeCount; j++) {
			if (slice[j] == -1) {
				assert((*c)[j][l] == -1 && (*c)[j][k] == -1);
				continue;
			}
			double oldweight = 0;
			if ((*c)[i][l] > -1 && (*c)[j][l] > -1) oldweight += (*w)[l][(*c)[i][l]][(*c)[j][l]];
			if ((*c)[i][k] > -1 && (*c)[j][k] > -1) oldweight += (*w)[k][(*c)[i][k]][(*c)[j][k]];
			newWeights[slice[i]][slice[j]] = oldweight;
		}
	}

	//now swap columns
	for (NodeID i = 0; i < nodeCount; i++) {
		(*c)[i][l] = slice[i];
		(*f)[i][l] = (bool) (slice[i] != -1);
		if (l != k) {
			(*c)[i][k] = -1; //better not use erase, can never be too cautios with pointers to vectors
			(*f)[i][k] = false;
		}
	}

	(*w)[l].swap(newWeights);
	if (l == k) assert(ncc == lClusterCount);
}

void Model::dropFeature(ClusterMatrix * c, WeightMatrices * w, FeatureID l) {
	//count Clusters in c
	NodeID nodeCount = c->size();
	assert(nodeCount > 0);
	FeatureID featureCount = c->at(0).size();
	assert(w->size() == featureCount);
	assert(l < featureCount);
	/**
	 * CAUTION: calling erase on a vector should not cause reallocation of the whole vector, but might! Don't use yet!
	 */
	assert(false);
	for (NodeID i = 0; i < nodeCount; i++) {
		(*c)[i].erase((*c)[i].begin()+l);
		assert((*c)[i].size() == featureCount-1);
	}

	w->erase(w->begin()+l);
	assert(c->size() == nodeCount);
	assert(w->size() == featureCount-1);
}

bool Model::isDependent(ClusterMatrix * c, FeatureID l, FeatureID k) {
	//count Clusters in c
	NodeID nodeCount = c->size();
	assert(nodeCount > 0);
	FeatureID featureCount = c->at(0).size();
	assert(l < featureCount);
	assert(k < featureCount);
	int clusterCount = 0;
	bool kEmpty = true;
	for (NodeID i = 0; i < nodeCount; i++) {
		if ((*c)[i][l] >= clusterCount) clusterCount = (*c)[i][l]+1;
		if ((*c)[i][k] > -1) kEmpty = false;
	}

	assert(clusterCount <= nodeCount+1);
	if (kEmpty) return true; //trivially dependent
	bool result = true;
	vector<int> projections(clusterCount+1,-2);//+1 because I need to make sure nodes not participating in l also don't participate k.
	for (NodeID i = 0; i < nodeCount; i++) {
		if (projections[(*c)[i][l]+1] == -2) {
			//fill projection map
			projections[(*c)[i][l]+1] = (*c)[i][k];
		} else {
			//protest if mismatches happen
			if (projections[(*c)[i][l]+1] != (*c)[i][k]) result = false;
		}
	}
	return result;
}

ClusterID Model::clusterCombinations(ClusterMatrix * c, FeatureID l, FeatureID k) {
	int lClusters = Model::countClusters(c, l);//inefficient, could save one iteration by counting both in the same loop
	int kClusters = Model::countClusters(c, k);

	NodeID nodeCount = c->size();

	//create occupation array
	vector<vector<bool> > occupied(lClusters+1);
	for (ClusterID p = 0; p < occupied.size(); p++) {
		occupied[p].resize(kClusters+1, false);
	}

	for (NodeID i = 0; i < nodeCount; i++) {
		occupied[(*c)[i][l]+1][(*c)[i][k]+1] = true;
	}

	int resultClusters = 0;
	for (ClusterID p = 0; p < occupied.size(); p++) {
		for (ClusterID q = 0; q < occupied[p].size(); q++) {
			if ((p > 0 || q > 0) && occupied[p][q]) resultClusters++;
		}
	}
	assert(resultClusters >= lClusters);
	assert(resultClusters >= kClusters);
	assert(resultClusters < (lClusters+1)*(kClusters+1));
	return resultClusters;
}

Model Model::mapToRepresentative(Model input) {
	//throw std::runtime_error("Don't use this function, it is buggy.");
	//for now, minimize total cluster count.
	NodeID nodeCount = input.n;
	if (nodeCount == 0) return input;
	FeatureID featureCount = input.weights.size();
	for (FeatureID k = 0; k < featureCount; k++) {
		for (FeatureID l = 0; l < featureCount; l++) {
			ClusterID kClusterCount = countClusters(&input.clusterAssignment, k);
			ClusterID lClusterCount = countClusters(&input.clusterAssignment, l);
			ClusterID resultClusterCount = clusterCombinations(&input.clusterAssignment, k, l);
			if (pow((double) resultClusterCount,2) < pow((double) kClusterCount,2) + pow((double) lClusterCount,2)) {
				if (k != l) {
					//cout << "Merging features " << l << " and " << k << ", because " << kClusterCount << "^2 + ";
					//cout << lClusterCount << "^2 > " << resultClusterCount << "^2" << endl;
					mergeFeatures(&input.featurePresent, &input.clusterAssignment, &input.weights, k, l);
				}
			} else {
				//cout << "Not merging features " << l << " and " << k << ", because " << kClusterCount << " + ";
				//cout << lClusterCount << " <= " << resultClusterCount << endl;
			}
		}
	}

	//left-order features
	if (Settings::getDefaultSettings().leftOrder)  {
		input.leftOrder();
	}

	return input;
}

void Model::leftOrder() {
	FeatureID featureCount = weights.size();
	vector<FeatureID> perm = Util::dummyPermutation(featureCount);
	std::sort(perm.begin(), perm.end(), [this](int i, int j){return comp(this->clusterAssignment, i,j);});
	for (NodeID i = 0; i < n; i++) {
		featurePresent[i] = Util::apply(perm, featurePresent[i]);
		clusterAssignment[i] = Util::apply(perm, clusterAssignment[i]);
	}
	weights = Util::apply(perm, weights);
}

void Model::purgeUnusedFeatures() {
	/**
	* Remove all features without inhabitants. This method is safe to call any time.
	*/
	FeatureID featureCount = weights.size();
	if (featureCount == 0) return; //nothing to remove, already barren and empty

	vector<bool> keep(featureCount, false);
	int lastOccupied = -1;
	int firstEmpty = -1;

	for (FeatureID m = 0; m < featureCount; m++) {
		for (NodeID i = 0; i < n; i++) {
			if (featurePresent[i][m]) {
				keep[m] = true;
				lastOccupied = m;
			}
		}
		if (!keep[m] && firstEmpty == -1) firstEmpty = m;
	}

	if (lastOccupied < firstEmpty) {
		assert(lastOccupied +1 == firstEmpty);
		//just resize everything, no copying necessary
		weights.resize(lastOccupied+1);
		for (NodeID i = 0; i < n; i++) {
			featurePresent[i].resize(lastOccupied+1);
			clusterAssignment[i].resize(lastOccupied+1);
		}
	} else {
		//alas, no such luck. Copy!
		FeatureMatrix featurecopy;
		ClusterMatrix clustercopy;
		WeightMatrices weightcopy;

		featurecopy.resize(n);
		clustercopy.resize(n);

		for (FeatureID m = 0; m < featureCount; m++) {
			if (keep[m]) {
				weightcopy.push_back(weights[m]);
				for (NodeID i = 0; i < n; i++) {
					featurecopy[i].push_back(featurePresent[i][m]);
					clustercopy[i].push_back(clusterAssignment[i][m]);
				}
			}
		}

		featurePresent = featurecopy;
		clusterAssignment = clustercopy;
		weights = weightcopy;
	}

	if (featurePresent.size() == 0) cout << "Warning: No Features left." << endl;
}

double Model::getLogLikelihoodContributionExceptBias(Graph* g,
		FeatureMatrix* f, ClusterMatrix* c, WeightMatrices* w, double s,
		double sigma, FeatureID m) {
	double with = Model::logLikelihood(g, f, c, w, s, sigma);
	//now: either duplicate code from sampler to remove feature from matrix or duplicate likelihood code.
	NodeID nodeCount = g->size();
	assert(nodeCount > 0);
	FeatureID featureCount = (*f)[0].size();
	assert(m < featureCount);
	assert(sigma > 0);

	double sum = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
			if ((*f)[i][m] && (*f)[j][m]) {
				double strength = (*w)[m][(*c)[i][m]][(*c)[j][m]];
				sum += (i == j) ? strength : 2*strength;
			}
		}
	}

	double average = sum / (nodeCount*nodeCount);

	vector<int> backup(nodeCount);

	for (NodeID i = 0; i < nodeCount; i++) {
		backup[i] = (*c)[i][m];
		(*c)[i][m] = -1;
		(*f)[i][m] = false;
	}

	double without = Model::logLikelihood(g, f, c, w, s+average, sigma);
	for (NodeID i = 0; i < nodeCount; i++) {
		(*c)[i][m] = backup[i];
		(*f)[i][m] = (bool)(backup[i] != -1);
	}

	return with - without;
}

double Model::loglikelihood(Graph* g, vector<vector<double> >* T,
		double bias, double variance) {
	assert(variance > 0);
	double result = 0;
	NodeID n = g->size();
	for (NodeID i = 0; i < n; i++) {
			for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
				double diff = (*T)[i][j] - g->hasEdge(i,j);
				double loglinkDensity =  - (diff*diff); //normalLogProbabilityDensity(getLinkMean(i, j, f, c, w, s), sigma ,g->hasEdge(i,j));
				result += loglinkDensity;
				if (j != i) result += loglinkDensity;//we assume a symmetric matrix and iterate only over one half. The missing duplicate is added here
			}
		}
		result = result/(2*variance*variance);
		result += n*n*(-log(variance) - 0.5*log(2*M_PI));
		assert(result == result);
		return result;
}

double Model::getLinkMean(NodeID a, NodeID b, vector<bitPool> * compressedFeatures, ClusterMatrix * c, WeightMatrices * w, double s) {
	int aFeatures = compressedFeatures->at(a);
	int bFeatures = compressedFeatures->at(b);
	int common = aFeatures & bFeatures;
	FeatureID m = 0;
	double result = s;
	while (common) {
		if (common & 1) {
			ClusterID ac = (*c)[a][m];
			ClusterID bc = (*c)[b][m];
			result += (*w)[m][ac][bc];
		}
	common = common >> 1;
	m++;
	}
	return result;
}

double Model::getLinkMean(NodeID a, NodeID b, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s) {
	//sum over all active features
	//add weights
	double interimResult = 0;
	assert(a < f->size());
	assert(b < f->size());
	assert(s == s);
	if (abs(s) == numeric_limits<double>::infinity()) {
		cerr << "s: " << s << endl;
	}
	FeatureID featureCount = (*f)[a].size();
	assert(featureCount == (*f)[b].size());
	for (FeatureID i = 0; i < featureCount; i++) {//eventually replace with iterators
		if ((*f)[a][i] && (*f)[b][i])
		{
			assert((*c)[a][i] >= 0);
			assert((*c)[b][i] >= 0);
			assert ((*w)[i].size() > (unsigned) (*c)[a][i] && (*w)[i].size() > (unsigned) (*c)[b][i]);
			double weight = (*w)[i][(*c)[a][i]][(*c)[b][i]];
			if (abs(weight) == numeric_limits<double>::infinity()) {
				cerr << "Infinite weight at w[" << i << "][" << (*c)[a][i] << "][" << (*c)[b][i] << "]! Rather unfortunate, don't you think?" << endl;
			}
			interimResult += weight;
		}
	}
	assert (interimResult == interimResult);
	if (abs(interimResult) == numeric_limits<double>::infinity()) {
		cerr << "Infinite edge link between nodes " << a << " and " << b << "! They sure are close to each other..." << endl;
	}
	return interimResult + s;
}

int Model::clustersShared(NodeID a, NodeID b, ClusterMatrix* c) {
	int result = 0;
	assert(c->size() > 0);
	assert(a >= 0 && b >= 0);
	assert(a < c->size() && b < c->size());
	for (FeatureID m = 0; m < (*c)[0].size(); m++) {
		if ((*c)[a][m] == (*c)[b][m] && (*c)[b][m] != -1) result++;
	}
	return result;
}

int Model::clustersNotShared(NodeID a, NodeID b, ClusterMatrix* c) {
	int result = 0;
	assert(c->size() > 0);
	assert(a >= 0 && b >= 0);
	assert(a < c->size() && b < c->size());
	for (FeatureID m = 0; m < (*c)[0].size(); m++) {
		if ((*c)[a][m] != (*c)[b][m]) result++;
	}
	return result;
}

vector<vector<int> > Model::clusterDifferenceTriangle(ClusterMatrix* c1, ClusterMatrix* c2) {
	assert(c1->size() == c2->size());
	vector<vector<int> > result(c1->size());
	for (NodeID i = 0; i < c1->size(); i++) {
		result[i].resize(c1->size());
		for (NodeID j = 0; j < c1->size(); j++) {
			bool c1shared = true;
			for (FeatureID m = 0; m < (*c1)[i].size(); m++) {
				if ((*c1)[i][m] != (*c1)[j][m]) c1shared = false;
			}
			bool c2shared = true;
			for (FeatureID m = 0; m < (*c2)[i].size(); m++) {
				if ((*c2)[i][m] != (*c2)[j][m]) c2shared = false;
			}
			result[i][j] = (c1shared == c2shared) ? 1 : 0;
		}
	}
	return result;
}

int Model::clusterDifferenceRestricted(ClusterMatrix * c1, FeatureID k, ClusterMatrix * c2, FeatureID l) {
	assert(c1->size() == c2->size());
	NodeID nodeCount = c1->size();
	int result = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		assert((*c1)[i].size() == (*c1)[0].size());
		assert(k < (*c1)[i].size());
		assert((*c2)[i].size() == (*c2)[0].size());
		assert(l < (*c2)[i].size());
		for (NodeID j = 0; j < nodeCount; j++) {
			bool c1shared = (bool) ((*c1)[i][k] == (*c1)[j][k] && (*c1)[i][k] != -1);
			bool c2shared = (bool) ((*c2)[i][l] == (*c2)[j][l] && (*c2)[i][l] != -1);
			if (c1shared != c2shared) result++;
		}
	}
	return result/(nodeCount*nodeCount);
}

double Model::getLogLikelihoodContribution(Graph * g, FeatureID m) {
	return Model::getLogLikelihoodContribution(g, &featurePresent, &clusterAssignment, &weights, s, sigma, m);
}

double Model::getLogLikelihoodContribution(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double var, FeatureID m) {
	double with = Model::logLikelihood(g, f, c, w, s, var);
	//now: either duplicate code from sampler to remove feature from matrix or duplicate likelihood code.
	NodeID nodeCount = g->size();
	assert(nodeCount > 0);
	FeatureID featureCount = (*f)[0].size();
	assert(m < featureCount);
	assert(var > 0);
	vector<int> backup(nodeCount);

	for (NodeID i = 0; i < nodeCount; i++) {
		backup[i] = (*c)[i][m];
		(*c)[i][m] = -1;
		(*f)[i][m] = false;
	}
	double without = Model::logLikelihood(g, f, c, w, s, var);
	for (NodeID i = 0; i < nodeCount; i++) {
		(*c)[i][m] = backup[i];
		(*f)[i][m] = (bool)(backup[i] != -1);
	}

	return with - without;
}

Model Model::generateModel(size_t n, double mu_w, double sigma_w, double mu_s,
		double sigma_s) {
	Settings settings = Settings::getDefaultSettings();
	double alpha = sampleGamma(settings.alpha_shape,settings.alpha_scale);
	double gamma = sampleGamma(settings.gamma_shape,settings.gamma_scale);

	return generateModel(n, alpha, gamma, mu_w, sigma_w, mu_s, sigma_s);
}

Model Model::generateModel(Settings settings) {
	if (settings.crp_only) {
		return singleFullFeatureModel(settings);
	} else {
		return generateModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.mu_weights, settings.sigma_weights, settings.mu_bias, settings.sigma_bias);
	}
}

Model Model::emptyModel(Settings settings) {
	return emptyModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.init_bias);
}

Model Model::emptyModel(NodeID n, double alpha, double gamma, double s) {
	assert(alpha >= 0);
	assert(gamma >= 0);
	assert(n >= 0);
	FeatureMatrix f(n);
	ClusterMatrix c(n);
	WeightMatrices w(0);

	Model result(f, c, w, alpha, gamma, s);
	return result;
}

Model  Model::exampleModel() {
	/*
	 * This is the example Model used by Palla et al.
	 */
	NodeID n = 30;
	FeatureMatrix f(n);
	ClusterMatrix c(n);

	for (NodeID i = 0; i < n; i++) {
		f[i].resize(2,true);
		f[i][0] = (bool)(i >= 5 && i < 25);
		c[i].resize(2,-1);
		if (i >= 5 && i < 15) c[i][0] = 0;
		else if (i >= 15 && i < 25) c[i][0] = 1;
		if (i < 10) c[i][1] = 0;
		else if (i < 20) c[i][1] = 1;
		else c[i][1] = 2;
	}
	double mu_w = 2;
	double sigma_w = 4;
	WeightMatrices w = sampleWeightMatrices(c, mu_w, sigma_w);
	return Model(f, c, w);
}

Model Model::singleFullFeatureModel(NodeID n, double alpha, double gamma, double s, double sigma) {
	FeatureMatrix f(n);
	ClusterMatrix c(n);
	WeightMatrices w(1);
	w[0].resize(n);

	for (NodeID i = 0; i < n; i++) {
		f[i] = std::vector<bool>({true});
		c[i] = std::vector<int>({int(i)});
		w[0][i].resize(n);
		for (NodeID j = 0; j < n; j++) {
			w[0][i][j] = 0;
		}
	}

	return Model(f, c, w, alpha, gamma, s, sigma);
}

Model Model::singleFullFeatureModel(Settings settings) {
	return singleFullFeatureModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.init_bias, settings.init_variance);
}

bool Model::isConstructed() {//obsolete. TODO: contemplate removal
	return constructed;
}

double Model::likelihood(Graph * g) {
	return likelihood(g, &featurePresent, &clusterAssignment, &weights, s, sigma);
}

double Model::logLikelihood(Graph* g) {
	assert(g->size() == featurePresent.size());
	return logLikelihood(g, &featurePresent, &clusterAssignment, &weights, s, sigma);
}

double Model::likelihood(Graph * g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma) {
	//TODO: maybe just scrap this method. It's not in use anywhere and is not going to be.
	double likelihood = 1;
	assert(g->size() == f->size());
	assert(g->size() == c->size());
	for (NodeID i = 0; i < g->size(); i++) {
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
			//Actually the probability is more like pdf*numeric_limits<double>min();
			//But since we are using densities here, this is not a problem.
			double linkDensity = normalProbabilityDensity(getLinkMean(i, j, f, c, w, s), sigma ,g->hasEdge(i,j));
			likelihood *= linkDensity;
			if (j != i) likelihood *= linkDensity;//see comment at same row of getLogLikelihood
		}
	}
	assert(likelihood >= 0);
	//assert(likelihood <= 1);
	return likelihood;
}

double Model::logLikelihood(Graph *g, FeatureMatrix * f, vector<bitPool> * compressedFeatures, ClusterMatrix * c, WeightMatrices * w, double s, double sigma) {
	double result = 0;
	assert(sigma > 0);
	NodeID n = g->size();
	for (NodeID i = 0; i < n; i++) {
		for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
				double diff;

				if (w->size() < 8*sizeof(bitPool)) diff = getLinkMean(i, j, compressedFeatures, c, w, s) - g->hasEdge(i,j);
				else diff = getLinkMean(i,j,f,c,w,s) - g->hasEdge(i,j);

				double loglinkDensity =  - (diff*diff); //normalLogProbabilityDensity(getLinkMean(i, j, f, c, w, s), sigma ,g->hasEdge(i,j));
				result += loglinkDensity;
				if (j != i) result += loglinkDensity;//we assume a symmetric matrix and iterate only over one half. The missing duplicate is added here
			}
		}
	result = result/(2*sigma*sigma);
	result += n*n*(-log(sigma) - 0.5*log(2*M_PI));
	//assert(result < 0);//logarithms of probabilities
	assert(result == result);
	return result;
}

double Model::logLikelihood(Graph *g, FeatureMatrix * f, ClusterMatrix * c, WeightMatrices * w, double s, double sigma) {
	double result = 0;
	assert(sigma > 0);
	NodeID n = g->size();
	for (NodeID i = 0; i < n; i++) {
			for (NodeID j = 0; j <= i; j++) {//We assume the graph is undirected
				double diff = getLinkMean(i, j, f, c, w, s) - g->hasEdge(i,j);
				double loglinkDensity =  - (diff*diff); //normalLogProbabilityDensity(getLinkMean(i, j, f, c, w, s), sigma ,g->hasEdge(i,j));
				result += loglinkDensity;
				if (j != i) result += loglinkDensity;//we assume a symmetric matrix and iterate only over one half. The missing duplicate is added here
			}
		}
	result = result/(2*sigma*sigma);
	result += n*n*(-log(sigma) - 0.5*log(2*M_PI));
	//assert(result < 0);//logarithms of probabilities
	assert(result == result);
	return result;
}

Model Model::generateModel(size_t n, double alpha, double gamma, double mu_w, double sigma_w, double mu_s, double sigma_s) {
	assert(alpha > 0);
	assert(gamma >= 0);
	assert(n >= 0);
	FeatureMatrix f = sampleFeatureMatrixwithIBP(n, alpha);
	ClusterMatrix c = sampleClusterAssignmentsbyCRP(n, gamma, f);
	WeightMatrices w = sampleWeightMatrices(c, mu_w, sigma_w);
	double s = sampleNormal(mu_s, sigma_s);

	return Model(f, c, w, alpha, gamma, s);
}

Graph Model::sampleGraphInstance(double variance) {
	assert(constructed);
	vector<vector<edgetype> > edgeArray(n);
	for (NodeID i = 0; i < n; i++) {
		edgeArray[i].resize(n, false);
		for (NodeID j = 0; j <= i; j++) {
			edgeArray[i][j] = sampleNormal(getLinkMean(i,j), variance);
			edgeArray[j][i] = edgeArray[i][j];
		}
	}
	return Graph(edgeArray);
}

FeatureMatrix Model::getFeatureMatrix() {
	return featurePresent;
}
ClusterMatrix Model::getClusterMatrix() {
	return clusterAssignment;
}

WeightMatrices Model::getWeights() {
	return weights;
}

NodeID Model::getNodeCount() {
	return featurePresent.size();
}

FeatureID Model::getFeatureCount() {
	return weights.size();
}

int Model::getTotalLinkCount() {
	FeatureID featureCount = weights.size();
	int result = 0;
	for (FeatureID m = 0; m < featureCount; m++) {
		/**
		 * calling countClusters is a bit inefficient,
		 * because it traverses the array in the wrong dimension,
		 * causing cache misses.
		 * However, this method won't be called that often and is more
		 * readable this way.
		 */
		int cl = countClusters(&clusterAssignment, m);
		result += cl*cl;
	}
	return result;
}

void Model::printWeightMatrices() {
	/**
	 * Print the weight matrices to stout. Currently somewhat difficult to interpret
	 */

	for (FeatureID m = 0; m < weights.size(); m++) {
		for (ClusterID c = 0; c < weights[m].size(); c++) {
			assert(weights[m][c].size() == weights[m].size());
			for (ClusterID d = 0; d < weights[m].size(); d++) {
				cout << weights[m][c][d] << " ";
			}
			cout << endl;
		}
		cout << endl;
	}
}

void Model::printClusters(ClusterMatrix crpresult) {
	/**
	 * Print C to stdout. If a feature isn't selected, a blank line is printed.
	 */
	for (NodeID i = 0; i < crpresult.size(); i++) {
		cout << "Node ";
		if (i < 10) cout << " ";
		cout << i << ": ";
		FeatureID featureCount = crpresult[i].size();
		for (FeatureID k = 0; k < featureCount; k++) {
			if (crpresult[i][k] != -1)
				cout << " " << crpresult[i][k];
			else
				cout << "  ";
		}
		cout << endl;
	}
}

void Model::printLikelihoodContribution(Graph graph) {
	for (FeatureID k = 0; k < weights.size(); k++) {
		std::streamsize oldp = cout.precision(2);
		cout << " "
				<< Model::getLogLikelihoodContribution(&graph,
						&featurePresent, &clusterAssignment,
						&weights, s,
						sigma, k);
		cout.precision(oldp);
	}
	cout << endl;
}

}
}
