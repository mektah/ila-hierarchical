/*
 * Graph.cpp
 *
 *  Created on: 30.11.2012
 *      Author: moritz
 */

#include <fstream>
#include <assert.h>

#include "Graph.h"

namespace CILA {
namespace core {

Graph::Graph() {
	nodeCount = 0;
}

Graph::Graph(vector<vector<edgetype> > edgeArray) {
	/**
	 * Create graph from edge array.
	 * edgeArray must be quadratic.
	 */
	this->edges = edgeArray;
	nodeCount = edges.size();
	if (nodeCount > 0) assert(edges[0].size() == edges.size());
}

Graph::~Graph() {

}

NodeID Graph::size() {
	return nodeCount;
}

edgetype Graph::hasEdge(NodeID i, NodeID j)  {
	assert(i < nodeCount);
	assert(j < nodeCount);
	return edges[i][j];
}

void Graph::addNode(vector<edgetype> connections) {
	/**
	 * Add a node.
	 * The vector "connections" describes the edges to all
	 * existing nodes and the reflexive edge. Must be of the correct size.
	 */
	assert(connections.size() == nodeCount+1);
	for (NodeID i = 0; i < edges.size(); i++) {
		edges[i].push_back(connections[i]);
	}
	edges.push_back(connections);
	for (NodeID i = 0; i < edges.size(); i++) {
		assert(edges[i].size() == edges.size());
	}
	nodeCount++;
}

Graph Graph::getSubgraph(NodeID limit) {
	/**
	 * Get a Graph containing only the nodes 0..limit-1
	 */
	assert(limit <= nodeCount);
	assert(limit >= 0);
	if (limit == 0) return Graph();
	vector<vector<edgetype> > edgecopy(limit);
	for (NodeID i = 0; i < limit; i++) {
		edgecopy[i].resize(limit);
		edgecopy[i].assign(edges[i].begin(),edges[i].begin()+limit);
	}
	return Graph(edgecopy);
}

vector<edgetype> Graph::getNodeSlice(NodeID i) {
	/**
	 * Get the connections from node i to all nodes j with j <= i.
	 * Right format to feed into addNode.
	 */
	vector<edgetype> result(i+1);
	for (NodeID j = 0; j <= i; j++) {
		result[j] = edges[i][j];
	}
	return result;
}

void Graph::permute(vector<NodeID> perm) {
	/**
	 * Permute the graph according to perm.
	 */
	//first permute rows
	assert(perm.size() == nodeCount);
	vector<vector<edgetype> > edgecopy(nodeCount);
	for (NodeID i = 0; i < perm.size(); i++) {
		edgecopy[i].resize(nodeCount);
		vector<edgetype> rawcopy = edges[perm[i]];
		for (NodeID j = 0; j < nodeCount; j++) {
			edgecopy[i][j] = rawcopy[perm[j]];
		}
	}
	edges = edgecopy;
}

bool Graph::equals(Graph other) {
	assert(this->size() == other.size());
	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {
			if (hasEdge(i,j) != other.hasEdge(i,j)) return false;
		}
	}
	return true;
}

void Graph::exportToGraphviz(string name) {
	assert(nodeCount > 0);
	double min = edges[0][0];
	double max = edges[0][0];
	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {
			if (edges[i][j] < min) min = edges[i][j];
			if (edges[i][j] > max) max = edges[i][j];
		}
	}

	string datafilename = name + ".dat";
	ofstream datahandle;
	datahandle.open(datafilename.c_str());
	datahandle << "graph " << name << "{ ";

	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {
			if (i == j) continue;
			double length = 0.5 + max - edges[i][j];
			datahandle << "n" << i << " -- n" << j << " [len=" << length << ", style=invis];" << endl;
		}
	}
	datahandle << "}";
	datahandle.close();
}

}
}
