/*
 * types.h
 *
 *  Created on: 30.11.2012
 *      Author: moritz
 */

#ifndef TYPES_H_
#define TYPES_H_

#include <vector>

using std::vector;

namespace CILA {
namespace core {

typedef unsigned NodeID;
typedef unsigned ClusterID;
typedef unsigned FeatureID;
typedef vector<vector<vector<double> > > WeightMatrices;
typedef vector<vector<int> > ClusterMatrix;
typedef vector<vector<bool> > FeatureMatrix;
typedef double edgetype;
typedef vector<vector<double> > Matrix;
typedef unsigned bitPool;

}
}

#endif /* TYPES_H_ */
