//============================================================================
// Name        : main.cpp
// Author      : Moritz v. Looz
// Version     :
// Copyright   : GPLv3 or newer
// Description :
//============================================================================

#include <iostream>
#include <cstdlib>
#include <vector>
#include <time.h>
#include <math.h>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <omp.h>

#include <cxxopts.hpp>

#include "Graph.h"
#include "generative.h"
#include "types.h"
#include "sampler.h"
#include "Input.h"
#include "Util.h"
#include "Plotting.h"
#include "Aggregation.h"
#include "TestChamber.h"
#include "Analysis.h"
#include "Model.h"
#include "ModelIO.h"

using namespace CILA;
using namespace core;
using namespace io;


vector<double> partialTestRun(Graph graph, Settings settings, string name) {
	/**
	 * This method does some partly supervised learning.
	 * We read the real clusters from ../Data_Sz_HC_reordered.csv and use only the nodes from 0 .. limit-1 initially.
	 * Z and C are fixed and we sample only W. After samplerun steps, the remaining nodes are sorted into clusters
	 * and the assignments are compared to the ground truth.
	 */

	//load ground truth
	Input gt;
	gt.loadGroundTruth(settings.gtpath);
	vector<vector<string> > rawdata = Input::loadRawArray(settings.gtpath);

	//draw a permutation
	vector<NodeID> perm = Util::randPermutation(graph.size());

	//permute graph and do some sanity checks
	Graph graph2 = graph;
	gt.permute(perm);
	graph.permute(perm);
	Graph permuted = graph;
	permuted.permute(Util::reverse(perm));
	assert(permuted.equals(graph2));

	//check symmetry
	for (NodeID i = 0; i < perm.size(); i++) {
		for (NodeID j = 0; j < perm.size(); j++) {
			assert(graph.hasEdge(i,j) == graph.hasEdge(j,i));
		}
	}

	FeatureID featureCount = gt.groundTruthFeatureCount();
	ClusterMatrix cTG = gt.getgTClusters();

	//collect the number of clusters for each feature
	vector<int> clusters(featureCount, 0);

	for (NodeID i = 0; i < graph.size(); i++) {
		for (FeatureID m = 0; m < featureCount; m++) {
			if (clusters[m]	<= cTG[i][m]) clusters[m] = cTG[i][m]+1;
			}
	}

	//start with random weight matrices
	WeightMatrices w = sampleWeightMatrices(cTG, settings.mu_weights, settings.sigma_weights);

	//crop C and R to limit
	cTG.resize(settings.limit);
	Graph partial = graph.getSubgraph(settings.limit);

	assert(partial.size() == cTG.size());

	Sampler sampler(partial, cTG, w, settings);

	//do the sampling
	for (int i = 0; i < settings.iterations; i++) {
		if (settings.sample_variance) sampler.sampleHyperVariance();
		sampler.sampleHyperBias();
		sampler.sampleStepW();
		//sampler.sampleHyperAlpha();
		//sampler.sampleHyperGamma();
		if (i % 5 == 0 && !settings.silent) {
			cout << i << ": ";
			cout << Model::logLikelihood(&partial, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance());
			cout << ", with " << sampler.featureCount() << " features and " << sampler.totalClusterCount();
			cout << " clusters. ";
			cout << " Bias:" << sampler.getBias();
			cout << " Alpha:" << sampler.getAlpha();
			cout << " Gamma:" << sampler.getGamma();
			cout << " Variance:" << sampler.getVariance() << endl;
		}
		sampler.saveChainState();
	}

	ModelIO::writeSampler(settings, name, sampler, false);

	if (!settings.silent) {
		//print how important each feature is in explaining the graph
		for (FeatureID k = 0; k < sampler.featurePresent[0].size(); k++) {
			cout << " " << Model::getLogLikelihoodContribution(&partial, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance(), k);
		}
	}

	/**
	 * Now we assign the remaining nodes to the predefined clusters and count how often each node is assigned to each cluster.
	 */
	vector<int> accurateClusters(featureCount,0);
	int trials = settings.repeat;
	for (NodeID i = settings.limit; i < graph.size(); i++) {
		sampler.addNode(graph.getNodeSlice(i));

		vector<vector<int> > popularities(featureCount);
		for (FeatureID m = 0; m < featureCount; m++) popularities[m].resize(clusters[m], 0);

		for (int j = 0; j < trials; j++) {
			vector<int> assignments = sampler.sampleNode(i, 0, false);
			/*
			 * There is definitely a bug here. If the following line is active, all nodes after the first one are never sorted into clusters!
			 * TODO: investigate!
			 */
			if (j < trials -1) sampler.clearNode(i);
			for (FeatureID m = 0; m < featureCount; m++) {
				int as = assignments[m];
				if (as >= 0) popularities[m][as]++;
			}
		}
		NodeID realnode = perm[i];
		if (!settings.silent) cout << "Node " << realnode << endl;
		for (FeatureID m = 0; m < featureCount; m++) {
			if (!settings.silent) {
				cout << "	" << m << ": ";
				for (ClusterID c = 0; c < popularities[m].size(); c++) {
					if (gt.lowerBound(m, c) == gt.upperBound(m,c)) {
						cout << gt.lowerBound(m, c) << ": " << popularities[m][c] << ", ";
					} else {
						cout << gt.lowerBound(m, c) << "<: " << popularities[m][c] << " ";
					}
				}
				if (gt.upperBound(m,clusters[m]-1) != gt.lowerBound(m,clusters[m]-1))	cout << gt.upperBound(m,clusters[m]-1);
				cout << endl;
				string realvalue = rawdata[realnode][m+1];//TODO: m+1 happens to be right as of now, but needs to be fixed later
				cout << "True value:" << realvalue << endl;
			}
			int realCluster = gt.groundTruthCluster(i, m); //use i, not realnode! gt is already permuted!
			if (!settings.silent) cout << "Real Cluster:" << realCluster << endl;
			assert((realCluster == -1) || (realCluster < popularities[m].size()));
			int sortedcorrectly = popularities[m][realCluster];
			assert(sortedcorrectly >= 0);
			assert(sortedcorrectly <= trials);
			accurateClusters[m] += sortedcorrectly;
		}
	}
	if (!settings.silent) cout << "Accuracy:" << endl;
	int max = (trials*(graph.size()-settings.limit));
	vector<double> result(accurateClusters.size());
	for (FeatureID m = 0; m < featureCount; m++) {
		double accC = accurateClusters[m];
		if (!settings.silent) cout << "Feature " << m << ":" << accC << " / " << max << "=" << accC / max <<  endl;
		result[m] = accC / max;
	}
	return result;
}

void printResults(ClusterMatrix& labels, Sampler& result, Model& testModel, Settings settings, bool randomgraph) {

	Model last = result.getLastChainlink();
	ClusterMatrix clusters = last.getClusterMatrix();
	ClusterMatrix crpresult = testModel.getClusterMatrix();
	NodeID n = clusters.size();

	if (randomgraph) {
			double sum = Util::sumMatrix(Model::clusterDifferenceTriangle(&crpresult, &result.clusterAssignment));
			cout << "Similarity " << sum << "/" << n*n << "=" << sum / (n*n) << endl;
	}
	if (labels[0].size() > 0 && result.featureCount() > 0) {
		vector<double> cache = Util::averagePurityAndNMIMax(clusters, labels);
		cout << "PurityAverage: "
				<< cache[0] << endl;
		cout << "NMIAverage: "
				<< cache[1] << endl;
		Analysis::printPurity(labels, last);
		cout << "NMI:" << endl;
		Analysis::printNMI(labels, last);
		Analysis::printDistances(testModel, last);
		if (!randomgraph) {
			cout << "Anova:" << endl;
			Analysis::printAnova(Input::parseDoubles(Input::loadRawArray(settings.gtpath)), last);
		}
	}

}

void printProgress(Sampler& sampler, Graph& R, int i) {
		cout << i << ": ";
		cout << Model::logLikelihood(&R, &sampler.featurePresent, &sampler.clusterAssignment, &sampler.weights, sampler.getBias(), sampler.getVariance());
		cout << ", with " << sampler.featureCount() << " features and " << sampler.totalClusterCount();
		cout << " clusters. ";
		cout << " Bias:" << sampler.getBias();
		cout << " Alpha:" << sampler.getAlpha();
		cout << " Gamma:" << sampler.getGamma();
		cout << " Variance:" << sampler.getVariance() << endl;
}

int main(int argc, char* argv[]) {
	cxxopts::Options options(argv[0], " - Supported options");
	Settings settings = Settings::getDefaultSettings();
	int timestop = 100;
	time_t start = time(NULL);
	time_t seed = start;
	string name = std::to_string(start);
	string evalmodel;
	int seededrepeat = 1;

	/*
	 * add CLI options
	 */

	options.add_options()
			("help", "display options")
			("version", "show version")
			("init_variance", "set variance. If set to 0, variance is sampled", cxxopts::value<double>(settings.init_variance)->default_value(std::to_string(settings.init_variance)))
			("iterations", "number of iterations", cxxopts::value<int>(settings.iterations)->default_value(std::to_string(settings.iterations)))
			("chains", "number of chains", cxxopts::value<int>(settings.chains)->default_value(std::to_string(settings.chains)))
			("repeat", "repetitions of parameter runs", cxxopts::value<int>(settings.repeat)->default_value(std::to_string(settings.repeat)))
			("permute", "permute nodes randomly to prevent order from influencing results")
			("seededrun", "seed clusters with known data?", cxxopts::value<int>(seededrepeat))
			("seed", "seed for pseudorandom number generator", cxxopts::value<time_t>(seed))
			("euclid", "use negative euclidean distance instead of dot product for similarity")
			("timeplot", "run successively larger samples and plot time", cxxopts::value<int>(timestop))
			("optimap", "evaluate effect of optimizations")
			("plot", "plot everything")
			("silent", "reduce output")
			("thin", "only store every kth sample", cxxopts::value<int>(settings.thin)->default_value(std::to_string(settings.thin)))
			("m_aux", "number of temporary new clusters to create during cluster selection", cxxopts::value<int>(settings.m_aux)->default_value(std::to_string(settings.m_aux)))
			("nooldclusters", "don't allow cluster selection algorithm to select empty clusters")
			("limit_total_link_count", "limit model complexity", cxxopts::value<int>(settings.maxTLC)->default_value(std::to_string(settings.maxTLC)))
			("endlesschains", "run one chain after the other, plot cumulative stats after each")
			("endless", "run fixed number of chains, save runs after --iterations steps")
			("partlyfixed", "run large set of tests with partly fixed values")
			("exacting", "test model and random graphs repeatedly")
			("hypermap", "map hyperparameters")
			("alphamap", "map alpha")
			("gammamap", "map gamma")
			("biasmap", "map bias")
			("noisemap", "map noise")
			("evalmodel", "evaluate model agains ground truth", cxxopts::value<string>())
			("priorBaseline", "sample solutions from prior, compare with model", cxxopts::value<string>())
			("input", "input file. If none given, random input is generated.", cxxopts::value<string>(settings.inputpath)->default_value(settings.inputpath))
			("ground_truth", "ground truth file", cxxopts::value<string>(settings.gtpath)->default_value(settings.gtpath))
			("normalize_input", "divide values in input file by average, feature-wise")
			("mu_bias", "mean of bias", cxxopts::value<double>(settings.mu_bias)->default_value(std::to_string(settings.mu_bias)))
			("sigma_bias", "variance of bias", cxxopts::value<double>(settings.sigma_bias)->default_value(std::to_string(settings.sigma_bias)))
			("mu_weights", "mean of weights", cxxopts::value<double>(settings.mu_weights)->default_value(std::to_string(settings.mu_weights)))
			("sigma_weights", "variance of weights", cxxopts::value<double>(settings.sigma_weights)->default_value(std::to_string(settings.sigma_weights)))
			("left_order", "reorder features during mapping?")
			("adaptive_thresholds", "choose thresholds during aggregation to match alpha and gamma")
			("gamma_shape", "shape of gamma prior", cxxopts::value<double>(settings.gamma_shape)->default_value(std::to_string(settings.gamma_shape)))
			("gamma_scale", "scale of gamma prior", cxxopts::value<double>(settings.gamma_scale)->default_value(std::to_string(settings.gamma_scale)))
			("alpha_shape", "shape of alpha prior", cxxopts::value<double>(settings.alpha_shape)->default_value(std::to_string(settings.alpha_shape)))
			("alpha_scale", "scale of alpha prior", cxxopts::value<double>(settings.alpha_scale)->default_value(std::to_string(settings.alpha_scale)))
			("num_threads", "number of threads", cxxopts::value<int>(settings.num_threads)->default_value(std::to_string(settings.num_threads)))
			("analyze-files", "files to analyze", cxxopts::value<vector<string> >())
			("crponly", "sample only with Chinese restaurant process")
			;

	//po::positional_options_description p;
	//p.add("analyze-files", -1);


	options.parse(argc, argv);

	if (options.count("help")) {
	    cout << options.help() << "\n";
	    return 0;
	}

	if (options.count("version")) {
		cout << settings.majorversion << "." << settings.minorversion << "." << settings.subminorversion << endl;
		return 0;
	}

	/**
	 * Check for invalid option combinations.
	 */

	if (options.count("seededrun") && options.count("timeplot")) {
		cout << "Plotting the time for seeded clusters is not implemented." << endl;
		return 1;
	}

	if (!options.count("input") && options.count("seededrun")) {
		cout << "Need input data for seeded run." << endl;
		return 1;
	}

	if (options.count("timeplot") && options.count("randomgraph")) {//TODO: well, we could use slices of the model instead.
		cout << "Time plotting creates it's own graphs." << endl;
	}

	if (options.count("endlesschains")) {
		if (options.count("timeplot") || options.count("seededrun")) {
			cout << "Options incompatible" << endl;
			return 1;
		}
	}

	if (settings.init_variance == 0) {
		settings.sample_variance = true;
		settings.init_variance = 1;
	}

	if (options.count("seed")) {
	    cout << "Random seed was set to "
	 << options["seed"].as<time_t>() << ".\n";
	} else {
	    cout << "Random seed was not set. Initializing with " << seed << endl;
	}

	setSeed(seed);
	double gamma = settings.init_gamma;
	double alpha = settings.init_alpha;

	/**
	* Settings flags without arguments couldn't be transfered directly to settings object. Do that now.
	*/
	settings.euclid = options.count("euclid");
	settings.leftOrder = options.count("left_order");
	settings.crp_only = options.count("crponly");
	settings.adaptiveThresholds = options.count("adaptiveThresholds");
	settings.retryOld= !options.count("nooldclusters");
	settings.plot = options.count("plot");
	settings.silent = options.count("silent");
	settings.permute = options.count("permute");
	settings.randomgraph = (settings.inputpath.size() == 0);
	settings.normalize_input = options.count("normalize_input");

	if (settings.crp_only) {
		settings.sample_Z = false;
	}

	if (options.count("evalmodel")) {
		if (!options.count("ground_truth")) {
			throw std::runtime_error("Need a ground truth file to evaluate the model.");
		}
		string filename = options["evalmodel"].as<string>();

		Model model = ModelIO::readModel(filename);

		cout << "Cluster Assignments:" << endl;
		Model::printClusters(model.clusterAssignment);

		cout << "Weights:" << endl;
		model.printWeightMatrices();

		Input gt;
		gt.loadGroundTruth(settings.gtpath);

		ClusterMatrix clusters = gt.getgTClusters();
		cout << "Balanced Purity" << endl;
		Analysis::printPurity(clusters, model);
		cout << "NMI" << endl;
		Analysis::printNMI(clusters, model);
		cout << "Anova" << endl;
		bool colmask[] = {0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1};
		vector<vector<double> > raw = Input::parseDoubles(Input::loadRawArray(settings.gtpath));
		vector<vector<double> > noncategorical(raw.size());
		for (NodeID i = 0; i < raw.size(); i++) {
			assert(raw[i].size() == raw[0].size());
			for (FeatureID m = 0; m < raw[i].size(); m++) {
				if (colmask[m]) noncategorical[i].push_back(raw[i][m]);
			}
		}
		Analysis::printAnova(noncategorical, model);

		return 0;
	}

	if (options.count("priorBaseline"))  {
		string filename = options["priorBaseline"].as<string>();
		std::ifstream ifs(filename.c_str());
		if (!ifs) {
			throw std::runtime_error("Couldn't open "+ filename + '!');
		}
		if (!(boost::ends_with(filename, ".model"))) {
			cout << "Warning: " << filename << " does not end in .model." << endl;
		}

		Model model = ModelIO::readModel(filename);

		settings.nodeCount = model.getNodeCount();

		vector<double> hist(100, 0);
		Aggregation agg;

		for (int i = 0; i < settings.repeat; i++) {
			Model comparison = Model::generateModel(settings);
			double nmiavg = Util::averagePurityAndNMIMax(comparison.clusterAssignment, model.clusterAssignment)[1];
			if (nmiavg == 1) hist[99]++;
			else hist[nmiavg/0.01]++;
			if (i == 0) agg = Aggregation(comparison);
			else agg = Aggregation::aggregate(agg, i, Aggregation(comparison), 1);
		}
		Model collapsed = agg.collapse(settings);
		Analysis::printPurity(model.clusterAssignment, collapsed);
		Analysis::printNMI(model.clusterAssignment, collapsed);
		Analysis::printDistances(model, collapsed);

		Plotting::plotHistogramm(hist, 0, 1, "NMI-Avg-prior");
		return 0;
	}

	if (options.count("analyze-files")) {
		vector<Sampler> samplerlist;
		vector<string> filename = options["analyze-files"].as< vector<string> >();
		Model model;
		bool modelloaded = false;
		for (unsigned i = 0; i < filename.size(); i++) {
			std::ifstream ifs(filename[i].c_str());
			if (!ifs) {
				cout << "Couldn't open " << filename[i] << '!' << endl;
				continue;
			}
			try {
				if (boost::ends_with(filename[i], ".run")) {
					Sampler s = ModelIO::readSampler(filename[i]);
					cout << "Loaded run " << filename[i] << endl;
					if (s.chain.size() > 0) {
						samplerlist.push_back(s);
					}
				}
				if (boost::ends_with(filename[i], ".model")) {
					model = ModelIO::readModel(filename[i]);
					modelloaded = true;
					cout << "Loaded model " << filename[i] << endl;
				}
			}
			catch (...) {
				cout << "Skipping " << filename[i] << endl;
				continue;
			}
		}
		if (samplerlist.size() == 0) {
			throw std::runtime_error("No files loaded, ending");
		}
		Settings recset = samplerlist[0].getSettings();

		if (modelloaded) {
			Analysis::analyze(&samplerlist, model, settings);
		} else {
			Analysis::analyze(&samplerlist, settings);
		}

		return 0;
	}

	if (options.count("partlyfixed")) {
		TestChamber::fixedRuns(settings);
		return 0;
	}

	/*
	 * Plot time for model inversion dependent on n. For small values of n, the time will be a lower bound, since often no features are found.
	 */

	if (options.count("timeplot")) {
		time_t beginning = time(NULL);
		int steps = 5;
		int n = steps;
		int runs = settings.repeat;
		vector<double> averagedTime(timestop / steps);
		vector<vector<double> > wholeTime(timestop / steps);
		for (unsigned i = 0; i < wholeTime.size(); i++) wholeTime[i].resize(runs);
		#pragma omp parallel for schedule(dynamic,1) num_threads(settings.num_threads)
		for (n = steps; n <= timestop; n += steps) {
			//wholeTime[n / steps -1].resize(runs);
			double sum = 0;
			cout << "Starting " << runs << " runs with " << n << " nodes: ";
			cout.flush();
			vector<double> localtime(runs);
			for (int i = 0; i < runs; i++) {
				//generate model
				Model timedModel = Model::generateModel(n, settings.init_alpha, settings.init_gamma, settings.init_bias, settings.init_variance, settings.mu_weights, settings.sigma_weights);
				Model emptyModel = Model::emptyModel(n, settings.init_alpha, settings.init_gamma, 0);

				//take time
				cout << ".";
				cout.flush();
				time_t start = time(NULL);

				//infer model
				Sampler sampler(timedModel.sampleGraphInstance(settings.realnoise), emptyModel, settings);

				for (int j = 0; j < settings.iterations; j++) {
					sampler.sampleIter();
				}
				time_t duration = time(NULL) - start;
				localtime[i] = duration;
				sum += duration;
			}
			cout << " done" << endl;
			averagedTime[n / steps -1] = sum / runs;
			wholeTime[n / steps -1].swap(localtime);
			#pragma omp critical
			{
				//draw plot after every iteration to see intermediate results before the run finishes
				Plotting::plotHistogramm(averagedTime,steps, timestop+steps, std::to_string(beginning) + std::string(" Time"));
				Plotting::plotBoxplot(wholeTime, 0, timestop, "timeboxplot");
			}
		}
		return 0;
	}

	if (options.count("optimap")) {
		/**
		 * first dimension: compress z
		 * second dimension: cluster sampling
		 * third dimension: weight sampling
		 * fourth dimension: bias sampling
		 */
		settings.randommodel = false;
		settings.empty = true;
		settings.sequential = false;

		vector<vector<vector<vector<double> > > > avgTime(2);
		avgTime[0].resize(2);
		avgTime[1].resize(2);
		
		for (int i = 0; i < 2; i++) {
			avgTime[0][i].resize(3);
			avgTime[1][i].resize(3);
			for (int j = 0; j < 3; j++) {
				avgTime[0][i][j].resize(3);
				avgTime[1][i][j].resize(3);
			}
		}
		
		for (int i = 0; i < settings.repeat; i++) {
			Model model = Model::generateModel(settings);
			Graph R = model.sampleGraphInstance(0);
			cout << "Graph " << i+1 << " of " << settings.repeat << endl;
			for (int o = 0; o < 2; o++) {
				settings.compressZ = (o == 1);
				for (int j = 0; j < 2; j++) {
					settings.naiveclusters = (j == 0);
					for (int k = 0; k < 3; k++) {
						settings.naiveWeights = (k == 0);
						settings.conjugateWeights = (k == 2);
						for (int l = 0; l < 3; l++) {
							settings.naivebias = (l == 0);
							settings.conjugatebias = (l == 2);
							Model newmodel;
							newmodel.emptyModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.init_bias);
							time_t then = time(NULL);
							Sampler result = TestChamber::testrun(newmodel, R, settings, "Optimap");
							time_t now = time(NULL);
							avgTime[o][j][k][l] += (now - then);
					}
				}
			}
			}
		}
		for (int o = 0; o < 2; o++) {
			string compressoutput = (o == 0) ? "Uncompressed Likelihood" : "Compressed Likelihood";
			cout << compressoutput << endl;
		for (int j = 0; j < 2; j++) {
			string clusteroutput = (j == 0) ? "Naive clusters" : "Optimized clusters";
			cout << clusteroutput << ":" << endl;
			for (int k = 0; k < 3; k++) {
				if (k == 0) cout << "Naive Weights: ";
				else if (k == 1) cout << "Normal Weights: ";
				else cout << "Conjugate Weights: ";
				for (int l = 0; l < 3; l++) {
					if (l == 0) cout << "Naive Bias: ";
					else if (l == 1) cout << "Normal Bias: ";
					else cout << "Conjugate Bias: ";
					cout << avgTime[o][j][k][l] << " ";
				}
				cout << endl;
			}
		}
		}
		return 0;
	}

	if (options.count("exacting")) {
		settings.randommodel = false;
		settings.empty = true;
		settings.sequential = false;

		double average = TestChamber::averageQuality(settings);
		cout << "Average: " << average << endl;
		return 0;
	}

	if (options.count("hypermap")) {
		settings.randommodel = true;
		settings.empty = false;
		settings.sequential = false;
		double precision = 2;
		TestChamber::parametermap(precision, 20 + precision, precision, 20+precision, precision, settings);
		return 0;
	}

	if (options.count("alphamap")) {
		settings.randommodel = true;
		settings.empty = true;
		settings.sequential = true;
		double precision = 1;
		TestChamber::singleParmap(precision, 15+precision, precision, 0, settings);
		return 0;
	}

	if (options.count("gammamap")) {
		settings.randommodel = true;
		settings.empty = true;
		settings.sequential = true;
		double precision = 0.5;
		TestChamber::singleParmap(precision, 10, precision, 1, settings);
		return 0;
	}

	if (options.count("biasmap")) {
		settings.randommodel = true;
		settings.empty = false;
		settings.sequential = false;
		//settings.silent = true;
		TestChamber::singleParmap(-15, 15, 2, 2, settings);
		return 0;
	}

	if (options.count("noisemap")) {
		settings.randommodel = true;
		settings.empty = false;
		settings.sequential = false;
		//settings.silent = true;
		//settings.TLClimitActive = false;
		TestChamber::singleParmap(0, 5, 0.25, 3, settings);
		return 0;
	}

	/*
	 * save settings.
	 */
	ModelIO::writeSettings(settings, "inversion");

	/**
	 * Load similarity graph from subject data or create random graph from generative model.
	 */
	Graph R;
	ClusterMatrix labels;
	Model groundTruthModel;

	if (settings.inputpath.size() > 0) {
		R = Input::computeR(Input::parseDoubles(Input::loadRawArray(settings.inputpath)),settings.euclid, settings.normalize_input);
		settings.nodeCount = R.size();
	} else {
		groundTruthModel = Model::generateModel(settings);
		R = groundTruthModel.sampleGraphInstance(settings.realnoise);
		ModelIO::writeModel(settings, "randommodel", groundTruthModel, true);

		ClusterMatrix crpresult = groundTruthModel.getClusterMatrix();
		cout << "Generative test model A" << endl;
		Model::printClusters(crpresult);
		cout << "Alpha:" << groundTruthModel.alpha << ", Gamma:" << groundTruthModel.gamma << ", Bias:" << groundTruthModel.s << ", Variance:" << groundTruthModel.sigma << endl;
		labels = crpresult;
	}

	if (settings.gtpath.size() > 0) {
		Input gt;
		gt.loadGroundTruth(settings.gtpath);
		groundTruthModel = gt.getgTModel();
		NodeID pseudoN = groundTruthModel.getNodeCount();
		if (pseudoN != settings.nodeCount) {
			throw std::runtime_error("Ground truth and input have different node counts");
		}
		FeatureID pseudoM = groundTruthModel.getFeatureCount();
		string gtname = std::to_string(pseudoN) + string("-") + std::to_string(pseudoM) + string(".model");

		ModelIO::writeModel(settings, gtname, groundTruthModel, true);
		labels = gt.getgTClusters();
	} else {
		Model copy;
		copy.emptyModel(settings.nodeCount, settings.init_alpha, settings.init_gamma, settings.init_bias);
		groundTruthModel = copy;
	}
	assert(settings.nodeCount == R.size());
	assert(settings.nodeCount == groundTruthModel.getNodeCount());

	/**
	 * If supervised learning was selected.
	 */

	if (options.count("seededrun")) {
		vector<vector<double> > boxplot;
		FeatureID featurecount = 0;
		for (int i = 0; i < options["seededrun"].as<int>(); i++) {
			vector<double> accuracy = partialTestRun(R, settings, name + std::string("seeded"));
			featurecount = accuracy.size();
			if (boxplot.size() < accuracy.size()) boxplot.resize(accuracy.size());
			for (FeatureID m = 0; m < accuracy.size(); m++) {
				boxplot[m].push_back(accuracy[m]);
			}
		}
		Plotting::plotBoxplot(boxplot, 0, featurecount, "Accuracy-seeded");

		return 0;
	}

	double logp = groundTruthModel.logLikelihood(&R);
	Sampler result;
	vector<vector<Model> > chains;

	if (options.count("endlesschains")) {
		#pragma omp parallel shared(chains) num_threads(settings.num_threads)
		while (true) {
			//the option is called "endless" for a reason
			//double bias = sampleNormal(settings.mu_bias, settings.sigma_bias);
			cout << "Starting new chain" << endl;
			Model comparison = Model::generateModel(settings);

			Sampler interim = TestChamber::testrun(comparison, R, settings, name);

			#pragma omp critical
			{
				cout << "Accumulating chain ... ";
				cout.flush();
				chains.push_back(interim.getChain());
				cout << "complete" << endl;
				ModelIO::writeSampler(settings, name, interim, false);
			}
			#pragma omp master
			{
				cout << "Investigating cumulative mean" << endl;
				if (settings.randomgraph) {
					Analysis::investigateCumulativeMean(chains, groundTruthModel, settings);
				} else {
					Analysis::investigateCumulativeMean(chains, labels, settings);
				}
			}
		}
		return 0;
	}

	if (options.count("endless")) {
	#pragma omp parallel num_threads(settings.num_threads)
		{
			Model comparison = Model::generateModel(settings);

			Sampler sampler(R, comparison, settings);
			//sampler.setSamplingbools(true, true, true, true, true, true, samplesigma, false);//TODO: find other way to prevent sampler from saving states
			Aggregation result(comparison);//the argument doesn't matter, since it goes into the averaging with a weight of 0.

			int i = 0;
			int j = 0;
			string filename = ModelIO::writeModel(settings, name, comparison, false);

			time_t lastwrite = start;

			while (true) {
				sampler.sampleIter();
				i++;
				time_t now = time(NULL);
				if (i >= settings.burnin && i % settings.thin == 0) {
					Model model = sampler.getModel();
					string currentname = settings.getVersionString() + std::string("-") + name + std::string("-current.model");
					ModelIO::writeModel(settings, currentname, model, true);
					result = Aggregation::aggregate(result, j, Aggregation(model), 1);
					j++;
				}

				if  (!settings.silent && i % 25 == 0) {
					printProgress(sampler, R, i);
				}

				if (i % settings.iterations == 0) {
					//dump sampler to file
					if (now - lastwrite > 10 ) {//at least 10 seconds pause between two model writes
						ModelIO::writeAggregation(settings, name, result, false);
						lastwrite = time(NULL);
					}
				}
			}
		}
	}


	vector < NodeID > perm;
	if (settings.permute) {
		perm = Util::randPermutation(settings.nodeCount);
	} else {
		perm = Util::dummyPermutation(settings.nodeCount);
	}
	assert(Util::equals(Util::apply(perm, Util::reverse (perm)),Util::dummyPermutation (settings.nodeCount)));
	assert(perm.size() == settings.nodeCount);
	R.permute(perm);
	groundTruthModel.permute(perm);
	double permlogp = groundTruthModel.logLikelihood(&R);
	assert(abs(permlogp - logp) < 0.000001);
	cout << "According to model A, the graph had a logprobability of ";
	cout << logp << endl;
	cout << "Starting " << settings.iterations << " sampling steps of";
	if (settings.inputpath.size() == 0) {
		cout << " graph instance of random model";
	} else if (settings.euclid) {
		cout << " negative euclidean graph";
	} else {
		cout << " dot product graph";
	}

	cout << " with random model." << endl;

	settings.sequential = false;
	settings.empty = false;
	settings.randommodel = true;
	name = std::to_string(start) + string("-randommodel");
	Model r = Model::generateModel(settings);
	result = TestChamber::testrun(r, R, settings, name);
	ModelIO::writeSampler(settings, name, result, false);
	if (settings.permute) {
		result.permute(Util::reverse(perm));
		Model::printClusters(result.clusterAssignment);
	}

	printResults(labels, result, groundTruthModel, settings, settings.randomgraph);

	Model comparison;
	double bias = sampleNormal(settings.mu_bias,settings.sigma_bias);
	comparison.emptyModel(settings.nodeCount, alpha, gamma, bias);
	cout << "Comparison: Loglikelihood of graph with empty model:" << comparison.logLikelihood(&R)<< endl;

	if (!settings.crp_only) {
		settings.randommodel = false;
		settings.empty = true;
		settings.sequential = false;
		cout << "Starting sampling of graph, starting with empty model" << endl;
		name = std::to_string(start) + string("-empty");
		result = TestChamber::testrun(comparison, R, settings, name);
		ModelIO::writeSampler(settings, name, result, false);
		if (settings.permute) {
			result.permute(Util::reverse(perm));
			Model::printClusters(result.clusterAssignment);
		}

		for (FeatureID i = 0; i < result.clusterAssignment[0].size(); i++) {
			Plotting::exportToGraphviz(result.chain.back(), result.R, "feature" + std::to_string(i), i, false);
		}

		chains.push_back(result.getChain());

		printResults(labels, result, groundTruthModel, settings, settings.randomgraph);

		/*
		 * sequential initialisation
		 */

		settings.randommodel = false;
		settings.empty = false;
		settings.sequential = true;
		cout << "Now starting sampling with sequential initialisation." << endl;
		Model empty;
		bias = sampleNormal(settings.mu_bias,settings.sigma_bias);
		empty.emptyModel(1, alpha, gamma, bias);
		settings.sequential = true;
		name = std::to_string(start) + string("-sequential");
		result = TestChamber::testrun(empty, R, settings, name);
		ModelIO::writeSampler(settings, name, result, false);
		if (settings.permute) {
			result.permute(Util::reverse(perm));
			Model::printClusters(result.clusterAssignment);
		}

		chains.push_back(result.getChain());

		printResults(labels, result, groundTruthModel, settings, settings.randomgraph);
	}
}
