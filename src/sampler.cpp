/*
 * sampler.cpp
 *
 *  Created on: 12.12.2012
 *      Author: moritz
 *
 * Guide to this file:
 * Sampling of Z happens in:
 * 	sampleStepZandC and step_z_im.
 * Sampling of C happens in:
 * 	sampleStepZandC and decideClusterWithAux
 */

#include <vector>
#include <assert.h>
#include <boost/random.hpp>
#include <iostream>
#include <algorithm>
#include <map>

#include "types.h"
#include "sampler.h"
#include "generative.h"
#include "Util.h"

using std::min;
using std::max;
using std::numeric_limits;

namespace CILA {
namespace core {

Sampler::Sampler(Graph g, ClusterMatrix c, WeightMatrices w, Settings settings) {
	/**
	 * Create a new sampler without a Features instance. Z is redundant given C, it can be omitted.
	 * For the prior distributions of new weights and bias, default values are used.
	 * These are: mu_w = 2, sigma_w = 1, mu_bias = -1, sigma_bias = 4
	 */

	if (c.size() != g.size()) {
		throw std::runtime_error("Cluster Matrix and Graph must have same number of elements.");
	}

	FeatureID featureCount;
	if (c.size() == 0) {
		featureCount = 0;
	} else {
		featureCount = c[0].size();
	}

	//deduce Z from C
	FeatureMatrix f(c.size());
	for (NodeID i = 0; i < c.size(); i++) {
		assert(c[i].size() == c[0].size());
		f[i].resize(featureCount);
		for (FeatureID m = 0; m < featureCount; m++)  {
			f[i][m] = (bool) (c[i][m] != -1);
		}
	}

	Model model(f, c, w);

	init(g, model, settings);
}

Sampler::Sampler(Graph g, Model f, Settings settings) {
	init(g, f, settings);
}

Sampler::Sampler(Graph g, Settings settings) {
	Model model;
	settings.nodeCount = g.size();
	if (settings.crp_only) {
		model = Model::generateModel(settings);
	} else {
		model = Model::emptyModel(settings);
	}

	init(g, model, settings);
}

Sampler::Sampler() {

}

void Sampler::init(Graph g, Model f, Settings settings) {
	/**
	 * Create a new sampler using a Graph and a Features instance. The state of f is used as an inital state of the markov chain.
	 *
	 */
	this->R = g;
	assert(f.featurePresent.size() == g.size());
	assert(f.clusterAssignment.size() == f.featurePresent.size());
	if (f.featurePresent.size() > 0) assert(f.weights.size() == f.featurePresent[0].size());

	featurePresent = f.featurePresent;
	clusterAssignment = f.clusterAssignment;
	weights = f.weights;

	nodeCount = g.size();
	assert(nodeCount >= 0);
	FeatureID featureCount = featurePresent[0].size();

	this->param = settings;

	this->featurePopulation.resize(featureCount, 0);
	this->subClusterPopulation.resize(featureCount);

	this->gamma = f.gamma;
	this->alpha = f.alpha;
	this->bias = f.s;
	this->variance = f.sigma;
	this->mu_bias = settings.mu_bias;
	this->sigma_bias = settings.sigma_bias;
	this->mu_w = settings.mu_weights;
	this->sigma_w = settings.sigma_weights;

	this->sAlpha = settings.sample_alpha;
	this->sGamma = settings.sample_gamma;
	this->sBias = settings.sample_bias;
	this->sZ = settings.sample_Z;
	this->sC = settings.sample_C;
	this->sW = settings.sample_weights;
	this->sVariance = settings.sample_variance;
	this->sStates = settings.saveStates;


	assert(f.alpha >= 0);
	T = Model::computeT(&featurePresent, &clusterAssignment, &weights);
	ll = Model::loglikelihood(&R, &T, bias, variance);
	steps = 0;

	/**
	 * count feature and subcluster popularity
	 */
	countClusters();
	clusterSanityCheck();
	this->compressedZ = compressZ();
}

void Sampler::sampleIter() {

	vector<NodeID> newperm = param.permute ? Util::randPermutation(nodeCount) : Util::dummyPermutation(nodeCount);

	if (param.permute) permute(newperm);

	if (sVariance) sampleHyperVariance();
	if (sZ) {
		sampleStepZandC();
		//now remove features and clusters without inhabitants
		purgeUnusedFeatures();
		clusterSanityCheck();//not really needed any more, but can't hurt
		purgeEmptyClusters();
	}
	if (!sZ && sC) {
		for (NodeID i = 0; i < nodeCount; i++) {
			for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
				if (featurePresent[i][m]) {
					int oldassignment = clusterAssignment[i][m];
					assert(oldassignment >= 0);
					subClusterPopulation[m][oldassignment]--;
					vector<double> clusterlogProbs = decideClusterWithAux(i, m, param.m_aux, param.retryOld);
					ClusterID assignment = selectUniformLog(clusterlogProbs);
					clusterAssignment[i][m] = assignment;
					subClusterPopulation[m][assignment]++;
					purgeEmptyClusters(m);
				}
			}
		}
	}

	if (sW) sampleStepW();
	if (sAlpha) sampleHyperAlpha();
	if (sGamma) sampleHyperGamma();
	if (sBias) sampleHyperBias();
	if (param.permute) {
		vector<NodeID> rev = Util::reverse(newperm);
		permute(rev);
	}
	if (sStates && (steps % param.thin == 0))  saveChainState();
	steps++;
}

void Sampler::addNode(vector<edgetype> connections) {
	/**
	 * Adds a node to the observed graph and resizes the
	 * weight, cluster and feature matrices to fit.
	 * Used in the sequential initialization.
	 */
	assert(connections.size() == nodeCount+1);
	assert(featurePresent.size() == nodeCount);

	//add node to graph
	R.addNode(connections);
	nodeCount++;
	//add node to feature matrix and cluster matrix
	FeatureID featureCount = featurePresent[0].size();
	vector<bool> lonelynode(featureCount, false);
	featurePresent.push_back(lonelynode);
	assert(featurePresent.size() == nodeCount);
	vector<int> noclusters(featureCount, -1);
	clusterAssignment.push_back(noclusters);
	assert(clusterAssignment.size() == nodeCount);
	int comp = compressNode(nodeCount-1);
	compressedZ.push_back(comp);
}

vector<double> Sampler::decideClusterWithAux(NodeID i, FeatureID m, int m_aux, bool retryEmpty) {
	/**
	 * Compute loglikelihoods for assigning i to every present and m_aux temporary new clusters in feature m.
	 * The return vector has size oldClusters + m_aux, oldClusters being the number of clusters currently present in feature m
	 * If m_aux is set to 0, no new clusters are created.
	 *
	 * This method is inspired by the Auxiliary Variable Approach of Neal (2000, Algorithm 8), hence its name.
	 */

	assert(i >= 0 && i < featurePresent.size());
	assert(m >= 0 && m < featurePresent[i].size());
	assert(m_aux >= 0);
	assert(m_aux < 1000); //That would be just silly.
	assert(featurePresent[i][m]);//otherwise, the likelihood would be the same for every cluster
	ClusterID oldClusters = subClusterPopulation[m].size();

	/*
	 * Usually, clusters are discarded after becoming empty.
	 * However, if a cluster in feature m had only one inhabitant i before calling step_z(i,m),
	 *  it would be deleted and the node forced into a new cluster. To reduce the chain variance,
	 *   we keep empty clusters for one step and treat them as temporary new clusters to enable their reselection.
	 */
	ClusterID emptyClusters;
	if (retryEmpty) {
		emptyClusters = countEmptyClusters(m);
	}
	else {
		emptyClusters = 0;
	}
	double logprior, loglikelihood;
	loglikelihood = 0;

	vector<double> logProb(oldClusters + m_aux);
	vector<double> lPriors(oldClusters + m_aux);
	for (ClusterID c = 0; c < oldClusters + m_aux; c++) {
		if (c < oldClusters) {
			if (subClusterPopulation[m][c] == 0) {
				if (retryEmpty) {
					/*
					 * c is a leftover empty cluster. We treat it as a new one, the prior for selecting it is (gamma/(m_aux+emptyClusters))/(popularity(m) + gamma)
					 * The priors should sum to 1.
					 */
					logprior = log(gamma) -log(m_aux+emptyClusters) - log(featurePopulation[m] + gamma);
					assert(logprior == logprior);
				}
				else {
					logprior = log(0);//we don't retry leftover empty clusters.
					assert(logprior == logprior);
				}
			} else {
				/*
				 * Old cluster with more than zero inhabitants. Prior for selecting it is popularity(m,c)/(popularity(m)+gamma)
				 */
				logprior = log(subClusterPopulation[m][c]) - log(featurePopulation[m] + gamma);
				assert(logprior == logprior);
			}

			if (c > 0) {
				/*
				 * logLikelihoodNewCluster expects Z, C, W, bias and variance to be in the state they were when loglikelihood was computed.
				 * This is why we change the clusterAssignment afterwards.
				 */
				if (param.naiveclusters) {
					clusterAssignment[i][m] = c;
					if (param.compressZ) loglikelihood = Model::logLikelihood(&R, &featurePresent, &compressedZ, &clusterAssignment, &weights, bias, variance);
					else loglikelihood = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);
				} else {
					if (param.compressZ && this->featureCount() < 32) loglikelihood = Model::logLikelihoodNewCluster(&R, &compressedZ, &clusterAssignment, &weights, bias, variance, i, m, c, loglikelihood);
					else loglikelihood = Model::logLikelihoodNewCluster(&R, &featurePresent, &clusterAssignment, &weights, bias, variance, i, m, c, loglikelihood);
					clusterAssignment[i][m] = c;
				}

			} else {
				/*
				 * Since we are at the beginning of the loop, we can't just compute the diff to the previous state. We have to run a full likelihood
				 * computation. The clusterAssignment is set to c. (Zero in this case.)
				 * TODO: maybe expand method signature to get previous likelihood
				 */
				clusterAssignment[i][m] = c;
				if (param.compressZ) loglikelihood = Model::logLikelihood(&R, &featurePresent, &compressedZ, &clusterAssignment, &weights, bias, variance);
				else loglikelihood = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);
			}

		} else {
			/*
			 * Now we are beyond the realm of existing clusters, it is time for new ones.
			 */
			assert(m_aux > 0);
			logprior = log(gamma) -log(m_aux+emptyClusters) - log(featurePopulation[m] + gamma);//prior for a new cluster
			assert(logprior == logprior);

			/**
			 * sample connection weights for all connections between existing clusters and this new one
			 */
			vector<double> sampledweights;
			assert(subClusterPopulation[m].size() == weights[m].size());
			sampledweights.resize(subClusterPopulation[m].size());
			for (ClusterID k = 0; k < sampledweights.size(); k++) {
				double weight = sampleNormal(mu_w, sigma_w);
				sampledweights[k] = weight;
			}
			assert(sampledweights.size() == weights[m].size());
			assert(weights[m].size() == weights[m][0].size());

			//add row to weight matrix
			weights[m].push_back(sampledweights);
			assert(weights[m].size() == weights[m][0].size() +1);

			//add column to weight matrix
			for (ClusterID k = 0; k < sampledweights.size(); k++) {
				assert(weights[m][k].size() == weights[m].size()-1);
				weights[m][k].push_back(sampledweights[k]);
				assert(weights[m][k].size() == weights[m].size());
			}
			//finally, add reflexive weight;
			weights[m][weights[m].size()-1].push_back(sampleNormal(mu_w,sigma_w));
			assert(weights[m].size() == weights[m][weights[m].size()-1].size());
			if (param.naiveclusters) {
				clusterAssignment[i][m] = c;
				if (param.compressZ) loglikelihood = Model::logLikelihood(&R, &featurePresent, &compressedZ, &clusterAssignment, &weights, bias, variance);
				else loglikelihood = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);
			}
			else  {
				if (param.compressZ && featureCount() < 32) loglikelihood = Model::logLikelihoodNewCluster(&R, &compressedZ, &clusterAssignment, &weights, bias, variance, i, m, c, loglikelihood);
				else loglikelihood = Model::logLikelihoodNewCluster(&R, &featurePresent, &clusterAssignment, &weights, bias, variance, i, m, c, loglikelihood);
				clusterAssignment[i][m] = c;
			}
			subClusterPopulation[m].push_back(1);
			assert(subClusterPopulation[m].size() == c+1);
			subClusterPopulation[m][c]--;
		}
		lPriors[c] = logprior;
		assert(logprior == logprior);
		if (!(loglikelihood == loglikelihood)) {
			//logProb[c], weights, clusters, features, retryEmpty
			cout << "NaN at " << c << " of " << oldClusters << " + " << m_aux << endl;
			cout << "logProbs: ";
			for (unsigned e = 0; e < m_aux + oldClusters; e++) {
				cout << logProb[e] << " ";
			}
			cout << endl;
			cout << featureCount() << " features, " << totalClusterCount() << " clusters, variance:" << variance << ", i:" << i << ", m:" << m << endl;
			cout << "Empty clusters:" << emptyClusters;
			param.dumpSettings();
		}
		assert(loglikelihood == loglikelihood);
		logProb[c] = logprior + loglikelihood;
		if (!(logProb[c] == logProb[c])) {
			cout << "logProb[" << c << "] = " << logProb[c] << "!" << endl;
		}
		assert(logProb[c] == logProb[c]); //TODO: This assertion fails once in a blue moon. It shouldn't.
	}
	//assert(logSumExp(lPriors) == 0);
	assert(logProb.size() == oldClusters + m_aux);
	return logProb;
}

bitPool Sampler::compressNode(NodeID i) {
	assert(i >= 0);
	FeatureID f = featureCount();
	assert(f == featurePresent[i].size());

	bitPool result = 0;
	/**
	 * If there are too many features to store them in one field, return
	 */
	if (f >= 8*sizeof(bitPool)) return numeric_limits<bitPool>::max();//Used to trigger assertion faults if used
	assert(f < 8*sizeof(bitPool));
	if (f == 0) return result;
	for (int m = f-1; m >= 0; m--) {
		result = result << 1;
		result += featurePresent[i][m] ? 1 : 0;
	}
	for (FeatureID m = 0; m < f; m++) {
		assert(isSet(result, m) == featurePresent[i][m]);
	}

	return result;
}

vector<bitPool> Sampler::compressZ() {
	vector<bitPool> result(this->nodeCount);
	for (NodeID i = 0; i < nodeCount; i++) {
		result[i] = compressNode(i);
	}
	return result;
}

bool Sampler::isSet(bitPool compressed, FeatureID m) {
	return (compressed >> m) & 1;
}

void Sampler::purgeUnusedFeatures() {
	if (std::count(featurePopulation.begin(), featurePopulation.end(), 0) == 0) return;//no unused features
	/**
	 * Remove all features without inhabitants. This method is safe to call any time.
	 */
	FeatureMatrix featurecopy;
	ClusterMatrix clustercopy;
	//countClusters();

	featurecopy.resize(featurePresent.size());
	clustercopy.resize(clusterAssignment.size());

	WeightMatrices weightcopy;
	vector<int> featPopcopy;
	vector<vector<int> > subcopy;

	for (FeatureID m = 0; m < featurePopulation.size(); m++) {
		if (featurePopulation[m] > 1) {
			//I could hasten the copying process by using references instead of
			//copies. However, this isn't the bottleneck right now, focus on
			//likelihood calculation first.

			//copy TODO: replace with swap
			featPopcopy.push_back(featurePopulation[m]);
			weightcopy.push_back(weights[m]);
			subcopy.push_back(subClusterPopulation[m]);

			for (NodeID i = 0; i < featurePresent.size(); i++) {
				featurecopy[i].push_back(featurePresent[i][m]);
				clustercopy[i].push_back(clusterAssignment[i][m]);
			}
		}
	}
	featurePresent = featurecopy;
	clusterAssignment = clustercopy;
	weights = weightcopy;
	subClusterPopulation = subcopy;
	featurePopulation = featPopcopy;
	if (featurePresent.size() == 0) cout << "Warning: No Features left." << endl;
	this->compressedZ = compressZ();
}

void Sampler::clusterSanityCheck() {
	/**
	 * Includes a bunch of assertions to make sure all matrices have the right size
	 * and the population vectors are correct. Throws assertion failure if something is wrong,
	 * is side-effect free if everything is okay.
	 */
	assert(featurePopulation.size() == weights.size()); //featureCount
	assert(clusterAssignment.size() == featurePresent.size());//nodeCount
	assert(subClusterPopulation.size() == featurePopulation.size()); //featureCount

	vector<int> featPopcopy = featurePopulation;
	vector<vector<int> > subClusCopy = subClusterPopulation;

	/*
     * make sure a cluster assignment exists when a node has a feature
	 * and none exists when it doesn't
	 */
	for (NodeID i = 0; i < featurePresent.size(); i++) {
		assert(featurePresent[i].size() == featurePresent[0].size());
		for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
			if (featurePresent[i][m]) {
				assert(clusterAssignment[i][m] >= 0);
				assert(clusterAssignment[i][m] < subClusterPopulation[m].size());
				assert(subClusterPopulation[m][clusterAssignment[i][m]] > 0);
				featPopcopy[m]--;
				subClusCopy[m][clusterAssignment[i][m]]--;
			} else {
				assert(clusterAssignment[i][m] == -1);
			}
		}
	}

	for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
		assert(featPopcopy[m] == 0);
		//assert(subClusterPopulation[m].size() > 0);
		for (ClusterID n = 0; n < subClusterPopulation[m].size(); n++) {
			assert(subClusCopy[m][n] == 0);
			assert(subClusterPopulation[m].size() == weights[m][n].size());
		}
	}
}

void Sampler::purgeEmptyClusters(FeatureID m) {
	if (std::count(subClusterPopulation[m].begin(), subClusterPopulation[m].end(), 0) == 0) return;//no empty clusters
	unsigned erased = 0;
	vector<bool> skipped(subClusterPopulation[m].size(), false);
	int uc = 0;
	for (ClusterID c = 0; c < subClusterPopulation[m].size(); c++) {
		//count empty subclusters
		assert(subClusterPopulation[m][c] >= 0);
		if (subClusterPopulation[m][c] == 0) {
			erased++;
			skipped[c] = true;
			//cout << "Marked cluster " << c << " to be erased, bringing the total to " << erased << endl;
		}
		//shift cluster assignments
		for (NodeID i = 0; i < R.size(); i++) {
			if (clusterAssignment[i][m] == c) {
				assert(subClusterPopulation[m][c] >= 1);
				//cout << "Shifted node " << i << " from " << uc;
				clusterAssignment[i][m] -= erased;
				//cout << " to " << clusterAssignment[i][m] << endl;
				if (featurePresent[i][m]) {
					assert(clusterAssignment[i][m] >= 0);
				}
				assert(
						clusterAssignment[i][m]
								< subClusterPopulation[m].size() - erased);
			}
		}
		uc++;
	}
	//don't use erase, too much memory reallocation. Instead, build new matrix and copy only non-zero columns
	vector<int> clustercopy;
	vector < vector<double> > weightcopy;
	clustercopy.resize(subClusterPopulation[m].size() - erased);
	weightcopy.resize(subClusterPopulation[m].size() - erased);
	for (ClusterID c = 0; c < weightcopy.size(); c++) {
		weightcopy[c].resize(subClusterPopulation[m].size() - erased);
	}
	if (erased == subClusterPopulation[m].size()) {
		//this feature is empty - should have been removed already
		assert(featurePopulation[m] == 0);
	}
	unsigned copyindex = 0;
	for (ClusterID c = 0; c < subClusterPopulation[m].size(); c++) {
		if (!skipped[c]) {
			assert(copyindex < clustercopy.size());
			clustercopy[copyindex] = subClusterPopulation[m][c];
			unsigned innercopyindex = 0;
			for (ClusterID cl = 0; cl < subClusterPopulation[m].size(); cl++) {
				if (!skipped[cl]) {
					assert(copyindex < weightcopy.size());
					assert(innercopyindex < weightcopy[copyindex].size());
					weightcopy[copyindex][innercopyindex] = weights[m][c][cl];
					innercopyindex++;
				}
			}
			copyindex++;
		}
	}
	//cout << "Size of copy:" << clustercopy.size() << endl;
	//cout << ", copyindex:" << copyindex << endl;
	assert(copyindex == clustercopy.size());
	subClusterPopulation[m] = clustercopy;
	weights[m] = weightcopy;
}

void Sampler::purgeEmptyClusters() {
	/**
	 * Removes empty clusters. Should be called directly after purgeUnusedFeatures().
	 * If called directly after sampleStepZandC without an intermittent call of purgeUnusedFeatures(),
	 * an illegal chain state may occur since features without clusters are bad.
	 * The call order "sampleStepZandC; purgeEmptyClusters; sampleHyperGamma" will lead to a segfault.
	 */
	for (FeatureID m = 0; m < featurePopulation.size(); m++) {
		purgeEmptyClusters(m);
	}
	clusterSanityCheck();
}

void Sampler::sampleHyperAlpha() {
	/**
	 * Sample a new value for alpha (control parameter for the IBP) from the posterior and store it as internal variable.
	 * TODO: find out why the math is correct
	 */
	int n = R.size();
	double harm = 0;
	for (int i = 1; i <= n; i++) {
		harm += 1.0f/i;
	}
	double featureCount = featurePopulation.size();

	alpha=sampleGamma( featureCount+param.alpha_shape , param.alpha_scale / ( param.alpha_scale + harm ) );
}

double Sampler::getAlpha() {
	return alpha;
}

void Sampler::sampleHyperGamma(double precision) {
	/**
	 * Sample a new value for gamma (control parameter for CRP) and store it as internal variable.
	 * This function creates a histogramm to sample from and is slower than sampleHyperAlpha.
	 * precision is the step size for the histogramm. Runtime is proportional to 1/precision.
	 */
	CRPPDF logpdf(&featurePopulation, &subClusterPopulation, param);

	gamma = loginverseTransformSampling(&logpdf, 0, 20, precision);
}

double Sampler::getGamma() {
	return gamma;
}

void Sampler::sampleHyperBias() {
	/**
	 * Sample the bias added to the link strength from the posterior.
	 */
	if (param.conjugatebias) {
		vector<vector<double > > T;
		if (param.compressZ) T = Model::computeT(&featurePresent, &compressedZ, &clusterAssignment, &weights);
		else T = Model::computeT(&featurePresent, &clusterAssignment, &weights);
		double sum = 0;
		for (NodeID i = 0; i < nodeCount; i++) {
			for (NodeID j = 0; j < nodeCount; j++) {
				sum += R.hasEdge(i,j) - T[i][j];
			}
		}

		double repeatedVar = 1 / (1 / pow(sigma_bias, 2) + (double)(nodeCount*nodeCount) / pow(variance, 2));
		double mean = (mu_bias / (sigma_bias*sigma_bias) + sum / (variance*variance)) * repeatedVar;
		double directbias = sampleNormal(mean, repeatedVar);
		bias = directbias;
	} else {
		BiasPDF logpdf(&R, &featurePresent, &clusterAssignment, &weights, mu_bias, sigma_bias, variance, param);
		bias = loginverseTransformSampling(&logpdf, -10, 10, 0.1);
	}
}

double Sampler::getBias() {
	return bias;
}

void Sampler::sampleHyperVariance(double precision) {
	/**
	 * Sample the variance of the gaussian noise. Runtime is proportional to 1/precision.
	 */
	double sumOfSquares = 0;
	for (NodeID i = 0; i < nodeCount; i++) {
		for (NodeID j = 0; j < nodeCount; j++) {//TODO: replace this with j <= i
			vector<vector<double > > T = Model::computeT(&featurePresent, &clusterAssignment, &weights);
			double diff = R.hasEdge(i,j) - (T[i][j]+this->bias);
			sumOfSquares += diff*diff;
			//diff[j][i] = g->hasEdge(j,i) - (T[j][i]+s);
		}
	}
	//sample inverse gamma from param.variance_shape + n/2, param.variance_scale + sumOfSquares/2
	double variance_posterior_shape = nodeCount*nodeCount/2+param.variance_shape;
	double variance_posterior_scale = param.variance_scale + sumOfSquares/2;
	variance=1/sampleGamma(variance_posterior_shape , 1/(variance_posterior_scale));

	//VariancePDF varpdf(&R, &featurePresent, &clusterAssignment, &weights, bias, param);
	//variance = loginverseTransformSampling(&varpdf, precision, 10, precision);
	//if (param.plot) Plotting::plotPDF(&varpdf, precision, 10, precision);
}

double Sampler::getVariance() {
	return variance;
}

double Sampler::loginverseTransformSampling(ProbabilityDensityFunctor * pdf,
		double leftBoundary, double rightBoundary, double stepsize) {
	/**
	 * Creates a histogramm using the logarithm of the probability density function for greater numerical stability.
	 * The return value is sampled from the pdf and is returned in normal (non-log) form.
	 */
	vector<double> histogramm((rightBoundary-leftBoundary)/stepsize);

	for (int i = 0; i*stepsize + leftBoundary < rightBoundary; i++) {
		histogramm[i] = pdf->logpdf(i*stepsize+leftBoundary);
		assert(histogramm[i] == histogramm[i]);
	}
	int index = selectUniformLog(histogramm);
	return leftBoundary+stepsize*index;
}

double Sampler::sliceSample(ProbabilityDensityFunctor * pdf, double initial, int iterations, double w) {
	/**
	 * Use Slice Sampling (Neal, 2003) to sample from a distribution.
	 * The return value is the mean of all sampled values after the burnin period.
	 * From this follows that the 'iterations' parameter needs to be higher than the 'burnin' parameter
	 * w is the initial value for slice lengths.
	 */
	assert(pdf->logpdf(initial) > -numeric_limits<double>::max());
	vector<double> xhistory;
	double x = initial;
	int evalcount = 0;

	for (int i = 0; i < iterations; i++) {
		xhistory.push_back(x);
		double y, leftBorder, rightBorder;
		y = pdf->logpdf(x) - sampleGamma(1,1);
		evalcount++;
		leftBorder = x - w;
		rightBorder = x + w;

		//doubling
		while (pdf->logpdf(leftBorder) > y) {
			evalcount++;
			leftBorder += leftBorder -x;
		}
		while (pdf->logpdf(rightBorder) > y) {
			evalcount++;
			rightBorder += rightBorder -x;
		}
		evalcount += 2;

		//shrinking
		x = sampleUniform(leftBorder, rightBorder);
		while (pdf->logpdf(x) < y) {
			evalcount++;
			assert(x != xhistory.back());
			if (x < xhistory.back()) {
				leftBorder = x;
			} else {
				rightBorder = x;
			}
			x = sampleUniform(leftBorder, rightBorder);
		}
		evalcount++;
	}
	//cout << evalcount << " evaluations for " << iterations << " iterations." << endl;
	return xhistory.back();
}

void Sampler::permute(vector<NodeID> perm) {
	/**
	 * Permutes Z and C. W and the population vectors remain unchanged.
	 */
	assert(perm.size() == featurePresent.size());
	FeatureMatrix featurecopy(nodeCount);
	ClusterMatrix clustercopy(nodeCount);
	for (NodeID i = 0; i < perm.size(); i++) {
		featurecopy[i] = featurePresent[perm[i]];
		clustercopy[i] = clusterAssignment[perm[i]];
	}
	featurePresent = featurecopy;
	clusterAssignment = clustercopy;
	R.permute(perm);
//	model.permute(perm);
}

vector<int> Sampler::sampleNode(NodeID i, int m_aux, bool newFeaturesAllowed) {
	/**
	 * Sample only a single node and return the cluster assignments in each feature.
	 * The return vector has the same size as the total number of features and contains -1 for inactive features.
	 */
	assert(i < featurePresent.size());
	assert(!newFeaturesAllowed);//not yet implemented
	FeatureID featureCount = featurePresent[i].size();
	vector<int> result(featureCount);
	for (FeatureID m = 0; m < featureCount; m++) {
		step_z_im(m, i, m_aux, true);
		result[m] = clusterAssignment[i][m];
	}
	return result;
}

void Sampler::clearNode(NodeID i) {
	/**
	 * Removes node i from all features.
	 * It is advisable to call purgeEmptyFeatures afterwards
	 */
	FeatureID featureCount = featurePresent[i].size();
	for (FeatureID m = 0; m < featureCount; m++) {
		if (featurePresent[i][m]) {
			featurePopulation[m]--;
			subClusterPopulation[m][clusterAssignment[i][m]]--;
		}
		featurePresent[i][m] = false;
		clusterAssignment[i][m] = -1;
	}
}

void Sampler::step_z_im(FeatureID m, NodeID i, int m_aux, bool retryOld) {
	/**
	 * The core of the inference algorithm.
	 * Feature presence (z_{i,m}) and cluster assignment (c_{i,m}) of node i are sampled.
	 * A new cluster might be created if m_aux > 0.
	 */
	assert(subClusterPopulation[m].size() >= 0);
	assert(subClusterPopulation[m].size() == weights[m].size());
	if (featurePopulation[m] == 0) {//probability of choosing feature is zero, might as well skip
		assert(!featurePresent[i][m]);
		return;
	}
	/*
	 * P(z_{im} = 1|R) \propto n_{-im}/N \cdot P(R|z_{im} = 1, W_{-im}, C_{-im}, Z_{-im})
	 */

	//prepare matrices for turning feature off
	int previousCluster = -1;
	if (featurePresent[i][m]) {
		previousCluster = clusterAssignment[i][m];
		featurePopulation[m]--; //likelihood calculation later needs n_{im}
		subClusterPopulation[m][previousCluster]--;
		featurePresent[i][m] = 0;
		clusterAssignment[i][m] = -1;
		compressedZ[i] = compressNode(i);
	}
	double loglikelihood_reject;
	if (param.compressZ) loglikelihood_reject = Model::logLikelihood(&R, &featurePresent, &compressedZ,
				&clusterAssignment, &weights, bias, variance);
	else loglikelihood_reject = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);
	double logprior_reject = log(1-((double) featurePopulation[m] / R.size()));
	double logprior_accept = log(featurePopulation[m]) - log(R.size());

	/**
	 * case when feature is activated
	 */
	featurePresent[i][m] = true;
	compressedZ[i] = compressNode(i);
	vector<double> logProb = decideClusterWithAux(i, m, m_aux, retryOld);
	ClusterID assignment = selectUniformLog(logProb);
	clusterAssignment[i][m] = assignment;
	subClusterPopulation[m][assignment]++;
	featurePopulation[m]++;

	//get likelihood and figure out whether feature is chosen
	double loglikelihood_accept = Util::logSumExp(logProb);
	assert(loglikelihood_accept < numeric_limits<double>::max());
	assert(loglikelihood_accept > -numeric_limits<double>::max());

	//adding logpriors - TODO: rename variables
	double logprob_reject = logprior_reject + loglikelihood_reject;
	double logprob_accept = logprior_accept + loglikelihood_accept;

	//now we have both likelihoods in logform, subtract maximum from both
	// to improve numeric stability, subtraction is better than division.
	double maximum = max(logprob_reject, logprob_accept);
	double prob_accept = exp(logprob_accept-maximum);
	double prob_reject = exp(logprob_reject-maximum);

	//now normalize these likelihoods
	double prob = normalize(prob_accept, prob_reject);//This is odd: The likelihood is sometimes 1!

	bool accepted = (sampleUniform() < prob);
	if (!accepted) {
		featurePresent[i][m] = false;
		clusterAssignment[i][m] = -1;
		featurePopulation[m]--; //likelihood calculation later needs n_{im}
		subClusterPopulation[m][assignment]--;
		compressedZ[i] = compressNode(i);
		purgeEmptyClusters(m);
		ll = loglikelihood_reject;
	} else if (m_aux > 0){
		purgeEmptyClusters(m);
		ll = loglikelihood_accept;
	}
}

void Sampler::sampleStepZandC() {
	/**
	 * Samples both feature matrix and cluster matrix, deciding participation of nodes in present features and creating new ones.
	 * m_aux is the parameter for the Auxiliary Variable algorithm, used in decideClusterWithAux
	 */

	assert(nodeCount > 0);
	assert(featurePresent.size() == nodeCount);
	for (NodeID i = 0; i < R.size(); i++) {
		/* We assume i is the last object to be added.
		 * We can do that because of the interchangeability within CRP and IBP.
		 */
		int m_aux = param.m_aux;
		int tlc = 0;
		for (FeatureID m = 0; m < weights.size(); m++) {
			tlc += pow((double)weights[m].size(), 2);
		}

		if (param.TLClimitActive && tlc > param.maxTLC) m_aux = 0;
		for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
			step_z_im(m, i, m_aux, param.retryOld);
		}
		/*
		 * after handling the already present features, add Poisson(alpha/N) new ones
		 */

		FeatureID featureCount = featurePresent[0].size();
		int newfeatures = samplePoisson((double) alpha / (double)R.size());
		if (newfeatures == 0 || (param.TLClimitActive && tlc > param.maxTLC)) continue;
		double logLikelihood_reject;
		if (param.compressZ) logLikelihood_reject = Model::logLikelihood(&R, &featurePresent, &compressedZ, &clusterAssignment, &weights, bias, variance);
		else logLikelihood_reject = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);

		//enlarge arrays
		weights.resize(featureCount + newfeatures);
		featurePopulation.resize(featureCount+newfeatures, 0);
		subClusterPopulation.resize(featureCount + newfeatures);

		for (NodeID l = 0; l < nodeCount; l++) {
			clusterAssignment[l].resize(featureCount+newfeatures, -1);
			featurePresent[l].resize(featureCount+newfeatures, false);
		}

		bitPool start = 0;
		for (FeatureID m = featureCount;  m < featureCount + newfeatures; m++) {
			subClusterPopulation[m].push_back(1);//only one subcluster each with i as only inhabitant
			assert(subClusterPopulation[m].size() == 1);
			featurePresent[i][m] = true;
			featurePopulation[m] = 1;
			clusterAssignment[i][m] = 0;
			weights[m].resize(1);
			weights[m][0].resize(1);
			weights[m][0][0] = sampleNormal(mu_w, sigma_w);
			start = start | (1 << m);
		}
		compressedZ[i] |= start;

		//TODO: use loglikelihoodNewCluster
		//accept or not accept them
		double logLikelihood_accept;
		if (param.compressZ) logLikelihood_accept = Model::logLikelihood(&R, &featurePresent, &compressedZ, &clusterAssignment, &weights, bias, variance);
		else logLikelihood_accept = Model::logLikelihood(&R, &featurePresent, &clusterAssignment, &weights, bias, variance);
		double c = max(logLikelihood_accept, logLikelihood_reject);
		double standardlikelihood = normalize(exp(logLikelihood_accept-c), exp(logLikelihood_reject-c));

		/*
		 * now follows the Metropolis-Hastings step: We always accept new features if they improve the solution. Even if they don't they are sometimes accepted.
		 */
		double likelihood;
		if (param.classicNewFeatures) {
			likelihood = min((double)1,exp(logLikelihood_accept - logLikelihood_reject));
		} else {
			likelihood = standardlikelihood;
		}

		assert(likelihood >= 0 && likelihood <= 1);
		if (sampleUniform() > likelihood) {
				for (NodeID k = 0; k < nodeCount; k++) {
					featurePresent[k].resize(featureCount);
					clusterAssignment[k].resize(featureCount);
					featurePopulation.resize(featureCount);
					subClusterPopulation.resize(featureCount);
				}
				weights.resize(featureCount);
				compressedZ[i] &= ~start;
				//cout << "New feature rejected." << endl;
		} else {
				//cout << "Featurecount: " << featureCount << " -> " << featureCount + newfeatures << endl;
		}
	}

	int totalFeatureUsage;
	for (FeatureID m = 0; m < featurePopulation.size(); m++) {
		totalFeatureUsage += featurePopulation[m];
	}
}

FeatureMatrix Sampler::getZ() {
	return featurePresent;
}

ClusterMatrix Sampler::getC() {
	return clusterAssignment;
}

int Sampler::featureCount() {
	return featurePopulation.size();
}

Settings Sampler::getSettings() {
	return param;
}

int Sampler::totalClusterCount() {
	/**
	 * Returns total number of clusters, summed over all features.
	 */
	int result = 0;
	if (featurePresent.size() == 0) return 0;
	FeatureID fCount = featurePresent[0].size();
	for (FeatureID f = 0; f < fCount; f++) {
		int localClusters = subClusterPopulation[f].size();
		assert(localClusters > 0); //features without clusters should have been purged already.
		result += localClusters;
	}
	return result;
}



void Sampler::saveChainState() {
	Model model(featurePresent, clusterAssignment, weights, alpha, gamma, bias, variance);
	chain.push_back(model);
}

Model Sampler::getModel() {
	Model model(featurePresent, clusterAssignment, weights, alpha, gamma, bias, variance);
	return model;
}

vector<std::pair<ClusterMatrix, int> > Sampler::generateClusterStatistics() {
	std::map<ClusterMatrix, int> frequencyMap;
	for (int i = 0; i < param.iterations; i++) {
		sampleIter();
		ClusterMatrix sampleC = getC();
		FeatureID featureCount = sampleC[0].size();
		vector<FeatureID> perm = Util::dummyPermutation(featureCount);
		std::sort(perm.begin(), perm.end(), [&sampleC](int k, int l){return Model::comp(sampleC, k,l);});
		for (NodeID i = 0; i < R.size(); i++) {
			sampleC[i] = Util::apply(perm, sampleC[i]);
		}
		//should map to representative here
		if (frequencyMap.find(sampleC) == frequencyMap.end())
		{
			frequencyMap[sampleC] = 1;
		} else {
			frequencyMap[sampleC]++;
		}
		if (i % 10 == 0) {
			cout << featureCount;
			cout << ".";
			cout.flush();
		}
		if (i % 500 == 0) cout << frequencyMap.size() << endl;
	}
	vector<std::pair<ClusterMatrix, int> > result;
	for (std::pair<ClusterMatrix, int> sample : frequencyMap) {
		result.push_back(sample);
	}
	std::sort(result.begin(), result.end(), [](std::pair<ClusterMatrix, int> a, std::pair<ClusterMatrix, int> b){return a.second > b.second;});
	return result;
}

vector<Model> Sampler::getChain() {
	return chain;
}

Model Sampler::getLastChainlink() {
	return chain.back();
}

void Sampler::sampleStepW() {
	/**
	 * Resample all weights.
	 * Runtime is proportional to \sum_{f \in F} (c_f)^2 / precision
	 */

	for (FeatureID m = 0; m < featurePopulation.size(); m++) {
		for (ClusterID i = 0; i < weights[m].size(); i++) {
			vector<NodeID> c1pop;
			for (NodeID r = 0; r < nodeCount; r++) {
				if (clusterAssignment[r][m] == i) {
					c1pop.push_back(r);
				}
			}
			for (ClusterID j = 0; j <= i; j++) {
				vector<NodeID> c2pop;
				for (NodeID r = 0; r < nodeCount; r++) {
					if (clusterAssignment[r][m] == j) {
						c2pop.push_back(r);
					}
				}
				weights[m][i][j] = 0;
				weights[m][j][i] = 0;

				double sum = 0;

				for (unsigned k = 0; k < c1pop.size(); k++) {
					for (unsigned l = 0; l < c2pop.size(); l++) {
						double diff =  R.hasEdge(c1pop.at(k),c2pop.at(l)) - Model::getLinkMean(c1pop.at(k), c2pop.at(l), &featurePresent, &clusterAssignment, &weights, bias);
						sum += diff;
						if (i != j) sum += diff;//we assume a symmetric matrix and iterate only over one half. The missing duplicate is added here
					}
				}
				double lc = c1pop.size()*c2pop.size();
				if (i != j) lc *= 2;

				double repeatedVar = 1 / (1 / pow(sigma_w, 2) + (double)(lc) / pow(variance, 2));
				double mean = (mu_w / (sigma_w*sigma_w) + sum / (variance*variance)) * repeatedVar;

				if (param.conjugateWeights) {
					double directweight = sampleNormal(mean, repeatedVar);
					weights[m][i][j] = directweight;
				} else {
					double precision = 0.1;
					WeightPDF pdf(&R, &featurePresent, &clusterAssignment, &weights, m, i, j, bias, variance, param);
					double sampledweight = loginverseTransformSampling(&pdf, -10, 10, precision);
					weights[m][i][j] = sampledweight;
				}

				weights[m][j][i] = weights[m][i][j];
			}
		}
	}
}

WeightMatrices Sampler::getW() {
	return weights;
}

void Sampler::countClusters() {
	/**
	 * count feature and subcluster popularity, since the CRP and IBP probabilities depend on them.
	 */
	for (FeatureID m = 0; m < subClusterPopulation.size(); m++) {
		featurePopulation[m] = 0;
		for (ClusterID c = 0; c < subClusterPopulation[m].size(); c++) {
			subClusterPopulation[m][c] = 0;
		}
	}

	for (NodeID i = 0; i < featurePresent.size(); i++) {
		for (FeatureID m = 0; m < featurePresent[0].size(); m++) {
			if (featurePresent[i][m]) {
				featurePopulation[m]++;
				assert(clusterAssignment[i][m] >= 0);
				unsigned oldsize = subClusterPopulation[m].size();
				if (oldsize <= (unsigned) clusterAssignment[i][m]) {
					subClusterPopulation[m].resize(clusterAssignment[i][m] + 1,	0);
					assert(subClusterPopulation[m].size() > oldsize);
				}
				subClusterPopulation[m][clusterAssignment[i][m]]++;
			}
		}
	}
	//assert every feature has at least one cluster
	for (FeatureID m = 0; m < subClusterPopulation.size(); m++) {
		assert(subClusterPopulation[m].size() > 0);
	}
}

ClusterID Sampler::countEmptyClusters(FeatureID m) {
	ClusterID result = 0;
	for (ClusterID c = 0; c < subClusterPopulation[m].size(); c++) {
		if (subClusterPopulation[m][c] == 0) result++;
	}
	return result;
}

}//end namespace core
}//ende namepsace CILA
