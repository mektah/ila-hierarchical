#!/bin/bash
scons
MATLAB_ROOT="/home/moritz/Matlab/R2018a"
cd src
$MATLAB_ROOT/bin/mex -v GCC='g++' sampleAndCollapse.cpp Model.cpp Graph.cpp Settings.cpp sampler.cpp Util.cpp Plotting.cpp generative.cpp TestChamber.cpp Aggregation.cpp ModelIO.cpp -L/usr/lib/x86_64-linux-gnu/ -I../cereal
$MATLAB_ROOT/bin/mex -v GCC='g++' getDefaultSettings.cpp Settings.cpp -I../cereal
cd ..
python3 setup.py build_ext --inplace
python3 CILA_test.py
