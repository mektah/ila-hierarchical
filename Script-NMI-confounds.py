from CILA import *
import math
import CILA
import pandas
from scipy import linalg
from sklearn import metrics
from mpi4py import MPI

from cilaUtil import getNumbersOfSavedFiles, getPrefix, removeLinearConfounds, decodeOptionsFromPID

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# get the options from rank number. This could also be done with bit shifting
deconfound, euclid, normalize, singlefeature, number = decodeOptionsFromPID(rank)

seed = number

inputpath = os.path.expanduser("~/Diplom/ila-hierarchical")
outputpath = '.'

inputfilename='inputdata.csv'
data = pandas.read_csv(os.path.join(inputpath, inputfilename), delimiter=',', header=None)

ground_truth = pandas.read_csv(os.path.join(inputpath, 'labels.csv'), header=None)
diagnosis = ground_truth[1]

if normalize:
    data = data.apply(lambda x : [(elem-x.mean()) / x.var() for elem in x])

if deconfound:
    data = removeLinearConfounds(data, os.path.join(inputpath, 'confounds.csv'))

CILA.setSeed(seed)

settings = CILA.getSettingsDict()
settings['normalize_input'] = normalize

R = CILA.toSquareMatrix(data.as_matrix(), euclid, normalize)

i = 0
settings['mu_weights'] = 0
settings['sigma_weights'] = 10
settings['sigma_bias'] = 10
settings['alpha_scale'] = 0.1
settings['mu_bias'] = 0
if singlefeature:
    settings['crp_only'] = True
    settings['sample_Z'] = False
iterations = 10**7
settings['thin'] = 1000
sampler = Sampler(R, settings)

# see whether we should continue a previous run
prefix = getPrefix(deconfound, euclid, normalize, singlefeature, number, CILA.getVersionString())
numbers = getNumbersOfSavedFiles(outputpath, prefix)

saveStatesAfter = -1

if len(numbers) > 0:
    maxNumber = max(numbers)
    if len(numbers) != maxNumber+1:
        print("List of previous saved samples probably not continuous.")
    startModel = loadModel(os.path.join(outputpath, prefix+'-'+str(maxNumber)+'.model'))
    sampler = Sampler(R, settings, startModel)
    i = maxNumber * settings['thin']
else:
    CILA.saveSettings(prefix+'.settings', settings, True)

print("Starting sampler for " + prefix + ", starting with iteration " + str(i))
if i > 0:
    # the initial state was read from a sample, saving it again would create a duplicate
    saveStatesAfter = i

# run sampler
while i < iterations:
    sampler.sampleIter()
    if i % settings['thin'] == 0:
        model = sampler.getModel()
        print(i, model.loglikelihood(R), model.getFeatureCount(), flush=True)
        if i > saveStatesAfter:
            filename = prefix+'-'+str(int(i / settings['thin']))+'.model'
            CILA.saveModel(filename, model, True)
        if model.getFeatureCount() > 0:
            clusterMatrix = model.getClusterMatrix()
            clusters = [row[0] for row in clusterMatrix]
            minCluster = min(clusters)
            normalizedClusters = [i-minCluster for i in clusters]
            assert(len(normalizedClusters) == len(diagnosis))
            print("NMI:", metrics.mutual_info_score(diagnosis, normalizedClusters))
    i += 1
