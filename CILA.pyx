"""
A Python interface for the CILA code.
CILA stands for 'Continuous Infinite Latent Attributes' and is a non-paramteric hierarchical Bayesian model for graph data.
Based on work by Palla et al., appearing in "An Infinite Latent Attribute Model for Network Data".
Nodes have some of infinitely many features. In each feature a node takes part in, it is assigned a cluster.
The probability of an edge between nodes a and b depends on the features a and b have in common and the weights between the clusters of a and b in each feature.

This approach differs from their work by introducing edge weights and using a normal distribution instead of a logistic link function.

This module uses Markov Chain Monte Carlo sampling to approximate the posterior distribution of the model.
"""

# C++ operators
from cython.operator import dereference

#import first stuff
from libcpp cimport bool
from libcpp.vector cimport vector
from libcpp.pair cimport pair
from libcpp.string cimport string

# type imports
from libc.stdint cimport uint64_t
from libc.stdint cimport int64_t
from libc.stdint cimport uint32_t
from libc.stdint cimport int32_t

# Typedefs
ctypedef uint32_t count
ctypedef uint32_t index
ctypedef uint32_t feature
ctypedef index node
ctypedef index bitPool
ctypedef double edgetype
ctypedef vector[vector[vector[edgetype]]] WeightMatrices
ctypedef vector[vector[int]] ClusterMatrix
ctypedef vector[vector[bool]] FeatureMatrix

# python libraries for nicer wrappers
import os

cdef extern from "src/Model.h" namespace "CILA::core::Model":
	_Model generateModel(count n, double alpha, double gamma, double s, double sigma, double mu_w, double sigma_w) except +
	_Model emptyModel(count n, double alpha, double gamma, double s) except +
	_Model exampleModel() except +
	_Model singleFullFeatureModel(_Settings settings) except +
	cdef cppclass _Model "CILA::core::Model":
		_Model() except +
		_Model(_Model copy) except + #here we rely on the automatically built copy-constructor.
		_Model(vector[vector[bool]] f, vector[vector[int]] c, vector[vector[vector[edgetype]]] w, double alpha, double gamma, double s, double sigma) except +
		bool isConstructed()
		count getNodeCount()
		count getFeatureCount()
		FeatureMatrix getFeatureMatrix()
		ClusterMatrix getClusterMatrix()
		WeightMatrices getWeights()
		void permute(vector[node] permutation) except +
		double logLikelihood(_Graph * g) except +
		double logPrior() except +
		double getLogLikelihoodContribution(_Graph * g, index m) except +
		_Graph sampleGraphInstance(double variance) except +

cdef class Features:
	"""
	A model instance. Contains feature matrix, cluster matrix and weight matrices.
	Objects are mutable and stateful.
	"""
	cdef _Model* _this

	def __cinit__(self, f=None, c=None, w=None,alpha=1, gamma=1, s=0, sigma=1):
		if f == None:
			self._this = new _Model()
		else:
			if not isinstance(f, list) or not isinstance(c, list):
				raise ValueError("Both f and c must be matrices of the same dimensions")
			if not len(f) == len(c):
				raise ValueError("Both f and c must have the same length")
			if len(f) > 0 and not len(f[0]) == len(c[0]):
				raise ValueError("Both f and c must be matrices of the same dimensions")
			if len(f) > 0 and not len(f[0]) == len(w):
				raise ValueError("Weight matrix vector must have one matrix for each column in f")
			self._this = new _Model(f, c, w, alpha, gamma, s, sigma)

	def __dealloc__(self):
		del self._this

	def __str__(self):
		return "Model, n: " + str(self._this.getNodeCount()) + ", fCount: " + str(self._this.getFeatureCount())

	@staticmethod
	def generateModel(n, alpha = 1, gamma = 1, mu_w = 1, sigma_w = 1, mu_s = 0, sigma_s = 1):
		"""
		Accepts n, alpha, gamma, mu_w, sigma_w, mu_s, sigma_s.
		Returns a model instance based on the distribution parameters.
		"""
		result = Features()
		del result._this
		result._this = new _Model(generateModel(n, alpha, gamma, mu_w, sigma_w, mu_s, sigma_s))
		return result

	@staticmethod
	def emptyModel(n, alpha, gamma, s):
		"""
		Return an instance without any features and clusters
		"""
		result = Features()
		del result._this
		result._this = new _Model(emptyModel(n, alpha, gamma, s))
		return result

	@staticmethod
	def exampleModel():
		"""
		Return a hard-coded example
		"""
		result = Features()
		del result._this
		result._this = new _Model(exampleModel())
		return result

	@staticmethod
	def singleFullFeatureModel(settings):
		"""
		Create a model instance consisting of a single feature in which all nodes participate
		"""
		cdef _Settings parsedSettings = _getSettingsObject(settings)
		result = Features()
		del result._this
		result._this = new _Model(singleFullFeatureModel(parsedSettings))
		return result

	def isConstructed(self):
		"""
		Returns True iff this model instance has already been filled with valid values for the parameters and matrices.
		Don't use it if it is not constructed.
		"""
		return self._this.isConstructed()

	def getNodeCount(self):
		"""
		Number of nodes
		"""
		return self._this.getNodeCount()

	def getFeatureCount(self):
		return self._this.getFeatureCount()

	def getFeatureMatrix(self):
		"""
		Returns the feature matrix

		Parameters
		----------
		None

		Returns
		-------
		vector[vector[bool]]
			feature matrix
		"""
		return self._this.getFeatureMatrix()

	def getClusterMatrix(self):
		"""
		Returns the cluster matrix

		Parameters
		----------
		None

		Returns
		-------
		vector[vector[int]]
			cluster matrix
		"""
		return self._this.getClusterMatrix()

	def getWeights(self):
		"""
		Returns a vector of weight matrices, one matrix for each feature

		Parameters
		----------
		None

		Returns
		-------
		vector[vector[vector[double]]]
			Weight matrices

		"""
		return self._this.getWeights()

	def permute(self, permutation):
		"""
		Accepts a node permutation and permutes all matrices accordingly.
		"""
		self._this.permute(permutation)

	def logprior(self):
		"""
		Returns the logPrior of the model.
		"""
		return self._this.logPrior()

	def loglikelihood(self, vector[vector[edgetype]] g):
		"""
		Accepts a graph and returns the loglikelihood

		Parameters
		----------
		g : vector[vector[double]]

		Returns
		-------
		double
			LogLikelihood of g under this model.

		"""
		graphpointer = new _Graph(g)
		return self._this.logLikelihood(graphpointer)

	def loglikelihoodContribution(self, vector[vector[edgetype]] g, index m):
		"""
		Computes the contribution of feature m to the loglikelihood of graph g under the model.

		Parameters
		----------
		g : vector[vector[double]]

		m : index

		Returns
		-------
		double
			difference to Loglikelihood if feature m was removed
		"""
		graphpointer = new _Graph(g)
		return self._this.getLogLikelihoodContribution(graphpointer, m)


	def sampleGraphInstance(self, variance=0):
		"""
		Returns a random graph, sampled according to the distribution of this instance

		Parameters
		----------
		variance : double

		Returns
		-------
		vector[vector[double]]
			Our sampled Graph

		"""
		return self._this.sampleGraphInstance(variance).edges

cdef extern from "src/Graph.h":
	cdef cppclass _Graph "CILA::core::Graph":
		_Graph() except +
		_Graph(vector[vector[edgetype]] edges) except +
		void permute(vector[node] permutation) except +
		vector[vector[edgetype]] edges

cdef extern from "src/Settings.h" namespace "CILA::core::Settings":
	_Settings getDefaultSettings() except +
	cdef cppclass _Settings "CILA::core::Settings":
		_Settings() except +
		bool isValid()
		string getVersionString()
		int iterations
		count nodeCount
		double mu_bias
		double sigma_bias
		double mu_weights
		double sigma_weights
		double gamma_scale
		double gamma_shape
		double alpha_scale
		double alpha_shape
		double variance_scale
		double variance_shape
		double init_alpha
		double init_gamma
		double init_bias
		double init_variance
		bool crp_only
		bool sample_alpha
		bool sample_gamma
		bool sample_Z
		bool sample_C
		bool sample_weights
		bool sample_bias
		bool sample_variance
		bool saveStates
		bool normalize_input
		double m_aux
		double cthreshold
		double fthreshold
		double sthreshold
		int burnin
		int thin

cdef settingsObjectToDict(_Settings wrappedSettings):
	result = {}
	result['iterations'] = wrappedSettings.iterations
	result['nodeCount'] = wrappedSettings.nodeCount
	result['mu_bias'] = wrappedSettings.mu_bias
	result['sigma_bias'] = wrappedSettings.sigma_bias
	result['mu_weights'] = wrappedSettings.mu_weights
	result['sigma_weights'] = wrappedSettings.sigma_weights
	result['gamma_scale'] = wrappedSettings.gamma_scale
	result['gamma_shape'] = wrappedSettings.gamma_shape
	result['alpha_shape'] = wrappedSettings.alpha_shape
	result['alpha_scale'] = wrappedSettings.alpha_scale
	result['variance_shape'] = wrappedSettings.variance_shape
	result['variance_scale'] = wrappedSettings.variance_scale
	result['init_alpha'] = wrappedSettings.init_alpha
	result['init_gamma'] = wrappedSettings.init_gamma
	result['init_bias'] = wrappedSettings.init_bias
	result['init_variance'] = wrappedSettings.init_variance
	result['crp_only'] = wrappedSettings.crp_only
	result['sample_alpha'] = wrappedSettings.sample_alpha
	result['sample_gamma'] = wrappedSettings.sample_gamma
	result['sample_Z'] = wrappedSettings.sample_Z
	result['sample_C'] = wrappedSettings.sample_C
	result['sample_weights'] = wrappedSettings.sample_weights
	result['sample_bias'] = wrappedSettings.sample_bias
	result['sample_variance'] = wrappedSettings.sample_variance
	result['normalize_input'] = wrappedSettings.normalize_input
	result['m_aux'] = wrappedSettings.m_aux
	result['cthreshold'] = wrappedSettings.cthreshold
	result['fthreshold'] = wrappedSettings.fthreshold
	result['sthreshold'] = wrappedSettings.sthreshold
	result['burnin'] = wrappedSettings.burnin
	result['thin'] = wrappedSettings.thin

	return result

def getSettingsDict():
	"""
	Returns a python dictionary with the default settings of the sampler.

	Parameters
	----------

	Sampler behaviour:
		iterations : bool
			Used when several iterations are performed at once, for example in generateClusterStatistics

		sample_alpha : bool
			Whether model parameter alpha is sampled. Defaults to True

		sample_gamma : bool
			Whether model parameter gamma is sampled. Defaults to True

		sample_Z : bool
			Whether feature matrix is sampled. Defaults to True

		sample_C : bool
			Whether cluster matrix is sampled. Defaults to True

		sample_weights : bool
			Whether weights are sampled. Defaults to True

		sample_bias : bool
			Whether scalar bias is sampled. Defaults to True

		sample_variance : bool
			Whether variance of random noise is sampled. Defaults to False

	Hyperparameters:
		alpha_shape : double
		alpha_scale : double
			The model parameter alpha governs the Indian Buffet Process responsible for feature selection.
			The prior of alpha is modeled as a gamma distribution, determined by shape and scale parameters.

		gamma_shape : double
		gamma_scale : double
			The model parameter gamma governs the Chinese Restaurant Process responsible for cluster assignment.
			The prior of gamma is modeled as a gamma distribution, determined by shape and scale parameters.


		mu_weights : double
		sigma_weights : double
			The weights modeling connections between clusters have a Gaussian prior and are determined by mu and sigma.

		mu_bias : double
		sigma_bias : double
			The bias, added to the estimated link strength, has a Gaussian prior 

		variance_shape : double
		variance_scale : double
			The variance of the gaussian noise is itself modeled as a gamma distribution, determined by shape and scale.

	Initial Parameter Values:
		init_alpha : double
			Initial default value of alpha, governing the Indian Buffet Process. Only applies if no input model is given.
		init_gamma : double
			Initial default value of gamma, governing the Chinese Restaurant Process. Only applies if no input model is given.
		init_bias : double
			Initial default value of the added bias. Only applies if no input model is given.
		init_variance : double
			Initial default value of the added bias. Only applies if no input model is given.

	Aggregation Behaviour:
		cthreshold : double
			Threshold for cluster aggregation. If two data points are in the same cluster in more than this ratio of samples,
			 they are assigned to the same cluster in the aggregation.

		double fthreshold : double
			Threshold for feature presence.
			If a data point x participates in feature y in more than this ratio of samples,
			 it participates in feature y in the aggregation.

	"""
	return settingsObjectToDict(getDefaultSettings())

def getVersionString():
	cdef _Settings result = getDefaultSettings()
	return result.getVersionString().decode("utf-8")


cdef _Settings _getSettingsObject(settings={}):
	"""
	Transforms a python dictionary into a _Settings object. When called without argument, default settings are returned.
	"""

	cdef _Settings result = getDefaultSettings()
	result.saveStates = False#this is different from the C++ default
	if 'iterations' in settings:
		result.iterations = settings['iterations']
	if 'nodeCount' in settings:
		result.nodeCount = settings['nodeCount']
	if 'mu_bias' in settings:
		result.mu_bias = settings['mu_bias']
	if 'sigma_bias' in settings:
		result.sigma_bias = settings['sigma_bias']
	if 'mu_weights' in settings:
		result.mu_weights = settings['mu_weights']
	if 'sigma_weights' in settings:
		result.sigma_weights = settings['sigma_weights']
	if 'gamma_scale' in settings:
		result.gamma_scale = settings['gamma_scale']
	if 'gamma_shape' in settings:
		result.gamma_shape = settings['gamma_shape']
	if 'alpha_shape' in settings:
		result.alpha_shape = settings['alpha_shape']
	if 'alpha_scale' in settings:
		result.alpha_scale = settings['alpha_scale']
	if 'variance_shape' in settings:
		result.variance_shape = settings['variance_shape']
	if 'variance_scale' in settings:
		result.variance_scale = settings['variance_scale']
	if 'init_alpha' in settings:
		result.init_alpha = settings['init_alpha']
	if 'init_gamma' in settings:
		result.init_gamma = settings['init_gamma']
	if 'init_bias' in settings:
		result.init_bias = settings['init_bias']
	if 'init_variance' in settings:
		result.init_variance = settings['init_variance']
	if 'crp_only' in settings:
		result.crp_only = settings['crp_only']
	if 'sample_alpha'  in settings:
		result.sample_alpha = settings['sample_alpha']
	if 'sample_gamma' in settings:
		result.sample_gamma = settings['sample_gamma']
	if 'sample_Z' in settings:
		result.sample_Z = settings['sample_Z']
	if 'sample_C' in settings:
		result.sample_C = settings['sample_C']
	if 'sample_weights' in settings:
		result.sample_weights = settings['sample_weights']
	if 'sample_bias' in settings:
		result.sample_bias = settings['sample_bias']
	if 'sample_variance' in settings:
		result.sample_variance = settings['sample_variance']
	if 'normalize_input' in settings:
		result.normalize_input = settings['normalize_input']
	if 'm_aux' in settings:
		result.m_aux = settings['m_aux']
	if 'cthreshold' in settings:
		result.cthreshold = settings['cthreshold']
	if 'fthreshold' in settings:
		result.fthreshold = settings['fthreshold']
	if 'sthreshold' in settings:
		result.sthreshold = settings['sthreshold']
	if 'burnin' in settings:
		result.burnin = settings['burnin']
	if 'thin' in settings:
		result.thin = settings['thin']
	return result

cdef extern from "src/sampler.h":
	cdef cppclass _Sampler "CILA::core::Sampler":
		_Sampler() except +
		_Sampler(_Graph g) except +
		_Sampler(_Graph g, _Settings settings) except +
		_Sampler(_Graph g, _Model f, _Settings settings) except +
		_Sampler(_Graph g, vector[vector[int]] c, vector[vector[vector[double]]] w, _Settings settings) except +
		void sampleIter() nogil except +
		vector[vector[bool]] getZ()
		vector[vector[int]] getC()
		vector[vector[vector[edgetype]]] getW()
		double getAlpha()
		double getGamma()
		double getBias()
		double getVariance()
		void permute(vector[node] permutation)
		count featureCount()
		count totalClusterCount()
		vector[pair[vector[vector[int]],int]] generateClusterStatistics()


cdef class Sampler:
	cdef _Sampler* _this

	def __cinit__(self, edges, settings={}, Features initialModel=None):
		if len(edges) <= 0 or len(edges[0]) != len(edges):
			raise ValueError("Input data must be quadratic matrix")
		cdef _Graph graph = _Graph(edges)
		cdef _Settings parsedSettings = _getSettingsObject(settings)
		if not parsedSettings.isValid():
			raise ValueError("Settings must be consistent")
		if isinstance(initialModel, Features):
			self._this = new _Sampler(graph, dereference(initialModel._this), parsedSettings)
		else:
			self._this = new _Sampler(graph, parsedSettings)

	def __dealloc__(self):
		del self._this

	def sampleIter(self):
		"""
		Run one iteration of MCMC sampler.
		"""
		self._this.sampleIter()

	def getModel(self):
		"""
		Get the current state of the markov chain.
		"""
		result = Features()
		del result._this
		result._this = new _Model(self._this.getZ(), self._this.getC(), self._this.getW(), self._this.getAlpha(), self._this.getGamma(), self._this.getBias(), self._this.getVariance())
		return result

	def generateClusterStatistics(self, ):
		"""
		Runs sampler for as many iterations as have been set in the settings.
		Counts how often each cluster matrix appeared. Warning: can take long!
		"""
		return self._this.generateClusterStatistics()

cdef extern from "src/Aggregation.h" namespace "CILA::Aggregation":
	_Aggregation aggregate(_Aggregation a, double weightA, _Aggregation b, double weightB) except +
	_Aggregation aggregate(vector[_Model] chain) except +
	cdef cppclass _Aggregation "CILA::Aggregation":
		_Aggregation() except +
		_Aggregation(_Model base) except +
		_Aggregation(_Aggregation copy) except + #here we rely on the automatically built copy-constructor.
		_Model collapse(_Settings settings) except +
		vector[vector[vector[double]]] getCommonClusteringRatios() except +

cdef class Aggregation:
	"""
	The Aggregation class is used to aggregate multiple samples into a point estimate.
	Consensus clustering is used to obtain a reasonably representative clustering.
	"""
	cdef _Aggregation* _this

	def __cinit__(self, Features modelinstance):
		cdef _Model* modelpointer = modelinstance._this
		self._this = new _Aggregation(modelpointer[0])

	def __dealloc__(self):
		del self._this

	@staticmethod
	def aggregate(Aggregation a, double weightA, Aggregation b, double weightB):
		"""
		Returns an Aggregation object, the weighted average of the inputs

		Parameters
		----------
		a : Aggregation

		weightA : double

		b : Aggregation

		weightB : double

		Returns
		-------
		Aggregation
			weighted average of a and b
		"""
		cdef _Aggregation ca = dereference(a._this)
		cdef _Aggregation cb = dereference(b._this)
		cdef _Aggregation temp = aggregate(ca, weightA, cb, weightB)
		cdef Aggregation result = Aggregation(Features())
		result._this = new _Aggregation(temp)
		return result

	@staticmethod
	def aggregateList(featureList):
		"""
		Aggregates a list of Feature objects into one Aggregation object
		"""
		cdef vector[_Model] cFeatureList
		cdef Features modelInstance
		cdef _Model _modelInstance

		for element in featureList:
			modelInstance = element
			_modelInstance = dereference(modelInstance._this)
			cFeatureList.push_back(_modelInstance)

		cdef _Aggregation temp = aggregate(cFeatureList)
		cdef Aggregation result = Aggregation(Features())
		result._this = new _Aggregation(temp)
		return result

	def collapse(self, settings={}):
		"""
		Collapses an Aggregation object into a Features object. Thresholds can be set with a settings object.

		Parameters
		----------
		settings : dictionary

		Returns
		-------
		Features
			model instance derived from this aggregation
		"""
		cdef _Settings parsedSettings = _getSettingsObject(settings)
		cdef _Model temp = self._this.collapse(parsedSettings)
		cdef Features result = Features()
		result._this = new _Model(temp)
		return result

	def getCommonClusteringRatios(self):
		return self._this.getCommonClusteringRatios()

cdef extern from "src/generative.h" namespace "CILA::core":
	void _setSeed "CILA::core::setSeed" (index seed)

def setSeed(index seed):
	""" Set the random seed that is used for sampling.

	Note that there is a separate random number generator per thread.

	Parameters
	----------
	seed : uint32_t
		The seed
	"""
	_setSeed(seed)


cdef extern from "src/Util.h":
	cdef cppclass _Util "CILA::core::Util":
		@staticmethod
		double computeNMI(vector[int] a, vector[int] b) except +

cdef class Util:

	@staticmethod
	def computeNMI(vector[int] a, vector[int] b):
		cdef double result = _Util.computeNMI(a,b)
		return result


cdef extern from "src/ModelIO.h" namespace "CILA::io::ModelIO":
	_Model readModel(string path) except +
	string writeModel(_Settings settings, string name, _Model model, bool preciseName) except +
	_Aggregation readAggregation(string path) except +
	string writeAggregation(_Settings settings, string name, _Aggregation agg, bool preciseName) except +
	_Settings readSettings(string path) except +
	string writeSettings(_Settings settings, string name, bool preciseName) except +

def loadModel(path):
	"""
	Loads a Features object from a file.

	Parameters
	----------
	path : string

	Returns
	-------
	Features
		model instance loaded from file.

	Raises
	------
	FileNotFoundError
		if path doesn't exist
	"""
	pathbytes = path.encode("utf-8")
	os.stat(path)
	cdef _Model temp = readModel(pathbytes)
	cdef Features result = Features()
	del result._this
	result._this = new _Model(temp)
	return result

def saveModel(name, Features model, bool preciseName=False):
	"""
	Saves a Features object to a file.

	Parameters
	----------
	name : string
		filename

	model : Features

	preciseName : bool
		Whether to use the name exactly. If this is true, the filename will be exactly as given.
		Otherwise  (default), the version number of the settings dictionary prepended and  a running count appended to avoid overwriting existing files with the same name.
		If preciseName is set to true, existing files with the same name are overwritten without warning

	Returns
	-------
	string
		filename the model was saved to
	"""
	pathbytes = name.encode("utf-8")
	cdef _Settings parsedSettings = _getSettingsObject()
	cdef _Model cppmodel = dereference(model._this)
	result = writeModel(parsedSettings, pathbytes, cppmodel, preciseName)
	return result.decode("utf-8")

def loadAggregation(path):
	"""
	Loads an Aggregation object from a file.

	Parameters
	----------
	path : string

	Returns
	-------
	Aggregation
		aggregation instance loaded from file.

	Raises
	------
	FileNotFoundError
		if path doesn't exist
	"""
	pathbytes = path.encode("utf-8")
	cdef _Aggregation temp = readAggregation(pathbytes)
	cdef Features dummymodel = Features()
	cdef Aggregation result = Aggregation(dummymodel)
	del result._this
	result._this = new _Aggregation(temp)
	return result

def saveAggregation(name, Aggregation agg, bool preciseName=False):
	"""
	Saves an Aggregation to a file.

	Parameters
	----------
	name : string
		filename

	agg : Aggregation

	preciseName : bool
		Whether to use the name exactly. If this is true, the filename will be exactly as given.
		Otherwise  (default), the version number of the settings dictionary prepended and a running count appended to avoid overwriting existing files with the same name.
		If preciseName is set to true, existing files with the same name are overwritten without warning

	Returns
	-------
	string
		filename the model was saved to
	"""
	pathbytes = name.encode("utf-8")
	cdef _Settings parsedSettings = _getSettingsObject()
	cdef _Aggregation cppagg = dereference(agg._this)
	result = writeAggregation(parsedSettings, pathbytes, cppagg, preciseName)
	return result.decode("utf-8")

def loadSettings(path):
	pathbytes = path.encode("utf-8")
	cdef _Settings temp = readSettings(pathbytes)
	return settingsObjectToDict(temp)

def saveSettings(name, settings={}, bool preciseName=False):
	pathbytes = name.encode("utf-8")
	cdef _Settings parsedSettings = _getSettingsObject(settings)
	result = writeSettings(parsedSettings, pathbytes, preciseName)
	return result.decode("utf-8")

cdef extern from "src/Input.h" namespace "CILA::io::Input":
	_Graph computeR(vector[vector[double]], bool euclid, bool normalize) except +

def toSquareMatrix(inputmatrix, bool euclid = True, bool normalize = False):
	"""
	Parameters
	----------
	inputmatrix : vector[vector[double]]
		data matrix which should be transformed to a square matrix

	euclid : bool
		if this is true, the Euclidean distance between two data points is used as output value
		if false, the dot product is used instead

	Returns
	-------
	square matrix, dimension equals first dimension of inputmatrix

	"""
	return computeR(inputmatrix, euclid, normalize).edges

cdef extern from "src/Plotting.h" namespace "CILA::io::Plotting":
	void exportToGraphviz(_Model model, _Graph R, string name, index feature, bool positive) except +

def exportToGraphViz(name, g, Features model, feature, bool positive):
	"""
	Exports graph and model to the dot format used by GraphViz. Nodes are colored according to their cluster, weight of edges is taken from input graph g.

	Parameters
	----------
	name : string
		filename for exported file

	g : vector[vector[double]]
		input graph

	feature : index
		feature number from which the clusters should be taken

	positive : bool
		Whether the edge weights are purely positive

	Returns
	-------
	None
	"""
	if len(g) != model.getNodeCount():
		raise ValueError("Graph g has " + str(len(g)) + " nodes, but model has" + str(model.getNodeCount()))

	if feature < 0 or feature >= model.getFeatureCount():
		raise ValueError("Can't plot feature " + str(feature) + " of model with " + model.getFeatureCount() + " features.")

	pathbytes = name.encode("utf-8")
	cdef _Model cppmodel = dereference(model._this)
	cdef _Graph graph = _Graph(g)
	exportToGraphviz(cppmodel, graph, pathbytes, feature, positive)
