CC=g++

FILES= src/Aggregation.cpp src/generative.cpp src/Graph.cpp src/main.cpp src/Plotting.cpp src/sampler.cpp src/TestChamber.cpp src/Analysis.cpp src/Model.cpp src/Input.cpp  src/ModelIO.cpp src/Settings.cpp  src/Util.cpp
EXECUTABLE=ILA
LFLAGS= 
CCFLAGS= --openmp --std=c++11 -Icereal -Icxxopts

all:
	$(CC) $(CCFLAGS) $(FILES) -o $(EXECUTABLE) $(LFLAGS)

clean:
	rm -f $(EXECUTABLE)
