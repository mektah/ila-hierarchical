from cilaUtil import getPrefix

outputbasename = 'nmi-plots'
numchains = 1

with open(outputbasename+'.tex', 'w') as tex:

    tex.write("\\documentclass{article}\n")
    tex.write("\\usepackage{subcaption}\n")
    tex.write("\\usepackage{pgfplots}\n")
    tex.write("\\pgfplotsset{compat=1.14}\n")
    tex.write("\\usepgfplotslibrary{external}\n") 
    tex.write("\\tikzexternalize\n")
    tex.write("\\begin{document}\n")

    tex.write("\\section{NMI}\n")

    tex.write("\\begin{figure}\n")
    for i in range(8):
        tex.write("\\begin{subfigure}{0.45\\linewidth}\n")
        tex.write("\\begin{tikzpicture}\n")
        tex.write("\\begin{axis}[\n")
        tex.write("\txlabel=iterations, ylabel=nmi, ymin=0.12, ymax=0.3, width=\\linewidth]\n")

        deconfound, euclid, normalize, singlefeature = decodeOptionsFromPID(i)

        colorlist = ['blue', 'green', 'orange', 'teal', 'red', 'lime', 'olive', 'magenta', 'cyan', 'lightgray', 'black', 'pink']

        confoundString = "deconfounded" if deconfound else "original"
        euclidString = "euclid" if euclid else "dotProduct"
        normalizedString = "normalized" if normalize else "unnormalized"
        
        for chain in range(numchains):
            prefix = getPrefix(deconfound, euclid, normalize, singlefeature, chain)
            tex.write("\\addplot[color=" + colorlist[chain] + "] file [x expr=\\thisrowno{0}*1000] {" + prefix + "-nmi.dat};\n")

        tex.write("\\end{axis}\n")
        tex.write("\\end{tikzpicture}\n")
        tex.write("\\caption{"+confoundString+'-'+euclidString+'-'+normalizedString+"}\n")
        tex.write("\\end{subfigure}\n")
        if i % 2 == 0:
            tex.write("\\quad\n")
        else:
            tex.write("\n")
    tex.write("\\caption{NMI of samples for different parameters}\n")
    tex.write("\\end{figure}\n")

    tex.write("\\end{document}\n")


