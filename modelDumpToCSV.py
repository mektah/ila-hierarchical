from CILA import *
import CILA

import os
import pickle
import pandas
import math
from sklearn import metrics

from cilaUtil import getNumbersOfSavedFiles, getPrefix, decodeOptionsFromPID


def evaluateAggregation(agg):
    n = agg.collapse().getNodeCount()
    clusteringRatios = agg.getCommonClusteringRatios()[0]
    assert(len(clusteringRatios) == n)
    
    categories = set([0,1])
    
    results = [None for i in range(n)]

    sumOfAbsErrors = 0
    
    #results for data from training set are obvious
    for index in range(n):
        if index % 2 == 0:
            results[index] = index > 41
        else:
            weightSumInCategory = {}
            elementsInCategory = {}
            for category in categories:
                weightSumInCategory[category] = 0
                elementsInCategory[category] = 0

            for trainindex in range(n):
                if trainindex % 2 == 0:
                    category = int(trainindex > 41)
                    weightSumInCategory[category] += clusteringRatios[index][trainindex]
                    elementsInCategory[category] += 1

                    
            assert(len(weightSumInCategory) == 2)
            assert(len(elementsInCategory) == 2)
            bestFit = -1
            maxValue = -float('inf')
            for category in categories:
                if weightSumInCategory[category] / elementsInCategory[category] > maxValue:
                    maxValue = weightSumInCategory[category] / elementsInCategory[category]
                    bestFit = category

            assert(maxValue > -float('inf'))

            results[index] = bestFit
            sumOfAbsErrors += math.fabs(results[index] - int(index > 41))
        
    return sumOfAbsErrors

def processFiles(rootpath, diagnosis, prefix, printthin = 1):
    numbers = getNumbersOfSavedFiles(rootpath, prefix)

    if len(numbers) == 0:
        print("No files to process for "+prefix+", aborting.")
        return

    settings = CILA.loadSettings(os.path.join(rootpath, prefix+".settings"))

    # load R
    graphfilename = prefix+'.graph'
    R = list()
    with open(os.path.join(rootpath, graphfilename), 'rb') as f:
        unpickler = pickle.Unpickler(f)
        R = unpickler.load()
            
    randsum = 0
    randcount = 0

    agg = Aggregation(Features.emptyModel(len(ground_truth), 1, 1, 0))

    with open(prefix+'-stats.dat', 'w') as f:
        f.write('number\trand\tcumMeanRand\tlogprior\tloglikelihood\tfeatureCount\tclusterCount\n')
        for number in numbers:
            model = loadModel(os.path.join(rootpath, prefix+'-'+str(number)+'.model'))
            #agg = Aggregation.aggregate(agg, number, Aggregation(model), 1)

            if number % printthin == 0:
                f.write(str(number) + '\t')

            #rand score
            rand = float('nan')
            clusterCount = 0
            if model.getFeatureCount() > 0:
                clusterMatrix = model.getClusterMatrix()
                clusters = [row[0] for row in clusterMatrix]
                minCluster = min(clusters)
                clusterCount = len(set(clusters))
                normalizedClusters = [i-minCluster for i in clusters]
                rand = metrics.adjusted_rand_score(diagnosis, normalizedClusters)
                randsum += rand
                randcount += 1

            if number % printthin == 0:
                #rand cumulative mean
                print(int(number), float(rand))
                f.write("{:.4f}\t".format(rand))

                if randcount > 0:
                    f.write("{:.4f}\t".format(randsum / randcount))
                else:
                    f.write(str(0) +'\t')

                #logprior and loglikelihood
                f.write(str(model.logprior()) + '\t' + str(model.loglikelihood(R)) + '\t')

                #feature count
                f.write(str(model.getFeatureCount()) +'\t')

                #cluster count
                f.write(str(clusterCount) + '\t')

                #classifier correctness - currently needs manual adaptation to data set
                #correctness = int('nan')
                #if len(agg.getCommonClusteringRatios()) > 0:
                #    correctness = evaluateAggregation(agg)
                f.write('\n')


if __name__ == '__main__':
    rootpath = '/home/moritz/Diplom/dump-ZInEP-0.14.1'
    labelpath = '/home/moritz/Diplom/ila-hierarchical'

    ground_truth = pandas.read_csv(os.path.join(labelpath,'ZInEP_TP2_PP_NPs_SP_sets.csv'), na_values=['-999','NaN',' ','9999999'],sep=",",decimal='.')
    labels = ground_truth.Risikogruppe2

    os.stat(rootpath)
    os.stat(labelpath)

    for i in range(32):
        deconfound, euclid, normalize, singlefeature, chain = decodeOptionsFromPID(i)

        prefix = getPrefix(deconfound, euclid, normalize, singlefeature, chain)

        processFiles(rootpath, labels, prefix, 10)
