import os
import glob
import fnmatch
import sys

import pandas
from scipy import linalg

def decodeOptionsFromPID(processID):
    deconfound = processID % 2 == 0
    euclid = int(processID / 2) % 2 == 0
    normalize = int(processID / 4) % 2 == 0
    singlefeature = int(processID / 8) % 2 == 0
    
    number = int(processID / 16)
    return deconfound, euclid, normalize, singlefeature, number


def getPrefix(deconfound, euclid, normalize, singlefeature, chainnumber, versionstring='0.14.1'):
    confoundString = "deconfounded" if deconfound else "original"
    euclidString = "euclid" if euclid else "dotProduct"
    normalizedString = "normalized" if normalize else "unnormalized"
    featureString = "singlefeature" if singlefeature else "multifeature"

    prefix = versionstring+'-'+confoundString+'-'+euclidString+'-'+normalizedString+'-'+featureString+'-chain-'+str(chainnumber)
    return prefix

def getNumbersOfSavedFiles(rootpath, prefix):
    matches = []
    for root, dirnames, filenames in os.walk(rootpath):
        for filename in fnmatch.filter(filenames, prefix+'*.model'):
            matches.append(os.path.join(root, filename))

    if len(matches) == 0:
        print("No matches found in " + os.path.join(rootpath, prefix)+'*.model')

    numbers = []
    for filename in matches:
        parts = filename.split('.')
        mainpart = parts[-2]
        number = mainpart.split('-')[-1]
        numbers.append(int(number))

    numbers.sort()

    return numbers

def removeLinearConfounds(data, path):
    confounds = pandas.read_csv(path)

    x, residuals, rank, s = linalg.lstsq(confounds, data)

    fit = confounds.as_matrix().dot(x)

    residual = data - fit

    return residual
