import unittest
import CILA

import os
import random

class TestCILA(unittest.TestCase):

	def test_emptyModel(self):
		n = 10
		alpha = 1
		gamma = 1
		s = 1
		model = CILA.Features.emptyModel(n, alpha, gamma, s)
		self.assertTrue(model.isConstructed())
		self.assertEqual(model.getNodeCount(), n)
		self.assertEqual(model.getFeatureCount(), 0)
		self.assertEqual(len(model.getFeatureMatrix()), n)
		self.assertEqual(len(model.getWeights()), 0)

	def test_singleFullFeatureModel(self):
		n = 30
		settings = CILA.getSettingsDict()
		settings['nodeCount'] = n
		model = CILA.Features.singleFullFeatureModel(settings)
		self.assertEqual(model.getNodeCount(), n)
		self.assertEqual(model.getFeatureCount(), 1)
		Z = model.getFeatureMatrix()
		self.assertEqual(len(Z), n)

		for f in Z:
			self.assertEqual(len(f), 1)
			self.assertTrue(f[0])

		C = model.getClusterMatrix()
		self.assertEqual(len(C), n)

		maxCluster = -1
		for c in C:
			self.assertEqual(len(c), 1)
			self.assertTrue(c[0] > -1)
			if c[0] > maxCluster:
				maxCluster = c[0]

		W = model.getWeights()
		self.assertEqual(len(W), 1)
		self.assertEqual(maxCluster+1, len(W[0]))

	def test_singletonAggregation(self):
		n = 100
		alpha = 1
		gamma = 1
		s = 1
		mu_w = 1
		sigma_w = 1
		mu_s = 0
		sigma_s = 1
		model = CILA.Features.generateModel(n, alpha, gamma, mu_w, sigma_w, mu_s, sigma_s)
		featureMatrix = model.getFeatureMatrix()
		clusterMatrix = model.getClusterMatrix()
		self.assertEqual(len(featureMatrix[0]), model.getFeatureCount())
		self.assertEqual(len(clusterMatrix[0]), model.getFeatureCount())
		self.assertEqual(len(model.getWeights()), model.getFeatureCount())
		agg = CILA.Aggregation(model)
		model2 = agg.collapse()
		# can't check for matrix equality, since features are permuted
		self.assertEqual(model.getFeatureMatrix(), model2.getFeatureMatrix())
		w1 = model.getWeights()
		w2 = model2.getWeights()
		self.assertEqual(len(w1), len(w2))
		for f in range(model.getFeatureCount()):
			weightsumA = 0
			weightsumB = 0
			self.assertEqual(len(w1[f]), len(w2[f]))
			clusterCount = len(w1[f])
			for i in range(clusterCount):
				for j in range(clusterCount):
					weightsumA += w1[f][i][j]
					weightsumB += w2[f][i][j]
			self.assertAlmostEqual(weightsumA, weightsumB)

		g = model.sampleGraphInstance(1)
		self.assertAlmostEqual(model.loglikelihood(g), model2.loglikelihood(g))
		
		for f in range(model.getFeatureCount()):
			self.assertAlmostEqual(model.loglikelihoodContribution(g, f), model2.loglikelihoodContribution(g, f))

	def test_aggregationThresholds(self):
		n = 3
		Z1 = [[True],[True],[True]]
		Z2 = [[True],[True],[False]]
		C1 = [[0],[0],[1]]
		C2 = [[0],[1],[-1]]
		W1 = [[[2, 1],[1, 2]]]
		W2 = [[[1, 2],[2, 1]]]
		model1 = CILA.Features(Z1, C1, W1)
		model2 = CILA.Features(Z2, C2, W2)
		agg1 = CILA.Aggregation(model1)
		agg2 = CILA.Aggregation(model2)
		settings = CILA.getSettingsDict()
		for i in range(100):
			settings['cthreshold'] = random.random()
			settings['fthreshold'] = random.random()
			settings['sthreshold'] = random.random()# not used yet
			weightA = random.random()
			combinedAgg = CILA.Aggregation.aggregate(agg1, weightA, agg2, 1-weightA)
			recoveredModel = combinedAgg.collapse(settings)
			Z = recoveredModel.getFeatureMatrix()
			self.assertTrue(Z[0][0])
			self.assertTrue(Z[1][0])
			self.assertEqual(Z[2][0], settings['fthreshold'] <= weightA)
			C = recoveredModel.getClusterMatrix()
			self.assertEqual(Z[2][0], C[2][0] != -1)
			self.assertEqual(C[0][0] == C[1][0], settings['cthreshold'] <= weightA)

	def test_loadSaveModel(self):
		# construct a model
		model = CILA.Features.generateModel(100)
		featureMatrix = model.getFeatureMatrix()
		clusterMatrix = model.getClusterMatrix()
		self.assertEqual(len(featureMatrix[0]), model.getFeatureCount())
		self.assertEqual(len(clusterMatrix[0]), model.getFeatureCount())
		self.assertEqual(len(model.getWeights()), model.getFeatureCount())
		self.assertTrue(model.isConstructed())
		tempfilename = ('unittest')

		randomfilename = CILA.saveModel(tempfilename, model)
		model2 = CILA.loadModel(randomfilename)
		os.remove(randomfilename)
		self.assertEqual(model.getClusterMatrix(), model2.getClusterMatrix())

		tempfilename = ('unittest.model')
		with self.assertRaises(FileNotFoundError):
			os.stat(tempfilename)
		randomfilename = CILA.saveModel(tempfilename, model, True)
		self.assertEqual(randomfilename, tempfilename)
		model2 = CILA.loadModel(randomfilename)
		os.remove(randomfilename)
		self.assertEqual(model.getClusterMatrix(), model2.getClusterMatrix())

	def test_loadSaveAggregation(self):
		model = CILA.Features.generateModel(100)
		self.assertTrue(model.isConstructed())
		agg = CILA.Aggregation(model)
		tempfilename = ('testagg')
		randomfilename = CILA.saveAggregation(tempfilename, agg)
		agg2 = CILA.loadAggregation(randomfilename)
		os.remove(randomfilename)
		self.assertEqual(agg.collapse().getClusterMatrix(), agg2.collapse().getClusterMatrix())

	def test_loadSaveSettings(self):
		settings = CILA.getSettingsDict()
		floatlist = ['mu_bias','sigma_bias','mu_weights','sigma_weights','gamma_scale','gamma_shape','alpha_shape','alpha_scale','variance_shape','variance_scale','init_alpha','init_gamma','init_bias','init_variance','cthreshold','fthreshold','sthreshold']
		for elem in floatlist:
			settings[elem] = random.random()
		boolList = ['sample_alpha','sample_gamma','sample_Z','sample_C','sample_weights','sample_bias','sample_variance']
		for elem in boolList:
			settings[elem] = random.random() < 0.5
		intList = ['iterations', 'nodeCount', 'burnin', 'thin']
		for elem in intList:
			settings[elem] = random.randint(0,100)
		filename = CILA.saveSettings('unittest', settings)
		self.assertTrue(os.path.isfile(filename))
		settings2 = CILA.loadSettings(filename)
		for elem in floatlist+boolList+intList:
			self.assertEqual(settings[elem], settings2[elem], elem+str(' not saved correctly.'))
		os.remove(filename)


	def test_exportToGraphViz(self):
		a = CILA.Features.generateModel(10)
		g = a.sampleGraphInstance(1)
		filename = 'testdot'
		extension = '.dat'
		if a.getFeatureCount() > 0:
			CILA.exportToGraphViz(filename, g, a, 0, False)
			CILA.exportToGraphViz(filename, g, a, 0, True)
			os.stat(filename + extension)
			os.remove(filename + extension)
			#TODO: add actual test about file contents here.


	def test_samplerSetup(self):
		# setting up random data
		G = []
		n = 20
		for i in range(0,n):
			G.append([])
			for j in range(0,n):
				G[i].append(random.random())

		# construction sampler without settings
		sampler = CILA.Sampler(G)
		model = sampler.getModel()
		self.assertTrue(model.isConstructed())
		self.assertEqual(model.getNodeCount(), n)
		self.assertEqual(model.getFeatureCount(), 0)

		# construcing sampler with untouched settings
		setdict = CILA.getSettingsDict()
		sampler = CILA.Sampler(G, setdict)
		model = sampler.getModel()
		self.assertTrue(model.isConstructed())
		self.assertEqual(model.getNodeCount(), n)
		self.assertEqual(model.getFeatureCount(), 0)

		# constructing sampler with settings
		setdict = CILA.getSettingsDict()
		setdict['iterations'] = 500
		setdict['sample_variance'] = True
		setdict['alpha_scale'] = 1
		sampler = CILA.Sampler(G, setdict)
		model = sampler.getModel()
		self.assertTrue(model.isConstructed())
		self.assertEqual(model.getNodeCount(), n)
		self.assertEqual(model.getFeatureCount(), 0)

		# bad data should raise errors
		rows = 5
		cols = 4
		malformedData = [[0 for b in range(cols)] for a in range(rows)]
		with self.assertRaises(ValueError):
			sampler2 = CILA.Sampler(malformedData, setdict)

		#inconsistent settings should raise errors
		badSetDict = CILA.getSettingsDict()
		badSetDict['sample_C'] = False
		badSetDict['sample_Z'] = True
		with self.assertRaises(ValueError):
			sampler = CILA.Sampler(G, badSetDict)

		badSetDict = CILA.getSettingsDict()
		badSetDict['crp_only'] = True
		badSetDict['sample_Z'] = True
		with self.assertRaises(ValueError):
			sampler = CILA.Sampler(G, badSetDict)

		# testing optional initial model
		initialModel = CILA.Features.generateModel(n)
		sampler2 = CILA.Sampler(G, setdict, initialModel)
		recoveredModel = sampler2.getModel()
		g = initialModel.sampleGraphInstance()
		self.assertEqual(initialModel.getClusterMatrix(), recoveredModel.getClusterMatrix())
		self.assertEqual(initialModel.getWeights(), recoveredModel.getWeights())
		self.assertEqual(initialModel.loglikelihood(g), recoveredModel.loglikelihood(g))# this doesn't test yet whether all parameters are saved, it might just take the same values from default.

		# testing correct initialization with crponly
		crpSettings = CILA.getSettingsDict()
		crpSettings['crp_only'] = True
		crpSettings['sample_Z'] = False
		sampler = CILA.Sampler(G, crpSettings)
		samplerModel = sampler.getModel()
		self.assertEqual(samplerModel.getFeatureCount(), 1)
		self.assertEqual(samplerModel.getFeatureMatrix(), [[True] for i in range(len(G))])

		#testing correct sampling bools with crponly
		for i in range(100):
			sampler.sampleIter()

		samplerModel = sampler.getModel()
		self.assertEqual(samplerModel.getFeatureCount(), 1)
		self.assertEqual(samplerModel.getFeatureMatrix(), [[True] for i in range(len(G))])


	def test_samplerRecognition(self):
		testgraph = [[2, 2, 0, 1, 1,], [2, 2, 0, 1, 1],[0, 0, 0, 0, 0],[1, 1, 0, 1, 1],[1, 1, 0, 1, 1]]
		settings = CILA.getSettingsDict()
		settings['sample_alpha'] = False
		settings['sample_gamma'] = False
		settings['sample_bias'] = False
		settings['sample_variance'] = True
		sampler = CILA.Sampler(testgraph, settings)

		for i in range(0, 10000):
			sampler.sampleIter()

		fittedModel = sampler.getModel()
		comparison = sampler.getModel().sampleGraphInstance(0)

		for row in range(len(testgraph)):
			for col in range(len(testgraph)):
				self.assertAlmostEqual(testgraph[row][col], comparison[row][col], places=2)

	def test_samplingBools(self):
		testgraph = [[2, 2, 0, 1, 1,], [2, 2, 0, 1, 1],[0, 0, 0, 0, 0],[1, 1, 0, 1, 1],[1, 1, 0, 1, 1]]
		n = 5
		settings = CILA.getSettingsDict()
		initialModel = CILA.Features()
		while initialModel.getFeatureCount() == 0:
			initialModel = CILA.Features.generateModel(n)

		# test sampling C and W, but not Z
		settings['sample_Z'] = False
		settings['sample_C'] = True
		settings['sample_W'] = True
		sampler = CILA.Sampler(testgraph, settings, initialModel)
		for i in range(3000):
			sampler.sampleIter()
		self.assertEqual(initialModel.getFeatureMatrix(), sampler.getModel().getFeatureMatrix())
		self.assertNotEqual(initialModel.getWeights(), sampler.getModel().getWeights())
		self.assertNotEqual(initialModel.getClusterMatrix(), sampler.getModel().getClusterMatrix())

		# test sampling W, but not Z and C
		settings['sample_Z'] = False
		settings['sample_C'] = False
		settings['sample_W'] = True
		sampler = CILA.Sampler(testgraph, settings, initialModel)
		for i in range(100):
			sampler.sampleIter()
		self.assertEqual(initialModel.getFeatureMatrix(), sampler.getModel().getFeatureMatrix())
		self.assertEqual(initialModel.getClusterMatrix(), sampler.getModel().getClusterMatrix())
		self.assertNotEqual(initialModel.getWeights(), sampler.getModel().getWeights())

	def test_inputNormalization(self):
		data = [[1,2,0.4,0,0],[2,0,0.9,1,1],[3,1,0.2,1,0]]
		n= 3
		R = CILA.toSquareMatrix(data)
		self.assertEqual(len(R), n)
		self.assertEqual(len(R[0]), n)

		# test normalization with dot product
		R = CILA.toSquareMatrix(data, False, True)
		weightSum = sum([sum(row) for row in R])
		dotProductAverage = weightSum / (n*n)
		self.assertEqual(dotProductAverage, 1)

		# test normalization with euclid weights
		R = CILA.toSquareMatrix(data, True, True)
		weightSum = sum([sum(row) for row in R])
		euclidAverage = weightSum / (n*n)
		self.assertEqual(euclidAverage, -1)

	def test_computeNMI(self):
		# test singletons
		a = [0,1,2,3,4]
		b = [0,1,2,3,4]
		self.assertEqual(CILA.Util.computeNMI(a,b), 1)

		# test unequal clusterings
		a = [0,1,1,2,2]
		b = [0,1,2,1,2]
		self.assertNotEqual(CILA.Util.computeNMI(a,b), 0)
		self.assertNotEqual(CILA.Util.computeNMI(a,b), 1)

		# test symmetry
		self.assertEqual(CILA.Util.computeNMI(a,b), CILA.Util.computeNMI(b,a))

	def test_versionString(self):
		version = CILA.getVersionString()
		self.assertEqual(version, str(version))

	def test_setSeed(self):
		n = 20
		CILA.setSeed(0)
		initialModel = CILA.Features()
		while initialModel.getFeatureCount() == 0:
			initialModel = CILA.Features.generateModel(n)

		CILA.setSeed(1)	
		secondModel = CILA.Features()
		while secondModel.getFeatureCount() == 0:
			secondModel = CILA.Features.generateModel(n)

		self.assertNotEqual(initialModel.getClusterMatrix(), secondModel.getClusterMatrix())
		
		CILA.setSeed(0)	
		thirdModel = CILA.Features()
		while thirdModel.getFeatureCount() == 0:
			thirdModel = CILA.Features.generateModel(n)

		self.assertEqual(initialModel.getClusterMatrix(), thirdModel.getClusterMatrix())

		

if __name__ == '__main__':
	unittest.main()
